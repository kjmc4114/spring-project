package 기문이;

public class T_34DTO {
	   private String userid,name,years,result;
	   private int cm,kg;
	   private double bmi;
	   
	   public String getUserid() {
	      return userid;
	   }
	   public void setUserid(String userid) {
	      this.userid = userid;
	   }
	   public String getName() {
	      return name;
	   }
	   public void setName(String name) {
	      this.name = name;
	   }
	   public String getYears() {
	      return years;
	   }
	   public void setYears(String years) {
	      this.years = years;
	   }
	   public String getResult() {
	      return result;
	   }
	   public void setResult(String result) {
	      this.result = result;
	   }
	   public int getCm() {
	      return cm;
	   }
	   public void setCm(int cm) {
	      this.cm = cm;
	   }
	   public int getKg() {
	      return kg;
	   }
	   public void setKg(int kg) {
	      this.kg = kg;
	   }
	   public double getBmi() {
	      return bmi;
	   }
	   public void setBmi(double bmi) {
	      this.bmi = bmi;
	   }
	   @Override
	   public String toString() {
	      return "T_34DTO [userid=" + userid + ", name=" + name + ", years=" + years + ", result=" + result + ", cm=" + cm
	            + ", kg=" + kg + ", bmi=" + bmi + "]";
	   }
	   public T_34DTO(String userid, String name, String years, int cm, int kg) {
	      super();
	      this.userid = userid;
	      this.name = name;
	      this.years = years;
	      this.cm = cm;
	      this.kg = kg;
	      
	      bmi=kg/((cm*cm)*0.0001);
	      
	      if(bmi<18.5){
	         result="저체중";
	      }else if(bmi<22.9){
	         result="정상";
	      }else if(bmi<=24.9){
	         result="과체중";
	      }else{
	         result="비만";
	      }
	   }
	}
