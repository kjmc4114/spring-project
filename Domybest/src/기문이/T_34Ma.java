package 기문이;

import java.util.List;
import java.util.Scanner;

public class T_34Ma {
   private T_34DAO dao;

   public T_34Ma() {
      dao = new T_34DAO();
   }
//목록 부르는 메소드
   void list() {

      // T_34DAO dao = new DAO();
      // List<T_34DTO> items = dao.listHealth() 이 두 문장인데 위에 private를 선언함으로 써
      // 한문장으로 할 수 있다.
      List<T_34DTO> items = dao.listHealth();// DAO에서 추가한 리스트 메소드를 불러오는
      for (T_34DTO dto : items) {
         System.out.println("id : " + dto.getUserid());
         System.out.println("이름 : " + dto.getName());
         System.out.println("나이 : " + dto.getYears());
         System.out.println("키 : " + dto.getCm());
         System.out.println("무게 : " + dto.getKg());
         System.out.println("BMI : " + String.format("%.1f", dto.getBmi()));
         System.out.println(dto.getResult());
         System.out.println();
      }
   }
//목록 찾는 메소드
   void search() {
      Scanner scan = new Scanner(System.in);
      System.out.println("검색할 id를 입력하세요");
      String id = scan.next();
      //T_34DTO는 자료형(String,int)으로 items라는 변수에 DAO의 serarch 메소드에 id의 값을 넣어서 불러옴      
      T_34DTO items=dao.searchHealth(id);
      if(items == null){
         System.out.println("없는 회원입니다.");
      } else {
      System.out.println("id : " + items.getUserid());
      System.out.println("이름 : " + items.getName());
      System.out.println("나이 : " + items.getYears());
      System.out.println("키 : " + items.getCm());
      System.out.println("무게 : " + items.getKg());
      System.out.println("BMI : " + String.format("%.1f", items.getBmi()));
      System.out.println(items.getResult());
      System.out.println();
      }
   }
//목록 추가하는 메소드(회원가입)
   void insert(){
      Scanner scan=new Scanner(System.in);
      System.out.print("id를 입력해주세요.");
      String userid=scan.next();
      System.out.print("이름을 입력해주세요.");
      String name=scan.next();
      System.out.print("나이를 입력해주세요.");
      String years=scan.next();
      System.out.print("키를 입력해주세요.");
      int cm=scan.nextInt();
      System.out.print("몸무게를 입력해주세요.");
      int kg=scan.nextInt();
      T_34DTO dto=new T_34DTO(userid,name,years,cm,kg);
      dao.insertHealth(dto);
      System.out.println("등록되었습니다.");
   }
//목록을 삭제하는 메소드(회원탈퇴)
   void delete(){
      Scanner scan = new Scanner(System.in);
      System.out.print("삭제할 id를 입력하세요.");
      String id=scan.next();
      int result=dao.deleteHealth(id);
      if(result == 1){
         System.out.println("삭제되었습니다.");
      }else {
         System.out.println("이름을 확인하세요.");
      }
   }
   public static void main(String[] args) {
      Scanner scan = new Scanner(System.in);
      T_34Ma t = new T_34Ma();
      while (true) {
         System.out.println("작업을 선택하세요(1.전체 목록, 2.회원 검색, 3.회원 가입, 4.회원 탈퇴  0:종료)");
         int n = scan.nextInt();
         switch (n) {
         case 0:
            scan.close();
            System.out.println("프로그램 종료");
            System.exit(0);
            break;
         case 1:
            t.list();
            break;
         case 2:
            t.search();
            break;
         case 3:
            t.insert();
            break;
         case 4:
            t.delete();
            break;
         }
      }
   }
}
