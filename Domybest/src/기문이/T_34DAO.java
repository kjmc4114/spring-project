package 기문이;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class T_34DAO {
   // 목록 리턴
   public List<T_34DTO> listHealth() {
      List<T_34DTO> items = new ArrayList<>();
      Connection conn = null;// db접속
      PreparedStatement pstm = null;// sql 실행
      ResultSet rs = null;// 실행 결과를 한 레코드식 읽음

      try {
         conn = DB.dbConn();// db(sql) 접속
         String sql = "select * from health";// sql 명령어를 스트링에 만듬
         pstm = conn.prepareStatement(sql);// sql에 담은 명령어를 실행 준비
         rs = pstm.executeQuery();// 쿼리 실행 => 결과셋에 전달
         // 결과셋.next() 다음 레코드를 읽음, 결과세.previous() 앞 레코드를 읽음
         while (rs.next()) {// 읽을 다음 레코드가 있을 때까지
            // 결과셋.get자료형("컬럼이름) 컬럼의 값을 읽음
            String userid = rs.getString("userid");
            String name = rs.getString("name");
            String years = rs.getString("years");
            int cm = rs.getInt("cm");
            int kg = rs.getInt("kg");
            items.add(new T_34DTO(userid, name, years, cm, kg));// 리스트에 추가
         }
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         try {
            if (rs != null)
               rs.close();
         } catch (Exception e2) {
            e2.printStackTrace();
         }
         try {
            if (pstm != null)
               pstm.close();
         } catch (Exception e2) {
            e2.printStackTrace();
         }
      }
      return items;
   }

   // 목록 찾기
   public T_34DTO searchHealth(String id) {
      T_34DTO items = null;
      Connection conn = null;
      PreparedStatement pstm = null;
      ResultSet rs = null;
      try {
         conn = DB.dbConn();
         String sql = "select * from health where userid=?";
         pstm = conn.prepareStatement(sql);
         pstm.setString(1, id);
         rs = pstm.executeQuery();
         if (rs.next()) {
            String userid = rs.getString("userid");
            String name = rs.getString("name");
            String years = rs.getString("years");
            int cm = rs.getInt("cm");
            int kg = rs.getInt("kg");
            items = new T_34DTO(userid, name, years, cm, kg);
         } else {
            items = null;
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
      return items;
   }

   // 목록 추가
   public void insertHealth(T_34DTO dto) {
      Connection conn = null;
      PreparedStatement pstm = null;
      try {
         conn = DB.dbConn();
         String sql = "insert into health (userid,name,years,cm,kg) values(?,?,?,?,?)";
         pstm = conn.prepareStatement(sql);// sql에 명령어를 전달하는..
         pstm.setString(1, dto.getUserid());
         pstm.setString(2, dto.getName());
         pstm.setString(3, dto.getYears());
         pstm.setInt(4, dto.getCm());
         pstm.setInt(5, dto.getKg());
         pstm.executeUpdate();
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         // open : Connection => PreparedStatement
         // close : 역순으로 닫음
         try {
            if (pstm != null)
               pstm.close();
         } catch (SQLException e) {
            e.printStackTrace();
         }
         try {
            if (conn != null)
               conn.close();
         } catch (SQLException e) {
            e.printStackTrace();
         }
      }
   }

   // 목록 삭제
   public int deleteHealth(String id) {
      int result = 0;
      Connection conn = null;
      PreparedStatement pstm = null;
      try {
         conn = DB.dbConn();
         String sql = "delete from health where userid=?";
         pstm = conn.prepareStatement(sql);
         pstm.setString(1, id);
         result = pstm.executeUpdate();// delete된 레코드의 갯수가 result에 저장됨
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         try {
            if (pstm != null)
               pstm.close();
         } catch (SQLException e) {
            e.printStackTrace();
         }
         try {
            if (conn != null)
               conn.close();
         } catch (SQLException e) {
            e.printStackTrace();
         }
      }
      return result;
   }
}
