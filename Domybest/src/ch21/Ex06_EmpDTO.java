package ch21;

import java.sql.Date; // date 가 여러가지가 있으므로 주의할것.

public class Ex06_EmpDTO {
private int empno;
private String ename;
private Date datetime;
private int sal;


public Ex06_EmpDTO() {
}


public Ex06_EmpDTO(int empno, String ename, Date datetime, int sal) {
	this.empno = empno;
	this.ename = ename;
	this.datetime = datetime;
	this.sal = sal;
}


public int getEmpno() {
	return empno;
}


public void setEmpno(int empno) {
	this.empno = empno;
}


public String getEname() {
	return ename;
}


public void setEname(String ename) {
	this.ename = ename;
}


public Date getDatetime() {
	return datetime;
}


public void setDatetime(Date datetime) {
	this.datetime = datetime;
}


public int getSal() {
	return sal;
}


public void setSal(int sal) {
	this.sal = sal;
}


@Override
public String toString() {
	return "Ex06_EmpDTO [empno=" + empno + ", ename=" + ename + ", datetime=" + datetime + ", sal=" + sal + "]";
}

}
