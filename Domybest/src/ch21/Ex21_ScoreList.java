package ch21;

import java.awt.EventQueue;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Ex21_ScoreList extends JFrame {
	private Ex19_ScoreDAO dao;
	private Vector data, col;
	private JPanel contentPane;
	private JTextField tfStudentNo;
	private JTextField tfName;
	private JTextField tfKor;
	private JTextField tfEng;
	private JTextField tfMat;
	private JTable table;
	private JButton btnSave;
	private DefaultTableModel model;
	private JButton btnUpdate;
	private Ex18_ScoreDTO dto;
	private JTextField tfFind;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex21_ScoreList frame = new Ex21_ScoreList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void clear() {
		tfStudentNo.setText("");
		tfName.setText("");
		tfKor.setText("");
		tfEng.setText("");
		tfMat.setText("");
		tfStudentNo.requestFocus();
		tfStudentNo.setEditable(true);
	}

	public void list() {

		model = new DefaultTableModel(dao.listScore(), col) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
	}

	public void Find() {
		String name = tfFind.getText();

		model = new DefaultTableModel(dao.FindScore(name), col) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setModel(model);
	}

	public void input() {
		String student_no = tfStudentNo.getText();
		String name = tfName.getText();
		int kor = Integer.parseInt(tfKor.getText());
		int eng = Integer.parseInt(tfEng.getText());
		int mat = Integer.parseInt(tfMat.getText());
		dto = new Ex18_ScoreDTO(student_no, name, kor, eng, mat);
	}

	public Ex21_ScoreList() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 602, 529);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("학번");
		lblNewLabel.setBounds(0, 21, 99, 29);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("이름");
		lblNewLabel_1.setBounds(0, 64, 99, 29);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("국어");
		lblNewLabel_2.setBounds(0, 103, 99, 29);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("영어");
		lblNewLabel_3.setBounds(0, 141, 99, 29);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("수학");
		lblNewLabel_4.setBounds(0, 183, 99, 29);
		contentPane.add(lblNewLabel_4);

		tfStudentNo = new JTextField();
		tfStudentNo.setBounds(109, 18, 186, 35);
		contentPane.add(tfStudentNo);
		tfStudentNo.setColumns(10);

		tfName = new JTextField();
		tfName.setBounds(109, 61, 186, 35);
		contentPane.add(tfName);
		tfName.setColumns(10);

		tfKor = new JTextField();
		tfKor.setBounds(109, 100, 186, 35);
		contentPane.add(tfKor);
		tfKor.setColumns(10);

		tfEng = new JTextField();
		tfEng.setBounds(109, 138, 186, 35);
		contentPane.add(tfEng);
		tfEng.setColumns(10);

		tfMat = new JTextField();
		tfMat.setBounds(109, 180, 186, 35);
		contentPane.add(tfMat);
		tfMat.setColumns(10);

		btnSave = new JButton("저장");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				input();
				// 여기서 부터가 안되면 result 가 뭐가 올지 알아보자.
				int result = dao.insertScore(dto);
				if (result == 1) {
					JOptionPane.showMessageDialog(Ex21_ScoreList.this, "저장성공!");
					list();
					table.setModel(model);
					clear();
				}

			}
		});
		btnSave.setBounds(430, 95, 124, 37);
		contentPane.add(btnSave);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 287, 554, 171);
		contentPane.add(scrollPane);
		dao = new Ex19_ScoreDAO();
		col = new Vector();
		col.add("학번");
		col.add("이름");
		col.add("국어");
		col.add("영어");
		col.add("수학");
		col.add("총점");
		col.add("평균");

		model = new DefaultTableModel(dao.listScore(), col) {
			@Override
			public boolean isCellEditable(int row, int column) {
				// TODO Auto-generated method stub
				return false;
			}
		};
		table = new JTable(model);
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				// 테이블의 행렬에서 값 가져오기
				int index = table.getSelectedRow();

				tfStudentNo.setEditable(false);
				tfStudentNo.setText(table.getValueAt(index, 0) + "");
				tfName.setText(table.getValueAt(index, 1) + "");
				tfKor.setText(table.getValueAt(index, 2) + "");
				tfEng.setText(table.getValueAt(index, 3) + "");
				tfMat.setText(table.getValueAt(index, 4) + "");

			}
		});
		scrollPane.setViewportView(table);

		btnUpdate = new JButton("수정");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input();
				int result = dao.updateScore(dto);
				if (result == 1) {
					JOptionPane.showMessageDialog(Ex21_ScoreList.this, "수정성공!");
					list();
					table.setModel(model);
					clear();
				}
			}
		});
		btnUpdate.setBounds(430, 137, 124, 37);
		contentPane.add(btnUpdate);

		JButton btnDelete = new JButton("삭제");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String student_no = tfStudentNo.getText();
				int result = 0;
				int response = JOptionPane.showConfirmDialog(Ex21_ScoreList.this, "삭제하시겠습니까?");
				if (response == JOptionPane.YES_OPTION) {
					result = dao.deleteScore(student_no);
				}
				if (result == 1) {
					JOptionPane.showMessageDialog(Ex21_ScoreList.this, "삭제했습니다!");
					list();
					table.setModel(model);
					clear();
				}
			}
		});
		btnDelete.setBounds(430, 179, 124, 37);
		contentPane.add(btnDelete);

		JLabel lblNewLabel_5 = new JLabel("이름을 입력하세요");
		lblNewLabel_5.setBounds(0, 233, 208, 29);
		contentPane.add(lblNewLabel_5);

		tfFind = new JTextField();
		tfFind.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				String name = tfFind.getText();

				model = new DefaultTableModel(dao.FindScore(name), col) {
					@Override
					public boolean isCellEditable(int row, int column) {
						return false;
					}
				};
				table.setModel(model);

			}
		});
		tfFind.setBounds(216, 231, 186, 35);
		contentPane.add(tfFind);
		tfFind.setColumns(10);

		JButton btnFind = new JButton("찾기");
		btnFind.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

			}
		});
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		btnFind.setBounds(430, 229, 124, 37);
		contentPane.add(btnFind);
	}
}
