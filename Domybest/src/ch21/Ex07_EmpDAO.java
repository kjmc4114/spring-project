package ch21;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import DB_Report.DB;

// dao : Data Access Object(데이터 처리 객체)
public class Ex07_EmpDAO {
	// 사원 레코드 삭제
	public int deleteEmp(int empno) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.dbConn();// db 접속
			String sql = "delete from emp where empno=?";//4번이 ?자리
			pstmt = conn.prepareStatement(sql);//sql 실행
			pstmt.setInt(1, empno);
			// affected rows를 리턴함
			result = pstmt.executeUpdate();//1 아니면 0 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	// 사원 레코드 추가
	public void insertEmp(Ex06_EmpDTO dto) {
		Connection conn = null; // db 접속
		PreparedStatement pstmt = null; // sql 실행 
		try {
			conn = DB.dbConn();
			String sql = "insert into emp (empno,ename,hiredate,sal)" + "values (?,?,?,?)";
			pstmt = conn.prepareStatement(sql);//sql을 실행
			pstmt.setInt(1, dto.getEmpno());
			pstmt.setString(2, dto.getEname());
			pstmt.setDate(3, dto.getDatetime());//날짜라서 date타입
			pstmt.setInt(4, dto.getSal());
			pstmt.executeUpdate();//업데이트 실행-->결과셋에 전달
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 오픈한 역순으로 닫음
			// finally절이 없어도 실행은 가능하나 성능이 저하됨
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	// 사원 목록을 리턴하는 코드
	public List<Ex06_EmpDTO> listEmp() {
		List<Ex06_EmpDTO> items = new ArrayList<>();
		Connection conn = null; // db 접속 처리
		PreparedStatement pstmt = null; // sql 실행
		ResultSet rs = null; // 실행결과를 한 레코드씩 읽음
		try {
			conn = DB.dbConn();
			String sql = "select * from emp";
			pstmt = conn.prepareStatement(sql); // 쿼리 실행 준비
			rs = pstmt.executeQuery(); // 쿼리 실행 => 결과셋에 전달
			// 결과셋.next() 다음 레코드를 읽음, 결과셋.previous() 앞 레코드를 읽음
			while (rs.next()) {
				// 결과셋.get자료형("컬럼이름") 컬럼의 값을 읽음
				int empno = rs.getInt("empno");
				String ename = rs.getString("ename");
				Date datetime = rs.getDate("datetime"); // java.sql.Date
				int sal = rs.getInt("sal");
				// 리스트에 dto 추가
				items.add(new Ex06_EmpDTO(empno, ename, datetime, sal));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items; // 리스트를 리턴함
	}
}
