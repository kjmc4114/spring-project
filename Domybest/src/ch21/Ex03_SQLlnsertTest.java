package ch21;

import java.io.FileInputStream;
import java.sql.*;
import java.util.Properties;



public class Ex03_SQLlnsertTest {
	public static void main(String[] args) throws Exception {
		FileInputStream fis = new FileInputStream("d:\\db.prop.txt");
		Properties prop = new Properties();
		prop.load(fis);
		String driver = prop.getProperty("driver");
		String url = prop.getProperty("url");
		String id = prop.getProperty("id");
		String password = prop.getProperty("password");
		System.out.println(driver);
		System.out.println(url);
		System.out.println(id);
		System.out.println(password);
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			Class.forName(driver); // 드라이버
			conn = DriverManager.getConnection(url, id, password); // DB에 접속
			String name = "세르릉";
			String address = "부촌";
			String num = "비밀";
			String email = "감옥@부촌구청.com";
			// 가독성을 위해서.
			String sql ="insert into confidentiality (name,address,num,email)"
			 +"values"
			 +"(?,?,?,?)";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1,name); // DB에서 데이터 번호는 1번부터 시작된다
			stmt.setString(2,name);
			stmt.setString(3,name);
			stmt.setString(4,name);
			stmt.executeUpdate();
			
//			String sql = "insert into confidentiality (name,address,num,email) values ('" + name + "','" + address
//					+ "','" + num + "','" + email + "')";
//			stmt = conn.createStatement();
//			stmt.executeUpdate(sql);
			// 스트링 빌더로 할때도 가능함.
			// StringBuilder sb = new StringBuilder ();
			// sb.append("insert into confidentiality
			// (name,address,num,email)");
			// sb.append("values");
			// sb.append("('"+name+"','"+address+"','"+num+"','"+email+"')");
			// stmt = conn.createStatement();
			// stmt.executeUpdate(sb.toString());
			System.out.println(sql);
			System.out.println("추가되었습니다.");
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				if( stmt != null) stmt.close();
				
			}catch(Exception e){
				e.printStackTrace();
			}
			try{
				if( conn != null) conn.close();
				conn.close();
				
			}catch(Exception e){
				e.printStackTrace();
			}

		}
	}
}
