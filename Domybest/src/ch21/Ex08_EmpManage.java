package ch21;

import java.sql.Date;
import java.util.List;
import java.util.Scanner;

public class Ex08_EmpManage {
	private Ex07_EmpDAO dao;

	public Ex08_EmpManage() { // 생성자
		dao = new Ex07_EmpDAO();
	}

	void delete() {
		Scanner scan = new Scanner(System.in);
		System.out.println("삭제할 사번:");//print(줄바꿈을 안한다.) println(줄바꿈을 한다)
		int empno = scan.nextInt();
		int result = dao.deleteEmp(empno);
		if (result == 1) {
			System.out.println("삭제되었습니다.");
		} else {
			System.out.println("존재하지 않는 사번입니다.");
		}
	}

	// 사원 목록 출력
	public void list() {
		// 사원 목록을 리턴받음
		List<Ex06_EmpDTO> items = dao.listEmp();
		System.out.println("사번\t이름\t입사일자\t\t급여");
		for (Ex06_EmpDTO dto : items) {
			System.out.print(dto.getEmpno() + "\t");
			System.out.print(dto.getEname() + "\t");
			System.out.print(dto.getDatetime() + "\t");
			System.out.print(dto.getSal() + "\n");
		}
	}

	void insert() {
		Scanner scan = new Scanner(System.in);
		System.out.print("사번:");
		int empno = scan.nextInt(); // 정수 입력
		System.out.print("이름:");
		String ename = scan.next(); // 문자열 입력
		System.out.print("입사일자:");
		String datetime = scan.next();
		System.out.print("급여:");
		int sal = scan.nextInt();
// Date.valueOf(문자열) 문자열을 Date type으로 변환
//Date.valueOf(hiredate) 날짜라서 데이트 타입으로 받을 수 없기에 우선 스트링으로 바꾸고 그다음에 데이터타입으로 바꾸는 것이다
		Ex06_EmpDTO dto = new Ex06_EmpDTO(empno, ename, Date.valueOf(datetime), sal);
		dao.insertEmp(dto);
		System.out.println("저장되었습니다.");
	}

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Ex08_EmpManage emp = new Ex08_EmpManage();
		while (true) {
			System.out.println("메뉴를 선택하세요(1.목록, 2.추가, 3.삭제, 0:종료)");
			int code = scan.nextInt();
			switch (code) {
			case 0:
				System.exit(0);
				break;
			case 1:
				emp.list();
				break;
			case 2:
				emp.insert();
				break;
			case 3:
				emp.delete();
				break;
			}
		}
	}
}