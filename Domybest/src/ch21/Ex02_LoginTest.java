package ch21;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class Ex02_LoginTest extends JFrame {

	private JPanel contentPane;
	private JTextField userid;
	private JTextField pwd;
	private JLabel lblResult;
	private JButton btnLogin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex02_LoginTest frame = new Ex02_LoginTest();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex02_LoginTest() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("아이디");
		lblNewLabel.setBounds(22, 21, 99, 29);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("비밀번호");
		lblNewLabel_1.setBounds(22, 73, 99, 29);
		contentPane.add(lblNewLabel_1);
		
		userid = new JTextField();
		userid.setBounds(143, 18, 186, 35);
		contentPane.add(userid);
		userid.setColumns(10);
		
		pwd = new JTextField();
		pwd.setBounds(143, 70, 186, 35);
		contentPane.add(pwd);
		pwd.setColumns(10);
		
		btnLogin = new JButton("로그인");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String strUserid = userid.getText();
				String strPwd = String.valueOf(pwd.getText());
				Connection conn = null;
				PreparedStatement pstmt = null; //실무에서 사용함. 코딩하고 쉽고, 인젝션 공격에 대비가능함.DB서버의 성능향상
				ResultSet rs = null;
				try{FileInputStream fis = new FileInputStream("d:\\db.prop");
					Properties prop = new Properties();
					prop.load(fis);
					String url = prop.getProperty("url");
					String id = prop.getProperty("id");
					String password = prop.getProperty("password");
					conn=DriverManager.getConnection(url,id,password);
					String sql = "select * from member where userid=? and pwd=?";
					pstmt=conn.prepareStatement(sql);
					pstmt.setString(1, strUserid);
					pstmt.setString(2, strPwd);
					rs=pstmt.executeQuery();
					if(rs.next()){
						lblResult.setText(rs.getString("name")+"님환영합니다.");
					}else{
						lblResult.setForeground(new Color(200,20,1));
						lblResult.setText("아이디 또는 비밀번호가 일치하지 않습니다.");
					}
				}catch (Exception e1){
					e1.printStackTrace();
				}finally{
					
					try {
						if(rs!=null)rs.close();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					try {
						if(pstmt!=null)pstmt.close();
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					try {
						if(conn!=null)conn.close();;
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				
					
					
				}
			}
		});
		btnLogin.setBounds(22, 119, 153, 37);
		contentPane.add(btnLogin);
		
		lblResult = new JLabel("");
		lblResult.setBounds(22, 179, 380, 29);
		contentPane.add(lblResult);
	}
}
