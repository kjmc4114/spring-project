package ch21;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class Ex01_ConnectDatabase {
public static void main(String[] args) {
								//3306은 mySQL의 기본 포트 번호이다. 따라서 생략이 가능함.
		String url = "jdbc:mysql://localhost:3306/java"; //연결 문자열
		String id = "java";
		String password = "java1234";
		Connection conn = null; //DB접속처리
		Statement stmt = null; //SQL 실행
		ResultSet rs = null;  // 레코드 탐색 
		// 파일 처리, 네트워크 작업 , DB처리 과정은 try catch를 필수로 요구한다.
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("드라이버를 로딩했습니다.");
			conn =  DriverManager.getConnection(url, id, password);
			System.out.println("mysql 접속 성공");
			
			String sql = "select *from books";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()){
				int book_id = rs.getInt("book_id");
				String title = rs.getString("title");
				
				System.out.println(book_id+"\t"+title);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}finally {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
}
}
