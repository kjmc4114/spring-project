package ch21;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

//실제로 데이터 베이스에 접속해서 추가,수정,삭제를 담당하는 클래스
public class Ex04_CarDAO {
	// DB에 접속해서 커넥션을 리턴해줌. (커넥션하는 애)
	public Connection dbConn() {
		Connection conn = null;
		try {
			FileInputStream fis = new FileInputStream("d:\\db.prop.txt");
			Properties prop = new Properties();
			prop.load(fis);
			String url = prop.getProperty("url");
			String id = prop.getProperty("id");
			String password = prop.getProperty("password");
			conn = DriverManager.getConnection(url, id, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	public List<Ex03_CarDTO> listCar() {
		List<Ex03_CarDTO> items = new ArrayList<>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			// DB접속
			conn = dbConn();
			// Sql 실행
			String sql = "select * from car";

			// 각각의 레코드를 dto에 저장.
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				String license_number = rs.getString("license_number");
				String company = rs.getString("company");
				String type = rs.getString("type");
				int year = rs.getInt("year");
				int efficiency = rs.getInt("efficiency");
				Ex03_CarDTO dto = new Ex03_CarDTO(license_number, company, type, year, efficiency);
				items.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
//			rs.close();
//			pstmt.close();
//			conn.close(); 종료처리
		}
		return items;
	}
	public void insertCar(Ex03_CarDTO dto){
	Connection conn = null;
	PreparedStatement pstmt =null;
	
		try {
		
		conn=dbConn();
		String sql = "insert into car (license_number,company,type,year,efficiency) values (?,?,?,?,?)";
		pstmt=conn.prepareStatement(sql);
		pstmt.setString(1, dto.getLicense_number());
		pstmt.setString(2, dto.getCompany());
		pstmt.setString(3, dto.getType());
		pstmt.setInt(4, dto.getYear());
		pstmt.setInt(5, dto.getEfficiency());
		pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(pstmt!=null)pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(conn!=null)conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public int deleteCar(String license_number){
		Connection conn = null;
		PreparedStatement pstmt=null;
		int result = 0;
		try {
			conn=dbConn();
			String sql= "delete from car where license_number=?";
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, license_number);
			//영향을 받은 레코드수 (affected rows)
			result = pstmt.executeUpdate();
		} catch (Exception e) {
e.printStackTrace();
		}finally{
			try {
				if(pstmt!=null)pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(conn!=null)conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}
}
