package ch21;

import java.util.List;
import java.util.Scanner;

public class Ex05_CarManage {
	Ex04_CarDAO dao = new Ex04_CarDAO();

	void delete(){
		Scanner scan = new Scanner(System.in);
		System.out.println("삭제할 차량번호를 입력하세요");
		String license_number = scan.nextLine();
		int result = dao.deleteCar(license_number);
		if(result == 1){
			System.out.println("삭제되었습니다");
		}else{
			System.out.println("차량번호를 확인해라 좀");
		}
		
	}
	
	void insert(){
	Scanner scan = new Scanner (System.in);
	System.out.println("차량번호");
	String license_number = scan.nextLine();
	System.out.println("제조사");
	String company = scan.nextLine();
	System.out.println("타입");
	String type = scan.nextLine();
	System.out.println("제조년도");
	int year = scan.nextInt();
	System.out.println("연비");
	int efficiency = scan.nextInt();
	Ex03_CarDTO dto = new Ex03_CarDTO(license_number,company,type,year,efficiency);
	dao.insertCar(dto);
	System.out.println("추가되었습니다.");
}
	
void list(){
	List<Ex03_CarDTO> items = dao.listCar();
	System.out.println("차량번호\t 연도\t 제조사\t 타입\t 연비");
	for(Ex03_CarDTO dto : items){
		System.out.println(dto.getLicense_number()+"\t"+dto.getYear()+"\t"+dto.getCompany()+"\t"+dto.getType()+"\t"+dto.getEfficiency());
//		System.out.print(dto.getLicense_number()+"\t");
//		System.out.print(dto.getYear()+"\t");
//		System.out.print(dto.getCompany()+"\t");
//		System.out.print(dto.getType()+"\t");
//		System.out.print(dto.getEfficiency()+"\n");
	}
}	
public static void main(String[] args) {
	Ex05_CarManage car = new Ex05_CarManage();
	Scanner input = new Scanner(System.in);
	for(;;){
		System.out.println("작업을 선택하세요(1:목록 2:추가 3:삭제 0:종료):");
		int code = input.nextInt();
		switch (code) {
		case 0:
			input.close();
			System.out.println("프로그램을 종료합니다.");
			System.exit(0);
			break;
		case 1: car.list(); break;
		case 2: car.insert(); break;
		case 3: car.delete(); break;
		}
	}

}
}
