package ch21.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DeptDAO { // 부서목록을 활용하기 위한 클래스
	//Jtable은 벡터 아닌경우에는 Arraylist 를 사용한다.
	
	
	
	public ArrayList<DeptDTO> listDept(){
		ArrayList<DeptDTO> items = new ArrayList<>();
		Connection conn =null;
		PreparedStatement pstmt =null;
		ResultSet rs = null;
		try {
			conn =DB.HrConn();
			String sql = "select * from dept order by dname";
			pstmt = conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while (rs.next()){ //다음레코드가 있으면!
				int deptno = rs.getInt("deptno");
				String dname = rs.getString("dname");
				String loc = rs.getString("loc");
				DeptDTO dto = new DeptDTO(deptno, dname, loc);
				items.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		
		}finally{
			try {
				if(rs!=null)rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(pstmt!=null)pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(conn!=null)conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items;
	}

}
