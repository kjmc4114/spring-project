package ch21.oracle;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.List;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class EmpComboEXAM extends JFrame {

	private JPanel contentPane;
	private JComboBox combo;
	private List list;
	//변수 추가
	private DeptDAO deptDao;
	private ArrayList<DeptDTO> deptList;
	private EmpDAO empDao;
	private ArrayList<EmpDTO> emplist;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EmpComboEXAM frame = new EmpComboEXAM();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public EmpComboEXAM() {
		deptDao = new DeptDAO();
		deptList = deptDao.listDept();
		empDao = new EmpDAO();
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		list = new List();
		contentPane.add(list, BorderLayout.CENTER);

		combo = new JComboBox();
		combo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				String dname = combo.getSelectedItem().toString(); // getsel.. 메소드의 반환타입이 오브젝트이기 떄문에 String으로 바뀜				
				//emplist = empDao.listEmp(dname);
				int idx = combo.getSelectedIndex(); //콤보박스에서 선택한 아이템의 인덱스값.
				int deptno = deptList.get(idx).getDeptno(); //해당되는 리스트에서, 부서코드만 가져옴.
				System.out.println("deptno :"+deptno);
				emplist = empDao.listEmp(deptno);
				list.removeAll(); //리스트 자체의 기능.
				for(EmpDTO dto : emplist){
					list.add(dto.getEname());
				}
			}
		});
		for(DeptDTO dto : deptList){
			combo.addItem(dto.getDname());
		}
		contentPane.add(combo, BorderLayout.NORTH);
		
	}

}
