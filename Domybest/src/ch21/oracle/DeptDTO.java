package ch21.oracle;

public class DeptDTO { //데이터 베이스 상에있는 목록들을 (자기가 원하는) 가져와서 저장하기 위한 클래스
private int deptno;
private String dname;
private String loc;
public int getDeptno() {
	return deptno;
}
public void setDeptno(int deptno) {
	this.deptno = deptno;
}
public String getDname() {
	return dname;
}
public void setDname(String dname) {
	this.dname = dname;
}
public String getLoc() {
	return loc;
}
public void setLoc(String loc) {
	this.loc = loc;
}
@Override
public String toString() {
	return "DeptDTO [deptno=" + deptno + ", dname=" + dname + ", loc=" + loc + "]";
}
public DeptDTO(int deptno, String dname, String loc) {
	this.deptno = deptno;
	this.dname = dname;
	this.loc = loc;
}

}
