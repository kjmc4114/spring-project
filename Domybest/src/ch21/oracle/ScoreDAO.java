package ch21.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import DB_Report.DB;

public class ScoreDAO {
	
	
	public int UpdateScore(ScoreDTO dto) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.OracleConn();
			StringBuilder sb = new StringBuilder();
			sb.append("update score set name=? , kor=?, eng=?, mat=? ");
			sb.append(" where student_no=? ");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(5, dto.getStudent_no());
			pstmt.setString(1, dto.getName());
			pstmt.setInt(2, dto.getKor());
			pstmt.setInt(3, dto.getEng());
			pstmt.setInt(4, dto.getMat());
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
			if(pstmt !=null)	pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
			if(conn != null)	conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;

	}
	
	public int insertScore(ScoreDTO dto) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.OracleConn();
			StringBuilder sb = new StringBuilder();
			sb.append("insert into score ");
			sb.append("values (?,?,?,?,?)");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, dto.getStudent_no());
			pstmt.setString(2, dto.getName());
			pstmt.setInt(3, dto.getKor());
			pstmt.setInt(4, dto.getEng());
			pstmt.setInt(5, dto.getMat());
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
			if(pstmt !=null)	pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
			if(conn != null)	conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;

	}

	public Vector listScore() {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			// select student_no, name,kor,eng,mat,(kor+eng+mat) tot,
			// round ( ((kor+eng+mat)/3.0), 2) avg from score;
			conn = DB.OracleConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select student_no, name,kor,eng,mat,(kor+eng+mat) tot,");
			sb.append(" round ( ((kor+eng+mat)/3.0), 2) avg from score");
			sb.append(" order by student_no");
			pstmt = conn.prepareStatement(sb.toString()); // 스트링 빌더는 스트링을 바꿔주는
															// 작업이 필요하다.
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				String student_no = rs.getString("student_no");
				String name = rs.getString("name");
				int kor = rs.getInt("kor");
				int eng = rs.getInt("eng");
				int mat = rs.getInt("mat");
				int tot = rs.getInt("tot");
				double avg = rs.getDouble("avg");
				row.add(student_no);
				row.add(name);
				row.add(kor);
				row.add(eng);
				row.add(mat);
				row.add(tot);
				row.add(avg);
				items.add(row);

			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs!=null)rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(pstmt!=null)pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(conn!=null)conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items;
	}

}
