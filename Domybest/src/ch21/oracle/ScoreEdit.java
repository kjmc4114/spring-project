package ch21.oracle;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ScoreEdit extends JFrame {

	private JPanel contentPane;
	private JTextField tfStudentNo;
	private JTextField tfName;
	private JTextField tfKor;
	private JTextField tfEng;
	private JTextField tfMat;
	private ScoreList parent;
	private ScoreDTO dto;


	public ScoreEdit(ScoreList parent, ScoreDTO dto) {
		this.parent = parent;
		this.dto = dto;
		System.out.println(parent);
		System.out.println(dto); // DTO의 toString 의 메소드의 기능으로 값이 전달됨.
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 353, 470);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("학번");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(0, 21, 99, 29);
		contentPane.add(lblNewLabel);

		tfStudentNo = new JTextField();
		tfStudentNo.setBounds(118, 18, 186, 35);
		contentPane.add(tfStudentNo);
		tfStudentNo.setColumns(10);

		JLabel label = new JLabel("이름");
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setBounds(0, 74, 99, 29);
		contentPane.add(label);

		tfName = new JTextField();
		tfName.setColumns(10);
		tfName.setBounds(118, 71, 186, 35);
		contentPane.add(tfName);

		JLabel label_1 = new JLabel("국어");
		label_1.setHorizontalAlignment(SwingConstants.RIGHT);
		label_1.setBounds(0, 127, 99, 29);
		contentPane.add(label_1);

		tfKor = new JTextField();
		tfKor.setColumns(10);
		tfKor.setBounds(118, 124, 186, 35);
		contentPane.add(tfKor);

		JLabel label_2 = new JLabel("영어");
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);
		label_2.setBounds(0, 180, 99, 29);
		contentPane.add(label_2);

		tfEng = new JTextField();
		tfEng.setColumns(10);
		tfEng.setBounds(118, 177, 186, 35);
		contentPane.add(tfEng);

		JLabel label_3 = new JLabel("수학");
		label_3.setHorizontalAlignment(SwingConstants.RIGHT);
		label_3.setBounds(0, 233, 99, 29);
		contentPane.add(label_3);

		tfMat = new JTextField();
		tfMat.setColumns(10);
		tfMat.setBounds(118, 230, 186, 35);
		contentPane.add(tfMat);

		// dto 값을 텍스트 필드에 출력.
		tfStudentNo.setText(dto.getStudent_no());
		tfName.setText(dto.getName());
		tfKor.setText(dto.getKor() + "");
		tfEng.setText(dto.getEng() + "");
		tfMat.setText(dto.getMat() + "");

		JButton btnUpdate = new JButton("변경");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String student_no = tfStudentNo.getText();
				String name = tfName.getText();
				int kor = Integer.parseInt(tfKor.getText());
				int eng = Integer.parseInt(tfEng.getText());
				int mat = Integer.parseInt(tfMat.getText());
				ScoreDTO dto = new ScoreDTO(student_no, name, kor, eng, mat);
				ScoreDAO dao = new ScoreDAO();
				int result = dao.UpdateScore(dto);
				if (result == 1) {
					JOptionPane.showMessageDialog(ScoreEdit.this, "변경되었습니다.");
					parent.refreshTable();
					dispose();
				}
			}
		});
		btnUpdate.setBounds(0, 302, 153, 37);
		contentPane.add(btnUpdate);

		JButton btnDelete = new JButton("삭제");
		btnDelete.setBounds(167, 302, 153, 37);
		contentPane.add(btnDelete);
	}
}
