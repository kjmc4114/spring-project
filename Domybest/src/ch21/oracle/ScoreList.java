package ch21.oracle;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ScoreList extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private ScoreDAO dao;
	private Vector cols;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ScoreList frame = new ScoreList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void refreshTable() {
		DefaultTableModel model = new DefaultTableModel(dao.listScore(), cols);
		table.setModel(model);

	}

	public ScoreList() {

		dao = new ScoreDAO();
		cols = new Vector();
		cols.add("학번");
		cols.add("이름");
		cols.add("국어");
		cols.add("영어");
		cols.add("수학");
		cols.add("총점");
		cols.add("평균");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		table = new JTable();
		scrollPane.setViewportView(table);
		refreshTable();

		JPanel panel = new JPanel();
		scrollPane.setColumnHeaderView(panel);

		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);

		JButton btnNewButton = new JButton("추가");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ScoreSave frm = new ScoreSave(ScoreList.this); // 그냥 this 라고 할시
																// ActionListener를
																// 가리킴.
				frm.setVisible(true);
				frm.setLocation(200, 200); // 부모창의 왼쪽위 모서리 기준으로 좌푯값이 설정됨.
			}
		});
		panel_1.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("편집");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int idx = table.getSelectedRow();
				if (idx == -1) {
					JOptionPane.showMessageDialog(ScoreList.this, "편집할 자료를 선택하세요");
					return;
				} else {
					String student_no = table.getValueAt(idx, 0) + "";
											//String.valueOf(table.getValueAt(idx,0)
					String name = String.valueOf(table.getValueAt(idx, 1));
					int Kor = Integer.valueOf(table.getValueAt(idx, 2)+"");
					int Eng = Integer.valueOf(table.getValueAt(idx, 3)+"");
					int Mat = Integer.valueOf(String.valueOf(table.getValueAt(idx, 4)));
					ScoreDTO dto = new ScoreDTO(student_no,name,Kor,Eng,Mat);
					ScoreEdit frm = new ScoreEdit (ScoreList.this, dto);
					frm.setVisible(true);
					
				
				}
			}
		});
		panel_1.add(btnNewButton_1);
	}

}
