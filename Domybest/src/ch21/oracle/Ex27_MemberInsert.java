package ch21.oracle;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.Properties;
import java.util.Scanner;

public class Ex27_MemberInsert {
	public static void main(String[] args) throws Exception {
		FileInputStream fis = new FileInputStream("d:\\oracle.prop");
		Properties prop = new Properties();
		prop.load(fis);
		String driver = prop.getProperty("driver");
		String url = prop.getProperty("url");
		String id = prop.getProperty("id");
		String password = prop.getProperty("password");
		Connection conn = null;		
		PreparedStatement pstmt = null; 
		//Statement stmt = null;
		// 그냥 statement = sql 이 완성된후에 인스턴스 생성.
		// prepared 는 틀만 만든후에 인스턴스 생성. 
		try {
			Class.forName(driver); // 생략가능
			conn = DriverManager.getConnection(url, id, password);
			Scanner scan = new Scanner(System.in);
			System.out.println("아이디");
			String userid = scan.nextLine();
			System.out.println("비밀번호");
			String passwd = scan.nextLine();
			System.out.println("이름:");
			String name = scan.nextLine();
			String sql = "insert into member (userid,passwd,name) values (?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userid);
			pstmt.setString(2, passwd);
			pstmt.setString(3, name);
			pstmt.executeUpdate();
						
//			String sql = "insert into member (userid,passwd,name) values" + "('"+userid+"' , '"+passwd+"' , '"+name+"')";
//			stmt = conn.createStatement();
//			stmt.executeUpdate(sql);
			System.out.println("저장되었습니다.");
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(pstmt!=null)
					pstmt.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			try {
				if(conn!=null)
				conn.close();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}
