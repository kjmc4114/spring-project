package ch21.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

public class StudentDAO {

	public int updateStudent(StudentDTO dto) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.HrConn();
			String sql = " update student set name = ?, deptno1 = ? , profno = ? , tel = ? , img_path = ? where studno = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, dto.getName());
			pstmt.setInt(2, dto.getDeptno1());
			pstmt.setInt(3, dto.getProfno());
			pstmt.setString(4, dto.getTel());
			pstmt.setString(5, dto.getImg_path());
			pstmt.setInt(6, dto.getStudno());
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public StudentDTO detailStudent(int studno) {
		StudentDTO dto = null;
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.HrConn();
			StringBuilder sb = new StringBuilder();
			sb.append(
					" select st.studno, st.name sname , dp.deptno, dp.dname , pf.PROFNO , pf.name pname , st.tel, st.img_path");
			sb.append(" from student st, department dp, professor pf ");
			sb.append(" where st.deptno1 = dp.deptno and st.profno = pf.PROFNO(+)  and st.studno = ?");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, studno);
			rs = pstmt.executeQuery();
			if (rs.next()) { // while 문을 써도 상관없음 한싸이클만 돌면 되징.
				dto = new StudentDTO();
				dto.setProfno(rs.getInt("profno"));
				dto.setStudno(rs.getInt("studno"));
				dto.setDeptno1(rs.getInt("deptno"));
				dto.setName(rs.getString("sname"));
				dto.setDname(rs.getString("dname"));
				dto.setPname(rs.getString("pname"));
				dto.setTel(rs.getString("tel"));
				dto.setImg_path(rs.getString("img_path"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return dto;
	}

	public ArrayList<String> listStudent() {
		ArrayList<String> items = new ArrayList<>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.HrConn();
			String sql = "select (studno ||' '|| name) name from student ";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				items.add(rs.getString("name")); // rs.getString (1) 을해도 같은 표현이나
													// 가독성을 위하여 컬럼이름을 설정해줌.
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items;

	}

	public Vector listLecture(int studno) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.HrConn();
			StringBuilder sb = new StringBuilder();
			sb.append(" select st.studno , st.name sname, sb.SUBJECT_NAME , pf.NAME pfname , sb.point , lt.grade");
			sb.append(" from student st,  subject sb, professor pf, lecture lt");
			sb.append(" where st.studno = lt.studno");
			sb.append(" and lt.subject_code = sb.subject_code and pf.profno = sb.profno and st.studno = ?");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, studno);
			rs=pstmt.executeQuery();
			while  (rs.next()){
				Vector row = new Vector();
				row.add(rs.getInt("studno"));
				row.add(rs.getString("sname"));
				row.add(rs.getString("subject_name"));
				row.add(rs.getString("pfname"));
				row.add(rs.getInt("point"));
				row.add(rs.getString("grade"));
				items.add(row);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
		return items;
	}
}
