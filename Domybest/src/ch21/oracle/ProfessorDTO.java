package ch21.oracle;

public class ProfessorDTO {
	private int profno;
	private String name;
	public ProfessorDTO() {
	}
	public ProfessorDTO(int profno, String name) {
		this.profno = profno;
		this.name = name;
	}
	public int getProfno() {
		return profno;
	}
	public void setProfno(int profno) {
		this.profno = profno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() { // dto 를 바로 보내도 스트링 화 시켜줌.
		return "ProfessorDTO [profno=" + profno + ", name=" + name + "]";
	}
	
}
