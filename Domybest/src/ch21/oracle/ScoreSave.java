package ch21.oracle;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ScoreSave extends JFrame {

	private JPanel contentPane;
	private JTextField tfSutdent;
	private JTextField tfName;
	private JTextField tfKor;
	private JTextField tfEng;
	private JTextField tfMat;
	private ScoreList frm;
	

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					ScoreSave frame = new ScoreSave();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	// ScoreList 프레임의 주소값을 받기 위해 매개변수를 가지는 생정자 추가.
	public ScoreSave(ScoreList frm) {
		this();
		this.frm = frm;
	}

	public ScoreSave() {
		// setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 357, 413);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("학번");
		lblNewLabel.setBounds(10, 21, 99, 29);
		contentPane.add(lblNewLabel);

		tfSutdent = new JTextField();
		tfSutdent.setBounds(68, 18, 186, 35);
		contentPane.add(tfSutdent);
		tfSutdent.setColumns(10);

		JLabel label = new JLabel("이름");
		label.setBounds(10, 70, 99, 29);
		contentPane.add(label);

		JLabel lblNewLabel_1 = new JLabel("국어");
		lblNewLabel_1.setBounds(10, 120, 99, 29);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("영어");
		lblNewLabel_2.setBounds(10, 168, 99, 29);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("수학");
		lblNewLabel_3.setBounds(10, 218, 99, 29);
		contentPane.add(lblNewLabel_3);

		JButton btnSave = new JButton("저장");
		btnSave.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String student_no = tfSutdent.getText();
				String name = tfName.getText();
				int kor = Integer.parseInt(tfKor.getText());
				int eng = Integer.parseInt(tfEng.getText());
				int mat = Integer.parseInt(tfMat.getText());
				ScoreDTO dto = new ScoreDTO(student_no, name, kor, eng, mat);
				ScoreDAO dao = new ScoreDAO();
				int result = dao.insertScore(dto);
				if (result == 1) {
					JOptionPane.showMessageDialog(ScoreSave.this, "저장되었습니다.");
					//frm = new ScoreList 으로 했을시 아예 새로운 프레임을 띄우는 것과 같기 떄문에 매개변수를 이용하여 띄워야함. 
					frm.refreshTable();
					dispose();// 현재 창을 닫음.
				}
			}
		});
		btnSave.setBounds(63, 274, 153, 37);
		contentPane.add(btnSave);

		tfName = new JTextField();
		tfName.setBounds(68, 71, 186, 35);
		contentPane.add(tfName);
		tfName.setColumns(10);

		tfKor = new JTextField();
		tfKor.setColumns(10);
		tfKor.setBounds(68, 120, 186, 35);
		contentPane.add(tfKor);

		tfEng = new JTextField();
		tfEng.setColumns(10);
		tfEng.setBounds(68, 170, 186, 35);
		contentPane.add(tfEng);

		tfMat = new JTextField();
		tfMat.setColumns(10);
		tfMat.setBounds(68, 218, 186, 35);
		contentPane.add(tfMat);
	}
}
