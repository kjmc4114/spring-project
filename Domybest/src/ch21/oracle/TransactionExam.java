package ch21.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TransactionExam {
	public static void main(String[] args) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		boolean success = false;
		try {
			conn = DB.HrConn();
			//conn.setAutoCommit(true); // 이게 항상 기본 설정이 되어있음.
			conn.setAutoCommit(false);
			StringBuilder sb = new StringBuilder();
			sb.append("insert into emp_copy (empno, ename, sal)");
			sb.append("values (?,?,?)");
			pstmt = conn.prepareStatement(sb.toString());
			long start = System.currentTimeMillis(); // 현재시각
			for (int i = 1; i <= 100000; i++) {
				pstmt.setInt(1, i);
				pstmt.setString(2, "kim");
				pstmt.setInt(3, 4500);
				System.out.println(i);
				//pstmt.executeQuery(); DB에 10만건의 요청을 하기 때문에 서버에 상당히 부하가 걸린다.
				pstmt.addBatch();
			}
			pstmt.executeBatch();
			success = true;
			long end = System.currentTimeMillis();
			System.out.println("작업수행 시간 :" + (end - start) +"초");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (success) {
					System.out.println("커밋되었습니다");
					conn.commit();
				}else {
					System.out.println("\n롤백되었습니다.");
					conn.rollback();
				} 
			} catch (Exception e2) {
				e2.printStackTrace();
			}

			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
