package ch21.oracle;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

public class TabbedPane extends JFrame {

	private JPanel contentPane;
	private JList list;
	private JTabbedPane tabbedPane;
	private StudentDAO studentDao;
	private DepartmentDAO departmentDao;
	private ProfessorDAO professorDao;
	private StudentDTO dto;
	private String img_path;
	private ArrayList<DepartmentDTO> departmentlist;
	private ArrayList<ProfessorDTO> professorlist;
	private JPanel panel;
	private JPanel panel_1;
	private JLabel lblimage;
	private JTextField tfName;
	private JTextField tfStudno;
	private JTextField tfTel;
	private JComboBox cboDeptno;
	private JComboBox cboProfno;
	private JScrollPane scrollPane_1;
	private JTable table;
	private Vector cols;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TabbedPane frame = new TabbedPane();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TabbedPane() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 713, 512);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		panel = new JPanel();
		tabbedPane.addTab("학생정보", null, panel, null);
		panel.setLayout(null);

		lblimage = new JLabel("");
		lblimage.setHorizontalAlignment(SwingConstants.CENTER);
		lblimage.setBounds(22, 32, 137, 178);
		panel.add(lblimage);

		JLabel lblNewLabel_1 = new JLabel("이름");
		lblNewLabel_1.setBounds(181, 32, 99, 29);
		panel.add(lblNewLabel_1);

		tfName = new JTextField();
		tfName.setBounds(302, 29, 186, 35);
		panel.add(tfName);
		tfName.setColumns(10);

		tfStudno = new JTextField();
		tfStudno.setColumns(10);
		tfStudno.setBounds(302, 79, 186, 35);
		panel.add(tfStudno);

		JLabel label = new JLabel("학번");
		label.setBounds(181, 82, 99, 29);
		panel.add(label);

		JLabel label_1 = new JLabel("학과");
		label_1.setBounds(181, 133, 99, 29);
		panel.add(label_1);

		JLabel label_2 = new JLabel("지도교수");
		label_2.setBounds(181, 181, 99, 29);
		panel.add(label_2);

		tfTel = new JTextField();
		tfTel.setColumns(10);
		tfTel.setBounds(302, 227, 186, 35);
		panel.add(tfTel);

		JLabel label_3 = new JLabel("전화번호");
		label_3.setBounds(181, 230, 99, 29);
		panel.add(label_3);

		cboDeptno = new JComboBox();
		cboDeptno.setBounds(302, 130, 186, 32);
		panel.add(cboDeptno);

		cboProfno = new JComboBox();
		cboProfno.setBounds(302, 178, 186, 35);
		panel.add(cboProfno);

		JButton btnSave = new JButton("저장");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				StudentDTO dto = new StudentDTO();
				dto.setStudno(Integer.parseInt(tfStudno.getText())); // 학번은 고치면
																		// 안됩니다.
				dto.setName(tfName.getText());
				dto.setTel(tfTel.getText());
				int index = cboDeptno.getSelectedIndex();
				dto.setDeptno1(departmentlist.get(index).getDeptno());
				index = cboProfno.getSelectedIndex() - 1; // 미배정을 추가하였기 떄문에.
				if (index != -1) {
					dto.setProfno(professorlist.get(index).getProfno());
				}

				dto.setImg_path(img_path);
				int result = studentDao.updateStudent(dto);
				if (result == 1) {
					JOptionPane.showMessageDialog(TabbedPane.this, "저장되었습니다");
				}
				
			}
		});
		btnSave.setBounds(181, 331, 153, 37);
		panel.add(btnSave);

		JButton btnImage = new JButton("사진등록");
		btnImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser(); // 파일을 선택하게 해주는 클래스
				int result = fc.showOpenDialog(TabbedPane.this);
				if (result == JFileChooser.APPROVE_OPTION) { // 파일을 선택한다면.
					img_path = fc.getSelectedFile().getAbsolutePath(); // 절대경로를
																		// 가져온다.
					System.out.println(img_path);
					File file = fc.getSelectedFile();
					try {
						ImageIcon icon = new ImageIcon(ImageIO.read(file));
						Image imageSrc = icon.getImage(); // 원본이미지
						BufferedImage bi = ImageIO.read(file);
						File copyFile = new File("test.jpg");
						ImageIO.write(bi, "jpg", copyFile);
						Image imageNew = imageSrc.getScaledInstance(80, 100, Image.SCALE_AREA_AVERAGING); // 사이즈가
																											// 조정된
																											// 이미지
						icon = new ImageIcon(imageNew);
						lblimage.setIcon(icon); // 라벨에 이미지를 아이콘으로 표시.
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					
				}
			}
		});
		btnImage.setBounds(22, 222, 144, 37);
		panel.add(btnImage);

		panel_1 = new JPanel();
		tabbedPane.addTab("수강정보", null, panel_1, null);
		panel_1.setLayout(null);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(0, 5, 511, 383);
		panel_1.add(scrollPane_1);
		
		cols = new Vector();
		cols.add("학번");
		cols.add("이름");
		cols.add("과목이름");
		cols.add("교수이름");
		cols.add("학점");
		cols.add("등급");
		
	
		
		
		
		table = new JTable();
		scrollPane_1.setViewportView(table);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.WEST);
		studentDao = new StudentDAO();
		departmentDao = new DepartmentDAO();
		professorDao = new ProfessorDAO();
		professorlist = professorDao.listProfessor();
		departmentlist = departmentDao.listDepartment();
		ArrayList<String> studentList = studentDao.listStudent(); // 리스트에 집어
																	// 넣었다. dao의
																	// 학과목록을
																	// 받아오는 리스트를
																	// 집어 넣은것.

		list = new JList(studentList.toArray()); // Jlist 에는 배열이 들어가야함으로, 리스트를
													// 배열로 바꾸는 메소드를 호출하여야한다.
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				// JList 에서 선택한 값을 저장.
				String str = list.getSelectedValue().toString(); // 리턴값이 E 라는 것은
																	// Obejct 란
																	// 뜻으로 구체화가
																	// 필요하다.
				System.out.println(str);
				String[] arr = str.split(" ");
				// 토드상에서 합친 기준을 따라서 나눈다. select (studno ||' '|| name) name from
				// student; 이 부분을 기억할 것.
				int studno = Integer.valueOf(arr[0]);
				DefaultTableModel model = new DefaultTableModel(studentDao.listLecture(studno),cols);
				table.setModel(model);
				dto = studentDao.detailStudent(studno); // 학번, 이름 , 전화번호를 가져옴.
				if (dto != null) {
					tfStudno.setText(String.valueOf(studno));
					tfName.setText(dto.getName());
					tfTel.setText(dto.getTel());

					// 학과 목록을 출력
					int cboIndex = 0;
					int count = 0;
					cboDeptno.removeAllItems(); // 콤보박스를 초기화
					for (DepartmentDTO dto2 : departmentlist) { // dto2 는 중복이름이
																// 아님.
						cboDeptno.addItem(dto2.getDname());
						if (dto.getDeptno1() == dto2.getDeptno()) { // 학생의 학과
																	// 코드와 , 지금의
																	// 학생코드가
																	// 맞으면.
							cboIndex = count; // 학생의 학과 인덱스 저장
						}
						count++; // 몇번째 콤보박스에 있을지.를 찾는것.
					}
					cboDeptno.setSelectedIndex(cboIndex); // 학생의 학과 선택

					cboIndex = 0;
					boolean assign = false;
					cboProfno.removeAll(); // 콤보박스를 한번 클리어 시켜야함.
					cboProfno.addItem("미배정");
					for (int i = 0; i < professorlist.size(); i++) {
						int profno = professorlist.get(i).getProfno(); // i번째
																		// 넘버를
																		// 가져옴.
						String name = professorlist.get(i).getName(); // i번째 이름.
						cboProfno.addItem(name); // 콤보박스에는 이름만 출력됨.
						if (profno == dto.getProfno()) { // 학생의 no 와 교수님의 no 가
															// 같은 시점.
							assign = true; // 있으면 트루
							cboIndex = i; // 에 있는 콤보박스

						}
					}
					// 지도교수가 미배정일 경우
					if (assign) {
						cboProfno.setSelectedIndex(cboIndex + 1); // 를 선택하게됨.
						System.out.println(assign);
					} else { // 없으면 false
						System.out.println(assign);
						cboProfno.setSelectedIndex(0);

					}
					//사진표시
					img_path = dto.getImg_path();
					if (img_path == null) {
						lblimage.setText("사진미등록");
						lblimage.setIcon(null);
					} else {
						lblimage.setText("");
						ImageIcon icon = new ImageIcon(img_path);
						Image imageSrc = icon.getImage();
						Image imageNew = imageSrc.getScaledInstance(80,100,Image.SCALE_AREA_AVERAGING);
						icon = new ImageIcon(imageNew);
						lblimage.setIcon(icon);
					}
					
				}
			}
		});
	
		
		list.setFont(new Font("굴림", Font.PLAIN, 20));
		scrollPane.setViewportView(list);
		
		
	}
}
