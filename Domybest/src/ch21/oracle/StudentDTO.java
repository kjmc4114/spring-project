package ch21.oracle;

public class StudentDTO {
private int studno;
private String name;
private int grade;
private String jumin;
private String tel;
private int deptno1;
private int deptno2; // 부전공
private int profno; // 지도교수 사번
// 조인의 결과물도 최종적으로 담을 수 있게 한다.
private String dname; //학과명 - 학과코드를 명으로 바꾸기위해서 조인 할것.
private String pname; // 교수이름 - 교수코드를 교수 명으로 바꾸기 위해서 조인할것.
private String img_path;

 public StudentDTO() {
}

public int getStudno() {
	return studno;
}

public void setStudno(int studno) {
	this.studno = studno;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getGrade() {
	return grade;
}

public void setGrade(int grade) {
	this.grade = grade;
}

public String getJumin() {
	return jumin;
}

public void setJumin(String jumin) {
	this.jumin = jumin;
}

public String getTel() {
	return tel;
}

public void setTel(String tel) {
	this.tel = tel;
}

public int getDeptno1() {
	return deptno1;
}

public void setDeptno1(int deptno1) {
	this.deptno1 = deptno1;
}

public int getDeptno2() {
	return deptno2;
}

public void setDeptno2(int deptno2) {
	this.deptno2 = deptno2;
}

public int getProfno() {
	return profno;
}

public void setProfno(int profno) {
	this.profno = profno;
}

public String getDname() {
	return dname;
}

public void setDname(String dname) {
	this.dname = dname;
}

public String getPname() {
	return pname;
}

public void setPname(String pname) {
	this.pname = pname;
}

public String getImg_path() {
	return img_path;
}

public void setImg_path(String img_path) {
	this.img_path = img_path;
}

public StudentDTO(int studno, String name, int grade, String jumin, String tel, int deptno1, int deptno2, int profno,
		String dname, String pname, String img_path) {
	this.studno = studno;
	this.name = name;
	this.grade = grade;
	this.jumin = jumin;
	this.tel = tel;
	this.deptno1 = deptno1;
	this.deptno2 = deptno2;
	this.profno = profno;
	this.dname = dname;
	this.pname = pname;
	this.img_path = img_path;
}

@Override
public String toString() {
	return "StudentDTO [studno=" + studno + ", name=" + name + ", grade=" + grade + ", jumin=" + jumin + ", tel=" + tel
			+ ", deptno1=" + deptno1 + ", deptno2=" + deptno2 + ", profno=" + profno + ", dname=" + dname + ", pname="
			+ pname + ", img_path=" + img_path + "]";
}
 
}
