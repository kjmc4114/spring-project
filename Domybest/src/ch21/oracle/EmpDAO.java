package ch21.oracle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class EmpDAO {
//	public ArrayList<EmpDTO> listEmp(String dname) { // 콤보박스용으로 묶어 놓기;
		public ArrayList<EmpDTO> listEmp(int deptno) {
		ArrayList<EmpDTO> items = new ArrayList<>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.HrConn();
			StringBuilder sb = new StringBuilder();
			sb.append(" select empno, ename, e.deptno, dname from");  
			sb.append(" emp e, dept d where e.deptno = d.deptno ");
			sb.append(" and e.deptno = ? ");
			sb.append(" order by ename");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setInt(1, deptno);
			rs = pstmt.executeQuery();
			while (rs.next()){
				int empno = rs.getInt("empno");
				String ename = rs.getString("ename");
				EmpDTO dto = new EmpDTO();
				dto.setEmpno(empno);
				dto.setEname(ename);
				items.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs!=null)rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(pstmt!=null)pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(conn!=null)conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	return items;
	}
	
}
