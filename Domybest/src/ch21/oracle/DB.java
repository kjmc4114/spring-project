package ch21.oracle;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DB {
	public static Connection HrConn(){
		Connection conn = null;
		try{
			FileInputStream fis = new FileInputStream("d:\\hr.prop");
			Properties prop = new Properties();
			prop.load(fis);
			String url = prop.getProperty("url");
			String id = prop.getProperty("id");
			String password = prop.getProperty("password");
			conn=DriverManager.getConnection(url,id,password);
		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}
	
	
	
	
	public static Connection OracleConn(){
		Connection conn = null;
		try{
			FileInputStream fis = new FileInputStream("d:\\oracle.prop");
			Properties prop = new Properties();
			prop.load(fis);
			String url = prop.getProperty("url");
			String id = prop.getProperty("id");
			String password = prop.getProperty("password");
			conn=DriverManager.getConnection(url,id,password);
		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}
	
	public static Connection dbConn(){
		Connection conn = null;
		try{
			FileInputStream fis = new FileInputStream("d:\\db.prop");
			Properties prop = new Properties();
			prop.load(fis);
			String url = prop.getProperty("url");
			String id = prop.getProperty("id");
			String password = prop.getProperty("password");
			conn=DriverManager.getConnection(url,id,password);
		}catch(Exception e){
			e.printStackTrace();
		}
		return conn;
	}

}
