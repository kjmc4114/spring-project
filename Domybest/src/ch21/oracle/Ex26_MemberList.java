package ch21.oracle;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Ex26_MemberList {
public static void main(String[] args) {
	String driver = "oracle.jdbc.driver.OracleDriver";
				//         여기까지 공통 : 포트번호 : 서비스이름 (우리는 XE 정식은 다름)
	String url = "jdbc:oracle:thin:@localhost:1521:xe";
	String id = "java";
	String pwd = "java1234";
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	try {
		Class.forName(driver);
		conn = DriverManager.getConnection(url,id,pwd);
		System.out.println("오라클에 접속되었습니다.");
		String sql = "select * from member";
		pstmt = conn.prepareStatement(sql);
		rs = pstmt.executeQuery();
		System.out.println("아이디\t이름\t비번");
		while (rs.next()){
			String userid = rs.getString("userid");
			String passwd = rs.getString("passwd");
			String name = rs.getString("name");
			System.out.println(userid+"\t"+passwd+"\t"+name);
					
		}
	} catch (Exception e) {
		e.printStackTrace();
	}finally {
		try {
			if(rs!=null)rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if(pstmt!=null)pstmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			if(conn != null)conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
}
