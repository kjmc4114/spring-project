package ch18;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JApplet;

public class Ex05_Shapes  extends JApplet{
	@Override
	public void init() {
		setSize(300,300);
	}
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.cyan);
		g.drawLine(10, 10, 200, 10);
		Graphics2D g2 = (Graphics2D)g; // 형변환
		g2.setStroke(new BasicStroke(10));// 선 굵기 조절
		g.drawRect(120,40,90,55);
		g.fillRect(120,140,90,55);
		g.setColor(Color.darkGray);
		g.fillRoundRect(120, 240, 90, 50, 40, 40);
		g.drawOval(0, 40, 90, 55);
		g.fillOval(0, 140, 90, 55);
		int[] x={10,30,50,40,20};
		int[] y={257,240,257,280,280};
		g.setColor(Color.RED);
		g.fillPolygon(x,y,x.length);
		
	}

}
