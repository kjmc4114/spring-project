package ch18;

import java.applet.AudioClip;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JApplet;
import javax.swing.JButton;

public class Ex07_Sound extends JApplet implements ActionListener {
	private AudioClip audio;
	private JButton play, stop, loop;

	@Override
	public void init() {
		setLayout(new FlowLayout());
		play = new JButton("play");
		stop = new JButton("stop");
		loop = new JButton("loop");
		add(play);
		add(stop);
		add(loop);
		System.out.println(getClass());
		play.addActionListener(this);
		stop.addActionListener(this);
		loop.addActionListener(this);
		// 중복코드를 제거하는 과정이 필요함. implements ActionListener
		// play.addActionListener(new ActionListener() {
		//
		// @Override
		// public void actionPerformed(ActionEvent e) {
		// System.out.println("play");
		// audio.play();
		//
		// }
		// });
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println(e);

		System.out.println(e.getSource()); // 버튼의 e 참조변수 주소값을 가리킴 버튼을 판별할때 쓰는
											// 방법임으로 잘 알아 둘것.

		System.out.println(e.getActionCommand()); // 버튼의 레이블을 가리킴
		audio = getAudioClip(getClass().getResource("shapeofyou.wav"));

		JButton btn = (JButton) e.getSource();

		if (btn == play) {
			audio.play();
		} else if (btn == stop) {
			audio.stop();
		} else if (btn == loop){
			audio.loop();
		}
	}

}
