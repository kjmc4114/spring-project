package ch18;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;

import javax.swing.JFrame;

public class Ex02_PaintExam extends JFrame {

	public Ex02_PaintExam() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300,300);
		setVisible(true);
		Container c = getContentPane();
		Graphics g = c.getGraphics();
	}
@Override
public void paint(Graphics g) {
	// TODO Auto-generated method stub
	super.paint(g);
	g.drawString("hellow java", 20, 100);
	g.setColor(Color.YELLOW);
	//g.fillRect(100, 100, 50, 50);
	g.drawRect(100, 100, 50, 50);
}

public static void main(String[] args) {
	new Ex02_PaintExam();
	
}	
}
