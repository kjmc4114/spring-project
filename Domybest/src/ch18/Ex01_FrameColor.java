package ch18;

import java.awt.Color;
import java.awt.Container;

import javax.swing.JFrame;

public class Ex01_FrameColor extends JFrame {
public Ex01_FrameColor() throws InterruptedException {
setVisible(true);
Container c = getContentPane();
for(int i = 0; i<=255; i++){
	setSize(i*5, i);
	setLocation(i*30, i);
	c.setBackground(new Color(i,0,0));

	Thread.sleep(10); //자세한 방법은 try catch , 간단한 방법은 throws로 해도 무관함. throws는 메인과, 생성자 둘다 필요함.
} 
	setDefaultCloseOperation(EXIT_ON_CLOSE); //모든창을 닫음
	//setDefaultCloseOperation(HIDE_ON_CLOSE); // 숨김처리 (기본값)
	//setDefaultCloseOperation(DISPOSE_ON_CLOSE); //현재창만 닫음
}	
public static void main(String[] args) throws InterruptedException {
	new Ex01_FrameColor();
}
}
