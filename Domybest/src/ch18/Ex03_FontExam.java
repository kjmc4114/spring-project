package ch18;

import java.applet.Applet;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

public class Ex03_FontExam extends Applet{
private Font font;
private FontMetrics fm;
private int x,y;
private String message;
private Dimension dim; // 화면의 가로 세로 길이를 저장하는 객체
@Override
	public void init() {
		message = "그래픽 테스트";
		font = new Font("굴림",Font.ITALIC, 30);
		fm = getFontMetrics(font);
		setSize(500,500); // 애플릿의 화면크기
		dim=getSize(); // 화면크기 계산
		x=(dim.width/2)-(fm.stringWidth(message)/2);
		y=(dim.height/2) - (fm.getDescent()/2);
	}
@Override
	public void paint(Graphics g) {
	 super.paint(g);
	 g.setFont(font);
	 g.drawString(message,x,y);
	 g.drawRect(x, y, 100, 200);
	}
}
