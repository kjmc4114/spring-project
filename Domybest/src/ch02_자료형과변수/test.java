package ch02_자료형과변수;
//main, 컨트롤+스페이스 => main method 작성
//sysout, 컨트롤+스페이스 => println 작성

public class test {

static int n; //전역변수 기본값이 자동으로 설정



static void test2(){
	int a=2; //지역변수 기본값이 없으면 사용할 수 없음.
	System.out.println(a);
}


public static void main(String[] args) {
	//컴파일러는 main을 제일  먼저 찾는다. 
	//final=상수
	char d='e'; //작은따옴표로 묶는 문자1개
	/*char b='AB';*/ // 문자1개 밖에 못함
	String e="AB"; /*문자열은 String으로 함*/
    final int c=10;
	System.out.println(c);
	System.out.println((int)d); //문자코드
	test2();
	System.out.println("\"good\"");

}
}
