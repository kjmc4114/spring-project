package REPROT;

import java.util.Scanner;

public class Circle_use {
private double r;
private double circum;
private double extent;

public double input(){
	System.out.println("반지름을 입력하세요");
	Scanner input=new Scanner(System.in);
	this.r=input.nextDouble();
	return r;
}

public double getR() {
		
	return r;
}
public void setR(double r) {
	
	this.r = r;
}
public double getCircum() {
	circum = 2*r*Math.PI;
	return circum;
}
public void setCircum(double circum) {
	
	this.circum = circum;
	
}
public double getExtent() {
	extent=r*r*Math.PI;
	return extent;
}
public void setExtent(double extent) {
	
	this.extent = extent;
}

public void print(){
	System.out.println("반지름:"+r+"\n"+"둘레:"+String.format("%.2f",getCircum())+"\n"+"넓이 :"+String.format("%.2f",getExtent()));
	
}

}
