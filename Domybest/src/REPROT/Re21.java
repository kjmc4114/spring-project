package REPROT;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class AddressBook {
	private String name;
	private String address;
	private String hand;
	private String email;
	Scanner input = new Scanner(System.in);
	public AddressBook() {
	}

	public AddressBook(String name, String address, String hand, String email) {
		this.name = name;
		this.address = address;
		this.hand = hand;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
	
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
	
		this.address = address;
	}

	public String getHand() {
		return hand;
	}

	public void setHand(String hand) {
		
		this.hand = hand;
		
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "AddressBook [name=" + name + ", address=" + address + ", hand=" + hand + ", email=" + email + ", input="
				+ input + "]";
	}
   
}

public class Re21 {
public static void main(String[] args) {
	List<AddressBook> list = new ArrayList<>();
	
	AddressBook[] ad3 ={
			new AddressBook("kim","서울구로구","010-1010-2","kim@nate.com"),
			new AddressBook("kim","서울구로구","010-1010-2","kim@nate.com"),
			new AddressBook("kim","서울구로구","010-1010-2","kim@nate.com"),
			new AddressBook("kim","서울구로구","010-1010-2","kim@nate.com"),
			new AddressBook("kim","서울구로구","010-1010-2","kim@nate.com"),
		};
	
	
	
	AddressBook ad1 = new AddressBook();
	ad1.setName("교수님");
	ad1.setHand("010-1234-5678");
	ad1.setAddress("서울강북구");
	ad1.setEmail("DDaDDable@naver.com");
	list.add(ad1);
	
	AddressBook ad2 = new AddressBook("아이유","서울성북구","010-5797-0429","IU@naver.com");
	list.add(ad2);
	list.add(new AddressBook("여민이형", "서울용산구", "010-2154-8774", "goldensujer@hannam.com"));
	list.add(new AddressBook("대균이형", "의정부시", "010-8785-5748", "kyobba@ppap.com"));
	list.add(new AddressBook("창훈이형", "서울중구", "010-6581-3564", "Biggestbro@soju.com"));
	list.add(new AddressBook("이쁜애", "천호동", "010-1281-3564", "2bbuknee@Ssagaji.com"));
	list.add(new AddressBook("푸르릉", "공릉동", "010-1281-3564", "dolgame@mania.com"));
	list.add(new AddressBook("기무기문", "학원근처", "010-1281-3564", "nunchi@bojima.com"));
	
	System.out.println("이름\t주소\t전화번호\t\t이메일"); 
	for(int i = 0;  i<list.size(); i++){ 
		AddressBook m = list.get(i);
		System.out.println(m.getName()+"\t"+m.getAddress()+"\t"+m.getHand()+"\t"+ m.getEmail());
		
		
	}
	
}
}
