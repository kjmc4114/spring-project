package REPROT;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Re25 extends JFrame {

	private JPanel contentPane;
	private JTextField tf;
	private JButton btnRest;
	private JButton btnExit;
	private JLabel lblresult;
	private Random random;
	private int cpu;
	private int input;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Re25 frame = new Re25();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Re25() {
		setTitle("아이유의 숫자게임");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		Random random = new Random();
		cpu = random.nextInt(100) + 1;

		JLabel lblTitle = new JLabel("띵커바웃넘버");
		lblTitle.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTitle.setBounds(12, 38, 136, 34);
		contentPane.add(lblTitle);

		tf = new JTextField();
		tf.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				tf.setText("");
			}
		});
		tf.setText("1부터 100까지");
		tf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				input = Integer.parseInt(tf.getText());

				if (input > cpu) {  // 중복되는 부분을 메서드로 만들면 좋다. 
					setting();
					lblresult.setText("좀더 작은수를 생각하시오");
					lblresult.setBackground(new Color(68, 168, 201));
			

				} else if (input < cpu) {
					setting();
					lblresult.setText("좀더 큰수를 생각하시오");
					lblresult.setBackground(new Color(20,135, 34));
			
				} else if (input == cpu) {
					setting();
					lblresult.setText("정답입니다.");
					lblresult.setBackground(new Color(256, 256, 256));
			
				}

			}
		});
		tf.setBounds(160, 42, 185, 27);
		contentPane.add(tf);
		tf.setColumns(10);

		lblresult = new JLabel("힌트 박스");
		lblresult.setBounds(111, 124, 185, 34);
		contentPane.add(lblresult);

		btnRest = new JButton("다시한번");
		btnRest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cpu = random.nextInt(100) + 1;
				tf.setText("");
				lblresult.setOpaque(false);
				lblresult.setBackground(new Color(20,135, 34));
				tf.requestFocus();
			}
		});
		btnRest.setBounds(56, 184, 136, 41);
		contentPane.add(btnRest);

		btnExit = new JButton("종료");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(229, 184, 126, 41);
		contentPane.add(btnExit);
	
	
	
	}

	void setting(){		
		
	lblresult.setOpaque(true);
	lblresult.setForeground(Color.white);
		
	}
}
