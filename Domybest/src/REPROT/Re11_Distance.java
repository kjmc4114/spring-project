package REPROT;

import java.util.Scanner;

public class Re11_Distance {
private int [] A = new int[2];
private int [] B = new int[2];
private double dis;
Scanner input = new Scanner(System.in);

public void input(){
System.out.println("A의 X좌표를 입력하세요");
A[0]=input.nextInt();
System.out.println("A의 Y좌표를 입력하세요");
A[1]=input.nextInt();
System.out.println("B의 X좌표를 입력하세요");
B[0]=input.nextInt();
System.out.println("B의 Y좌표를 입력하세요");
B[1]=input.nextInt();
}

public void setClac(){
	dis=Math.sqrt((Math.abs(B[0]-A[0])*Math.abs(B[0]-A[0]))+
			(Math.abs(B[1]-A[1])*Math.abs(B[1]-A[1])));

}
public double getClac(){
	return dis;
}

@Override
public String toString() {
	String rlt= "A의 좌표:"+"("+A[0]+","+A[1]+")"+"\n"+
"B의 좌표:"+"("+B[0]+","+B[1]+")"+"\n"+String.format("%.2f", getClac());
	return rlt;
}



}
