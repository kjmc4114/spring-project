package REPROT;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class Salary29 extends JFrame {

	private JPanel contentPane;
	private JTextField tfBasic;
	private JTextField tfBonus;
	private JTextField tfTax;
	private JLabel lblResult;
	private int basic, bonus, tax, b, total;

	/**
	 * Launch the application.
	 */
	void calc() {
//		if(tfBonus.getText().equals("")) {
//			JOptionPane.showMessageDialog(Salary29.this, "보너스를 입력하세요.");
//			return;
//		}
		total = basic + bonus - tax;
		lblResult.setText(total+"");}
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Salary29 frame = new Salary29();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Salary29() {
		setTitle("급여계산");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 297, 474);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("성명");
		lblNewLabel.setBounds(54, 36, 57, 15);
		contentPane.add(lblNewLabel);

		JComboBox cboName = new JComboBox();
		cboName.setModel(new DefaultComboBoxModel(new String[] { "", "이부장", "김과장", "홍대리", "최주임", "박사원" }));
		cboName.setBounds(138, 33, 86, 21);
		contentPane.add(cboName);

		JLabel label_4 = new JLabel("직급");
		label_4.setBounds(54, 75, 57, 15);
		contentPane.add(label_4);

		JComboBox cbo = new JComboBox();
		cbo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				int num = cbo.getSelectedIndex();
				if (e.getStateChange() == ItemEvent.SELECTED) {
					if (cbo.getSelectedIndex() == 1) {
						tfBasic.setText("3000000");
					} else if (cbo.getSelectedIndex() == 2) {
						tfBasic.setText("2500000");
					} else if (cbo.getSelectedIndex() == 3) {
						tfBasic.setText("2000000");
					} else if (cbo.getSelectedIndex() == 4) {
						tfBasic.setText("1500000");
					} else if (cbo.getSelectedIndex() == 5) {
						tfBasic.setText("1000000");
					}
				}
				tfBonus.requestFocus();
			}
		});

		cbo.setModel(new DefaultComboBoxModel(new String[] { "직함", "부장", "과장", "대리", "주임", "사원" }));
		cbo.setBounds(138, 72, 86, 21);
		contentPane.add(cbo);

		JLabel lblNewLabel_1 = new JLabel("기본급");
		lblNewLabel_1.setBounds(54, 117, 57, 15);
		contentPane.add(lblNewLabel_1);

		tfBasic = new JTextField();
		tfBasic.setBounds(138, 114, 86, 21);
		contentPane.add(tfBasic);
		tfBasic.setColumns(10);

		JLabel label = new JLabel("보너스");
		label.setBounds(54, 159, 57, 15);
		contentPane.add(label);

		tfBonus = new JTextField();
		tfBonus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tfBonus.getText();
			}
		});
		tfBonus.setColumns(10);
		tfBonus.setBounds(138, 156, 86, 21);
		contentPane.add(tfBonus);

		JLabel label_1 = new JLabel("세율");
		label_1.setBounds(54, 200, 57, 15);
		contentPane.add(label_1);

		JComboBox cboTax = new JComboBox();
		cboTax.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (!tfBasic.getText().equals("")) {
					basic = Integer.parseInt(tfBasic.getText());
				}
				if (!tfBonus.getText().equals("")) {
					bonus = Integer.parseInt(tfBonus.getText());
				}
				if (e.getStateChange() == ItemEvent.SELECTED) {
					b = (basic + bonus) / 100;
					if (cboTax.getSelectedIndex() == 1) {
						tax = b * 0;
						tfTax.setText(tax + "");
					} else if (cboTax.getSelectedIndex() == 2) {
						tax = b * 3;
						tfTax.setText(tax + "");
					} else if (cboTax.getSelectedIndex() == 3) {
						tax = b * 5;
						tfTax.setText(tax + "");
					}
				}
				calc();
			}
		});
		cboTax.setModel(new DefaultComboBoxModel(new String[] { "세율", "0%", "3%", "5%" }));
		cboTax.setBounds(138, 197, 86, 21);
		contentPane.add(cboTax);
		JButton button2 = new JButton("취소");
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tfBasic.setText("");
				tfBonus.setText("");
				tfTax.setText("");
				lblResult.setText("");
				cboName.setSelectedIndex(0);
				cbo.setSelectedIndex(0);
				cboTax.setSelectedIndex(0);
				return;
			}
		});
		tfTax = new JTextField();
		tfTax.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				calc();
			}
		});
		tfTax.setColumns(10);
		tfTax.setBounds(138, 238, 86, 21);
		contentPane.add(tfTax);

		JLabel label_2 = new JLabel("세액");
		label_2.setBounds(54, 241, 57, 15);
		contentPane.add(label_2);

		JButton button1 = new JButton("계산");
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				calc();
				if (tfBonus.getText().equals("")) {
					JOptionPane.showMessageDialog(Salary29.this, "보너스를 입력하세요.");
					return;
				}
			}
		});
		button1.setBounds(44, 282, 86, 23);
		contentPane.add(button1);

		
		button2.setBounds(138, 282, 86, 23);
		contentPane.add(button2);

		JLabel label_3 = new JLabel("실수령액");
		label_3.setBounds(54, 334, 57, 15);
		contentPane.add(label_3);

		lblResult = new JLabel("");
		lblResult.setBounds(138, 334, 86, 15);
		contentPane.add(lblResult);

	}

}
