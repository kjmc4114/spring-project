package REPROT;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class Re30 extends JFrame {

	private JPanel contentPane;
	private JTextField tf;
	private JTextArea ta;
	private JButton btnCheck;
	private JButton btnArry;
	private int[] num = new int[6];
	private BufferedReader reader = null;
	private BufferedWriter writer = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Re30 frame = new Re30();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Re30() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		tf = new JTextField();
		tf.setBounds(22, 21, 186, 35);
		contentPane.add(tf);
		tf.setColumns(10);

		btnCheck = new JButton("확인");
		btnCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String file2 = "D:\\Ted.txt";
				String str = "";
				try {
					reader = new BufferedReader(new InputStreamReader(new FileInputStream(file2)));
					ta.setText("");

					int i = 0;
					while (true) {
						str = reader.readLine();
						if (str == null)
							break;
						num[i] = Integer.parseInt(str);
						i++;
//						System.out.println(i);
//						System.out.println(str);
						ta.append(str + "\n");
					}
					System.out.println(num[0]);
					System.out.println(num[1]);
					System.out.println(num[2]);
					System.out.println(num[3]);
					System.out.println(num[4]);
					System.out.println(num[5]);
				}

				catch (Exception e2) {
					e2.printStackTrace();
				} finally {
					try {
						if (reader != null) {
							reader.close();
						}

					} catch (IOException e1) {

						e1.printStackTrace();
					}
				}
			

		}
	});
		
		btnCheck.setBounds(215, 20, 91, 37);
		contentPane.add(btnCheck);
		this.getRootPane().setDefaultButton(btnCheck);
		btnArry = new JButton("정렬");
		btnArry.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String file = "d:\\b.txt";
				try {
					writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
					int temp = 0;
					for (int i = 0; i < num.length; i++) {
						for (int j = i + 1; j < num.length; j++) {
							if (num[i] > num[j]) {
								temp = num[i];
								num[i] = num[j];
								num[j] = temp;

							}
						}
					}
					int i =0;
					while (true) {

						if (num.length<=i)
							break;
						writer.write(num[i] + "\r\n"); // \r = 캐리지 리턴. 커서를 맨앞으로
						i++;							// 돌리는것. \n new Line 의약자.
					}
//					for(int i = 0; i<num.length; i++ ){
//						writer.write(num[i] + "\r\n"); // \r = 캐리지 리턴. 커서를 맨앞으로
//											// 돌리는것. \n new Line 의약자.
//					}
					System.out.println("파일 복사가 완료되었습니다.");
				} catch (Exception e1) {
					e1.printStackTrace();
				} finally {
					try {

					} catch (Exception e2) {
						e2.printStackTrace();
					}
					try {
						if (writer != null)
							writer.close();
						// writer.close(); // writer를 닫아주지 않으면 저장이 안됨.

					} catch (Exception e3) {
						e3.printStackTrace();
					}

				}
			}
		});
		btnArry.setBounds(317, 20, 85, 37);
		contentPane.add(btnArry);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 77, 380, 131);
		contentPane.add(scrollPane);

		ta = new JTextArea();
		scrollPane.setViewportView(ta);
	}
}
