package REPROT;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.ItemSelectable;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Re29 extends JFrame {

	private JPanel contentPane;
	private JLabel lblResult;
	private JTextField tfBasic;
	private JTextField tfBonus;
	private JTextField tfTax;
	private int basic;
	private int bonus;
	private int taxrate;
	private int tax;
	private int silsuyeong;
	private int total;
	private JComboBox cbTaxrate;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Re29 frame = new Re29();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	void reset() {
		tfBonus.setText("");
		tfTax.setText("");
		tfBasic.setText("");
		lblResult.setText("");
	}

	public Re29() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 379, 499);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblGrade = new JLabel("직급");
		lblGrade.setBounds(22, 33, 99, 29);
		contentPane.add(lblGrade);

		JLabel lblbasic = new JLabel("기본급");
		lblbasic.setBounds(22, 83, 99, 29);
		contentPane.add(lblbasic);

		JLabel lblNewLabel_2 = new JLabel("보너스");
		lblNewLabel_2.setBounds(22, 133, 99, 29);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("세율");
		lblNewLabel_3.setBounds(22, 183, 99, 29);
		contentPane.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("세액");
		lblNewLabel_4.setBounds(22, 233, 99, 29);
		contentPane.add(lblNewLabel_4);


		lblResult = new JLabel("");
		lblResult.setBounds(22, 352, 307, 37);
		contentPane.add(lblResult);

		tfBasic = new JTextField();
		tfBasic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tfBasic.getText().equals("")) {
					JOptionPane.showMessageDialog(Re29.this, "기본급은 받아야지");
				} else {
					basic = Integer.parseInt(tfBasic.getText());
					tfBonus.requestFocus();

				}
			}
		});
		tfBasic.setBounds(143, 80, 186, 35);
		contentPane.add(tfBasic);
		tfBasic.setColumns(10);

		tfTax = new JTextField();
		tfTax.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		tfTax.setBounds(143, 230, 186, 35);
		contentPane.add(tfTax);
		tfTax.setColumns(10);

		JComboBox cbGrade = new JComboBox();
		cbGrade.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					int grade = cbGrade.getSelectedIndex();
					if (grade == 1) {
						tfBasic.setText("");
						tfBasic.setText("1000000");
						basic = 1000000;
					} else if (grade == 2) {
						tfBasic.setText("");
						tfBasic.setText("1500000");
						basic = 1500000;
					} else if (grade == 3) {
						tfBasic.setText("");
						tfBasic.setText("2000000");
						basic = 2000000;
					} else if (grade == 4) {
						tfBasic.setText("");
						tfBasic.setText("2500000");
						basic = 2500000;
					} else if (grade == 5) {
						tfBasic.setText("");
						tfBasic.setText("3000000");
						basic = 3000000;
					}
					lblResult.setText("");
					lblResult.setText("실수령액:" + String.format("%,d", basic) + "원");

				}
			}
		});
		tfBonus = new JTextField();
		tfBonus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tfBonus.getText().equals("")) {
					JOptionPane.showMessageDialog(Re29.this, "보너스도 못받니 ㅠㅠ?");
				} else {
					bonus = Integer.parseInt(tfBonus.getText());
					cbTaxrate.requestFocus();
				}
			}
		});
		tfBonus.setBounds(143, 130, 186, 35);
		contentPane.add(tfBonus);
		tfBonus.setColumns(10);

		cbGrade.setModel(new DefaultComboBoxModel(new String[] { "직책", "사원", "주임", "대리", "과장", "부장" }));
		cbGrade.setBounds(143, 30, 186, 29);
		contentPane.add(cbGrade);

		cbTaxrate = new JComboBox();
		cbTaxrate.setModel(new DefaultComboBoxModel(new String[] { "세율", "0%", "3%", "5%" }));
		cbTaxrate.setBounds(143, 180, 186, 32);
		contentPane.add(cbTaxrate);
		
		
		cbTaxrate.addItemListener(new ItemListener() {
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED) {
				if (tfBonus.getText().equals("")) {
					JOptionPane.showMessageDialog(Re29.this, "보너스도 못받니 ㅠㅠ?");
					return;
				}}
				if (e.getStateChange() == ItemEvent.SELECTED) {
					bonus = Integer.parseInt(tfBonus.getText());
					total = basic + bonus;
					double taxrate = cbTaxrate.getSelectedIndex();
					if (taxrate == 1) {
						taxrate = 0;
						tax = (int) (total * taxrate);
						silsuyeong = total - tax;

					} else if (taxrate == 2) {
						taxrate = 0.03;
						tax = (int) (total * taxrate);
						silsuyeong = total - tax;
					} else if (taxrate == 3) {
						taxrate = 0.05;
						tax = (int) (total * taxrate);
						silsuyeong = total - tax;
					}

				}
				tfTax.setText(Integer.toString(tax));
				lblResult.setText("실수령액:" + String.format("%,d", silsuyeong) + "원");
			}
		});
		JButton btnCalc = new JButton("계산");
		btnCalc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int grade = cbGrade.getSelectedIndex();
				bonus = Integer.parseInt(tfBonus.getText());
				if (grade == 1) {
					tfBasic.setText("");
					tfBasic.setText("1000000");
					basic = 1000000;
				} else if (grade == 2) {
					tfBasic.setText("");
					tfBasic.setText("1500000");
					basic = 1500000;
				} else if (grade == 3) {
					tfBasic.setText("");
					tfBasic.setText("2000000");
					basic = 2000000;
				} else if (grade == 4) {
					tfBasic.setText("");
					tfBasic.setText("2500000");
					basic = 2500000;
				} else if (grade == 5) {
					tfBasic.setText("");
					tfBasic.setText("3000000");
					basic = 3000000;
				}
				total = basic + bonus;
			
			
			double taxrate = cbTaxrate.getSelectedIndex();
			
			if (taxrate == 1) {
				taxrate = 0;
				tax = (int) (total * taxrate);
				silsuyeong = total - tax;

			} else if (taxrate == 2) {
				taxrate = 0.03;
				tax = (int) (total * taxrate);
				silsuyeong = total - tax;
			} else if (taxrate == 3) {
				taxrate = 0.05;
				tax = (int) (total * taxrate);
				silsuyeong = total - tax;
			}
			lblResult.setText("");
			lblResult.setText("실수령액:" + String.format("%,d", silsuyeong) + "원");
				
			}
		});
		this.getRootPane().setDefaultButton(btnCalc);
		// 한프레임에 하나밖에 쓸 수 없는 이벤트. 프레임의 기본 버튼으로 설정
		btnCalc.setBounds(22, 283, 153, 37);
		contentPane.add(btnCalc);

		JButton btnCancel = new JButton("취소");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cbGrade.setSelectedIndex(0);
				cbTaxrate.setSelectedIndex(0);
				tfBasic.setText("");
				tfBonus.setText("");
				tfTax.setText("");
				lblResult.setText("");
				
			}
		});
		btnCancel.setBounds(176, 283, 153, 37);
		contentPane.add(btnCancel);
	}
}
