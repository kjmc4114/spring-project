package REPROT;

public class Ex11 {
	double getDistance(int x1, int y1, int x2, int y2){
		int a=Math.abs(x2-x1); //절대값 계산
		int b=Math.abs(y2-y1);
		//제곱근
		double result=Math.sqrt( a*a + b*b );
		return result;
	}
	public static void main(String[] args) {
		Ex11 ex=new Ex11();
		double result=ex.getDistance(10, 10, 20, 20);
		System.out.println("거리:"+result);
		result=ex.getDistance(0, 0, 20, 20);
		System.out.println("거리:"+result);
	}
}
