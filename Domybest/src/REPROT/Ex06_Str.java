package REPROT;

public class Ex06_Str {
public static void main(String[] args) {
	String str="IU is lovely";
	System.out.println(str);
	System.out.println(str.toUpperCase());
	System.out.println(str.toLowerCase());
	System.out.println(str);
	str = str.toLowerCase();
	System.out.println(str);
	
	str=""; //빈문자열을 가리킴
	System.out.println("내용"+str);
	str=null; //참조하는 내용이 없음
	System.out.println("내용"+str);
	
	char ch='\0';
	System.out.println("내용"+ch+", code:"+(int)ch);
	ch = ' ';
	System.out.println("내용"+ch+", code"+(int)ch);

}
}
