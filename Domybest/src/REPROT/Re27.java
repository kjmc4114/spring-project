package REPROT;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class Re27 extends JFrame implements Runnable {
	private JPanel contentPane;
	private JTextField tf;
	private int dis;
	private int pay;
	private int gibonyo = 3000;
	private int chu1;
	private int chu2;
	private JLabel lblResult;
	private JCheckBox ckbSim;
	private JCheckBox ckbOutofcity;
	private boolean c = true;

	void calc() {
		dis = Integer.parseInt(tf.getText());
		if (dis <= 2) {
			pay = 3000;
		}else{ 
			pay = 3000 + ((int)Math.ceil(((dis-2)*1000)/140)*150);
			
		} 
		chu1 = ckbSim.isSelected() ? (int)(pay*0.2) : 0;
		chu2 = ckbOutofcity.isSelected() ? (int)(pay*0.2) : 0;
		pay = pay + chu1 + chu2;
		lblResult.setText(String.format("%,d", pay) + "원입니다.");
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Re27 frame = new Re27();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Re27() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 628, 395);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnStart = new JButton("출발");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c=true;
				Thread th = new Thread(Re27.this);
				th.start();

			}
		});
		btnStart.setBounds(348, 71, 81, 35);
		contentPane.add(btnStart);

		JLabel lblNewLabel = new JLabel("운행거리(Km)");
		lblNewLabel.setFont(new Font("맑은 고딕", Font.PLAIN, 20));
		lblNewLabel.setBounds(22, 74, 132, 29);
		contentPane.add(lblNewLabel);

		tf = new JTextField();
		tf.setFont(new Font("맑은 고딕", Font.PLAIN, 20));
		tf.setBounds(158, 71, 186, 35);
		contentPane.add(tf);
		tf.setColumns(10);

		JButton btnCal = new JButton("계산");
		btnCal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c= false;
				
//				tf.setText("");
//				lblResult.setText("");

			}
		});
		btnCal.setBounds(438, 70, 81, 37);
		contentPane.add(btnCal);

		JLabel lblpay = new JLabel("요금 :");
		lblpay.setFont(new Font("굴림", Font.PLAIN, 26));
		lblpay.setBounds(22, 189, 114, 49);
		contentPane.add(lblpay);

		lblResult = new JLabel("");
		lblResult.setFont(new Font("굴림", Font.PLAIN, 26));
		lblResult.setBounds(158, 189, 353, 49);
		contentPane.add(lblResult);

		ckbSim = new JCheckBox("심야할증");
		// ckbSim.addItemListener(new ItemListener() {
		// public void itemStateChanged(ItemEvent e) {
		// if (ckbSim.isSelected() == true) {
		//
		// chu = (int) (chu * 0.2);
		// }
		// }
		// });
		ckbSim.setBounds(18, 120, 201, 37);
		contentPane.add(ckbSim);

		ckbOutofcity = new JCheckBox("시외요금");
		// ckbOutofcity.addItemListener(new ItemListener() {
		// public void itemStateChanged(ItemEvent e) {
		// if (ckbSim.isSelected() == true) {
		// chu = (int) (chu * 0.2);
		// }
		// }
		// });
		contentPane.add(ckbOutofcity);
		ckbOutofcity.setBounds(260, 123, 201, 37);

	}

	@Override
	public void run() {
	
	while (c) {

			tf.setText(Integer.toString(dis));
			try {
				Thread.sleep(1000);
				calc();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			dis++;
		}
	}
}
