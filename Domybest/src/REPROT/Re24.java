package REPROT;

import java.util.ArrayList;
import java.util.List;

import ch14_CollectionGeneric.Ex06_Member;

class Re24_src{
private String code;
private String product;
private String juyo;
private int danga;
private int suryang;
private int don;
private int susuryo;
public Re24_src() {

}


public Re24_src(String code, String product, String juyo, int danga, int suryang) {
	this.code = code;
	this.product = product;
	this.juyo = juyo;
	this.danga = danga;
	this.suryang = suryang;
	don = danga * suryang;
	susuryo=(int) (don*0.001);

}



public String getCode() {
	return code;
}
public void setCode(String code) {
	this.code = code;
}
public String getProduct() {
	return product;
}
public void setProduct(String product) {
	this.product = product;
}
public String getJuyo() {
	return juyo;
}
public void setJuyo(String juyo) {
	this.juyo = juyo;
}
public int getDanga() {
	return danga;
}
public void setDanga(int danga) {
	this.danga = danga;
}
public int getSuryang() {
	return suryang;
}
public void setSuryang(int suryang) {
	this.suryang = suryang;
}


public int getDon() {
	return don;
}


public void setDon(int don) {
	this.don = don;
}


public int getSusuryo() {
	return susuryo;
}


public void setSusuryo(int susuryo) {
	this.susuryo = susuryo;
}


}
public class Re24 {

public static void main(String[] args) {
	int sum=0;
	int sum2=0;
	List<Re24_src> list = new ArrayList<>();
	
	Re24_src re = new Re24_src("001","애플","아이폰",799000,10);
	list.add(re);
	Re24_src re2 = new Re24_src("002","삼성","갤탭",599000,5);
	list.add(re2);
	Re24_src re3 = new Re24_src("003","노키아","인수합병",10000,3);
	list.add(re3);
// 객체 하나엔 하나의 자료만 들어가는게 설계 차원에서 좋다.
	System.out.println("상품코드\t회사\t주요상품\t단가\t수량\t금액\t판매수수료");
//	for(int i=0; i<list.size(); i++){
//		Re24_src re1 = list.get(i);	
//		
	for(Re24_src re1:list){	
		sum = sum+ re1.getDon();
		sum2 = sum2+ re1.getSusuryo();
		System.out.println(re1.getCode()+"\t"+re1.getProduct()+"\t"+re1.getJuyo()+"\t"+re1.getDanga()+"\t"+re1.getSuryang()+"\t"+re1.getDon()+"\t"+re1.getSusuryo());
	}
	System.out.println("합계:"+"\t\t\t\t\t"+sum+sum2);

}


}


