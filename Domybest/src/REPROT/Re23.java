package REPROT;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

class Re23_src {
	private String name;
	private int age;
	private double height;
	private double weight;
	private double bmi;
	private String state;

	Scanner input = new Scanner(System.in);

	public Re23_src() {

	}

	public Re23_src(String name, int age, double height, double weight) {
		this.name = name;
		this.age = age;
		this.height = height;
		this.weight = weight;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getBmi() {
		return bmi;
	}

	public void setBmi(double bmi) {
		this.bmi = weight / ((height * 0.01) * (height * 0.01));
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
	
		this.state = state;
	}
	public void calc(){
			bmi = weight / ((height * 0.01) * (height * 0.01));
		if (bmi < 18.5) {
			state = "저체중";
		} else if (bmi >= 18.5 && bmi < 22.9) {
			state = "정상";
		} else if (bmi >= 23 && bmi < 24.9) {
			state = "과체중";
		} else {
			state = "비만";
		}
		
	}
	public void input() {
		System.out.println("이름:");
		name = input.nextLine();
		System.out.println("나이");
		age = input.nextInt();
		System.out.println("몸무게");
		weight = input.nextDouble();
		System.out.println("키");
		height = input.nextDouble();
	}

}

public class Re23 {
	public static void main(String[] args) {
		String user;
		Scanner input = new Scanner(System.in);
		List<Re23_src> list = new ArrayList<>();
		Map<Object,Object> map= new HashMap<Object,Object>();
		Re23_src user1 = new Re23_src();
		user1.input();
		user1.calc();
		list.add(user1);
		map.put(user1.getName(),user1);
		Re23_src user2 = new Re23_src("김대균", 31, 185, 65);
		user2.calc();
		list.add(user2);
		map.put(user2.getName(),user2);
		Re23_src user3 = new Re23_src("윤근육", 30, 175, 70);
		list.add(user3);
		map.put(user3.getName(),user3);
		try {
			System.out.println("회원님 이름을 입력하세요");
		} catch (Exception e) {
			System.out.println("해당회원은 등록되어 있지 않습니다.");
		}
		user=input.nextLine();
		Re23_src listrlt = (Re23_src)map.get(user);
		System.out.println("회원이름\t나이\t신장\t몸무게\tBmi\t상태");
		System.out.println(listrlt.getName()+"\t"+listrlt.getAge()+"\t"+String.format("%.1f", listrlt.getHeight())
		+"\t"+String.format("%.1f", listrlt.getWeight())+"\t"+String.format("%.2f", listrlt.
				getBmi())+"\t"+listrlt.getState());
	}
}
