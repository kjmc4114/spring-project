package REPROT;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import ch14_CollectionGeneric.Ex08_Student;

class Re22_src {
	Scanner input = new Scanner(System.in);
	private String name;
	private String bank;
	private String zan;
	private String account;

	public Re22_src() {

	}

	public Re22_src(String name, String bank, String zan, String accout, String account) {
		this.name = name;
		this.bank = bank;
		this.zan = zan;
		this.account = account;
	}

	public void input() {
		System.out.println("이름");
		name = input.nextLine();
		System.out.println("은행:");
		bank = input.nextLine();
		System.out.println("잔액:");
		zan = input.nextLine();
		System.out.println("계좌: ex(00-00-00)");
		account = input.nextLine();
	}

	public String getName() {
		return name;
	}

	public String getBank() {
		return bank;
	}

	public String getZan() {
		return zan;
	}

	public String getAccount() {
		return account;
	}




}

public class Re22 {
	private static String user;
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		List<Re22_src> list = new ArrayList<>();
		Map<Object, Object> map = new HashMap<Object, Object>();
		Re22_src member1 = new Re22_src();
		member1.input();
		list.add(member1);
		map.put(member1.getAccount(), member1);
		Re22_src member2 = new Re22_src();
		member2.input();
		list.add(member2);
		map.put(member2.getAccount(), member2);
		Re22_src member3 = new Re22_src();

		System.out.println("고객님의 계좌번호를 입력해주세요.");
		user = input.nextLine();
		System.out.println("이름\t은행명\t계좌\t잔액");
		
		Re22_src list2 = (Re22_src) map.get(user);
		System.out.println(list2.getName()+list2.getBank()+String.format("%,d",
				Integer.parseInt(list2.getZan())));

	}

}
