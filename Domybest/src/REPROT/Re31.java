package REPROT;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.awt.event.ActionEvent;

public class Re31 extends JFrame {

	private JPanel contentPane;
	private JTextField tfFileName;
	private JTextField tfFindValue;
	private JTextField tfNewValue;
	private BufferedReader reader = null;
	private BufferedWriter writer = null;
	// private StringBuilder str = new StringBuilder();
	private String str2;
	// private String replace;
	private JTextArea ta;
	private String file1 = "D:\\Ted2.txt";
	private String file2 = "D:\\loveDaeGyun.txt";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Re31 frame = new Re31();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Re31() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 611, 432);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("파일경로");
		lblNewLabel.setBounds(22, 48, 99, 29);
		contentPane.add(lblNewLabel);

		tfFileName = new JTextField();
		tfFileName.setBounds(129, 45, 289, 35);
		contentPane.add(tfFileName);
		tfFileName.setColumns(10);

		JButton btnNewButton = new JButton("확인");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// file1 = tfFileName.getText();
				try {
					reader = new BufferedReader(new InputStreamReader(new FileInputStream(file1)));

					while (true) {
						str2 = reader.readLine();
						if (str2 == null)
							break;
						ta.append(str2 + "\n");

					}

				} catch (Exception e1) {
					e1.printStackTrace();
				}
				// finally {
				// try {
				// if (reader != null)
				// reader.close();
				//
				// } catch (Exception e2) {
				// e2.printStackTrace();
				// }
				// }
			}
		});

		btnNewButton.setBounds(428, 44, 135, 37);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("바꾸기");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// file2 = tfFileName.getText();
				String a = tfFindValue.getText();
				String b = tfNewValue.getText();
				try {
					reader = new BufferedReader(new InputStreamReader(new FileInputStream(file1)));
					writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file2)));
					while (true) {
					if (str2 == null)
					break;
					str2 =reader.readLine();
					str2 = str2.replace("java", "자바");
					System.out.println(str2);
					System.out.println(str2.replace("java","자바"));
					}
					
					while((str2 = reader.readLine()) != null) {
						str2 = str2.replace("java","자바");
						System.out.println(str2);
					}
					// str = str. replace (a, b);
					// wirter. write(str +"\r\n")}  3번이 제일 깔끔!
					// System.out.println(str2.replace("java", "자바"));
					// ta.append(str2.replace(a,b)+"\n");
					// writer.write(str2.replace(a, b) + "\r\n");
					 

					// }

				} catch (Exception e1) {
					e1.printStackTrace();
				} finally {

					try {
						if (reader != null)

							if (writer != null)
								writer.close();
						// writer.close(); // writer를 닫아주지 않으면 저장이 안됨.

					} catch (Exception e3) {
						e3.printStackTrace();
					}
					System.out.println("파일 복사가 완료되었습니다.");
				}

			}
		});
		btnNewButton_1.setBounds(454, 91, 109, 37);
		contentPane.add(btnNewButton_1);

		tfFindValue = new JTextField();
		tfFindValue.setBounds(129, 92, 109, 35);
		contentPane.add(tfFindValue);
		tfFindValue.setColumns(10);

		JLabel lblNewLabel_1 = new JLabel("찾는단어");
		lblNewLabel_1.setBounds(22, 95, 99, 29);
		contentPane.add(lblNewLabel_1);

		tfNewValue = new JTextField();
		tfNewValue.setBounds(345, 92, 99, 35);
		contentPane.add(tfNewValue);
		tfNewValue.setColumns(10);

		JButton btnNewButton_2 = new JButton("찾기");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ta.requestFocus();
			}
		});
		btnNewButton_2.setBounds(250, 91, 88, 37);
		contentPane.add(btnNewButton_2);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 155, 541, 185);
		contentPane.add(scrollPane);

		ta = new JTextArea();
		scrollPane.setViewportView(ta);
	}
}
