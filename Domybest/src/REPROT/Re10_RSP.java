package REPROT;

import java.util.Random;
import java.util.Scanner;

public class Re10_RSP {
	static Random com = new Random();
	static Scanner input = new Scanner(System.in);
	static String[] RSP = { "가위", "바위", "보" };

	static void input() {

		for (;;) {
			System.out.println("가위(1) 바위(2) 보(3)를 해보자. 종료=0");

			int user = input.nextInt();
			int rsp = com.nextInt(3);
			switch (user) {
			case 1:
				System.out.println("가위를 내셨습니다.");
				if (rsp == 0) {
					System.out.println("컴퓨터는" + RSP[0] + "을 냈습니다\n비겼습니다.");
				} else if (rsp == 1) {
					System.out.println("컴퓨터는" + RSP[1] + "을 냈습니다\n졌습니다. 드루와드루와^^");
				} else if (rsp == 2) {
					System.out.println("컴퓨터는" + RSP[2] + "을 냈습니다\n이겼습니다. 좋지?^^");
				}
				break;
			case 2:
				System.out.println("바위를 내셨습니다.");
				if (rsp == 0) {
					System.out.println("컴퓨터는" + RSP[0] + "을 냈습니다\n이겼습니다. 좋지?^^");
				} else if (rsp == 1) {
					System.out.println("컴퓨터는" + RSP[1] + "을 냈습니다\n비겼습니다.");
				} else if (rsp == 2) {
					System.out.println("컴퓨터는" + RSP[2] + "을 냈습니다\n졌습니다. 드루와드루와^^");
				}
				break;
			case 3:
				System.out.println("보를 내셨습니다.");
				if (rsp == 0) {
					System.out.println("컴퓨터는" + RSP[0] + "을 냈습니다.\n 졌습니다. 드루와드루와^^.");
				} else if (rsp == 1) {
					System.out.println("컴퓨터는" + RSP[1] + "을 냈습니다\n졌습니다.");
				} else if (rsp == 2) {
					System.out.println("컴퓨터는" + RSP[2] + "을 냈습니다\n이겼습니다. 좋지?^^");
				}
				break;
			case 0:
				System.out.println("끝~^^ 잘가~");
				System.exit(0);
			}
		}
	}

	public static void main(String[] args) {
		input();
	}
}
