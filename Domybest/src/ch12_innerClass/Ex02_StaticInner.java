package ch12_innerClass;

public class Ex02_StaticInner {
	static int x = 10;
	static class Inner{
		int getX(){
			return x;
		}
	}

public static void main(String[] args) {
	Ex02_StaticInner.Inner in = new Ex02_StaticInner.Inner();
	System.out.println(in.getX());
}

}

