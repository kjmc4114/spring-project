package ch12_innerClass; //InnerClass$Inner.class

public class Ex01_InnerClass {
private	static int x = 10;
	class Inner {
		int getX(){
			return x;
		}
	}
	
	
	
	
	public static void main(String[] args) {
{
		System.out.println(x);
		Ex01_InnerClass e = new Ex01_InnerClass();
		Ex01_InnerClass.Inner in = e.new Inner(); //외부.내부 에대한 객체 생성. 
		System.out.println(in.getX());
  }	
 }
}