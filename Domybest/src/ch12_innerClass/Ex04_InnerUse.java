package ch12_innerClass;

import ch11_추상화.Ex03_Flyer;
public class Ex04_InnerUse {
public static void main(String[] args) {
	Ex03_Flyer a = new Ex03_Flyer() {
		
		@Override
		public void takeoff() {
System.out.println("take off");
			
		}
		
		@Override
		public void land() {
			
			System.out.println(" land ");
		}
		
		@Override
		public void fly() {

			System.out.println("fly");
			
		}
	};
a.takeoff();
a.fly();
a.land();

}
}
