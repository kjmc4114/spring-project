package ch03_연산자;

import java.util.Scanner;

public class Ex12_Point {
public static void main(String[] args) {
	String name;
	int kor, eng, mat, tot;
	double avg;
	Scanner scan=new Scanner(System.in);
	System.out.println("이름");
	name=scan.next(); //문자열 출력
	System.out.println("국어점수");
	kor=scan.nextInt(); //정수입력
	System.out.println("영어점수");
	eng=scan.nextInt(); //정수입력
	System.out.println("수학점수");
	mat=scan.nextInt(); //정수입력
	/*	scan.close();*/
	tot=kor+eng+mat;
	avg = tot/3.0;
	System.out.println("이름\t국어\t영어\t수학\t총점\t평균");
	System.out.println(name+"\t"+kor+"\t"+eng+"\t"+mat+"\t"+tot+"\t"
	+String.format("%5.1f" , avg)); 
	System.out.printf(name+"\t"+kor+"\t"+eng+"\t"+mat+"\t"+tot+"\t"+avg);
}
}
