package ch03_연산자;

public class Ex08_Cast {
	public static void main(String[] args) {
		float f=1.6f;
		f=100;
		System.out.println(f);
		f=100.5f;
		int i=(int)f; // 강제형변환,명시적형변환 
		System.out.println(i);
		i=300;
		byte b=(byte)i; //error 
		System.out.println(b); //overflow 발생 
	}
}
