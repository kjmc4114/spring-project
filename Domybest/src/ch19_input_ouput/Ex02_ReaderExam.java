package ch19_input_ouput;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader; 

public class Ex02_ReaderExam {
	public static void main(String[] args) {
		int var = 0;
		//2바이트 단위로 읽음.
		Reader input = new InputStreamReader(System.in);
		//inputstream 1바이트로 단위로 읽음
		//InputStreamReader = 2바이트 단위로 읽음
		
		System.out.println("입력하세요");
		try {
		
			while(true){
				var = input.read(); // 1문자 단위로 읽음. (위에서 선언했으니까)
				if(var == 13) break; // 13 = 엔터키 
				System.out.print(var+"=="+ (char)var);
			}
		} catch (IOException e) {
	
			e.printStackTrace();
		}
	}

}
