package ch19_input_ouput;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Ex08_FileCopy {
	public static void main(String[] args) {
		String str = "";
		BufferedReader reader = null;
		BufferedWriter writer = null;
		String file1 = "d:\\a.txt";
		String file2 = "d:\\b.txt";

		// reader = new BufferedReader (new FileReader ( new File(file1)));
		// writer = new BufferedWriter (new FileWriter( new File(file2))); 같은
		// 방법임.

		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file1)));
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file2)));
			while (true) {
				str = reader.readLine();
				
				
				if (str == null)
					break;
				
				
				writer.write(str + "\r\n"); // \r = 캐리지 리턴. 커서를 맨앞으로 돌리는것. \n
											// new Line 의약자.
			}
			System.out.println("파일 복사가 완료되었습니다.");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null)
					reader.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
			try {
				if (writer != null)
					writer.close();
				// writer.close(); // writer를 닫아주지 않으면 저장이 안됨.

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
