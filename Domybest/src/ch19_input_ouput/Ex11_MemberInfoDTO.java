package ch19_input_ouput;

import java.io.Serializable;

public class Ex11_MemberInfoDTO implements Serializable {
private String juminNo;
private String passwd;



public String getJuminNo() {
	return juminNo;
}
public void setJuminNo(String juminNo) {
	this.juminNo = juminNo;
}
public String getPasswd() {
	return passwd;
}
public void setPasswd(String passwd) {
	this.passwd = passwd;
}
public Ex11_MemberInfoDTO() {
}
public Ex11_MemberInfoDTO(String juminNo, String passwd) {
	super();
	this.juminNo = juminNo;
	this.passwd = passwd;
}
        
}
