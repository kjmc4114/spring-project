package ch19_input_ouput;

import java.io.RandomAccessFile;

public class Ex10_Randomfile {

	public static void main(String[] args) {
		StringBuilder output = new StringBuilder(); //스트링 = 불변성 , 빌더는 = 가변성
		String str = null;
		try {
		RandomAccessFile file = new RandomAccessFile ("d:\\Ex02_PersonUse.txt","rw"); //읽기쓰기 모드 
		//파일 포인터 (파일을 어디까지 읽었는지 가리킴)
		file.seek(90);
		file.write("아이유 완전이쁨 밀어내기가 안되고 덮어쓰기가 됨.".getBytes());
		file.seek(0);
		while (file.getFilePointer() < file.length()){
			output.append(file.getFilePointer()+":");
			str = file.readLine(); //한줄씩읽으니까 ^^. 한줄을 읽어도 \r\n 으로 하지않으면 안됨. \n 만 읽으니까 \r 을 꼭 추가해주기.
			str = new String(str.getBytes("8859_1"),"utf-8"); //2바이트 배열 짜리를 3바이트 배열로 바꿔줌.
			output.append(str+"\r\n"); //바이트 배열 byte[] iso_8859-1 이 이클립스 기본. 한글은 utf-8
			}
		file.close();
		System.out.println(output);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
