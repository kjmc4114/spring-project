package ch19_input_ouput;

import java.io.IOException;

public class Ex01_InputStreamExam {

	public static void main(String[] args) {
		int var =0;

		System.out.println("내용을 입력하세요");
	// 입출력에 관련된것들은 거의 무조건 예외처리가 필수임. DB,Network.
			try {			
			 //1byte를 읽음 read는 int 를 리턴하게 되어있음 (컴퓨터는 2진수 로 풀어져서 숫자로 저장.)
			while(var != 13){
				var = System.in.read();
				System.out.println(var+"=="+(char)var);} 
		} catch (IOException e) {
			
			e.printStackTrace();
		}		


	}

}
