package ch19_input_ouput;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Ex14_Gugudan implements Runnable {
	private ServerSocket serverSocket; // 서비스용 소켓
	private Socket socket; // 클라이언트와의 통신용 소켓
	private DataInputStream dis;
	private DataOutputStream dos;

	public Ex14_Gugudan() {
		try {
			serverSocket = new ServerSocket(9999);
			System.out.println("구구단 서비스시작");
		} catch (Exception e) {
			e.printStackTrace();
		}
		while (true) {
			try {
				socket = serverSocket.accept();
				InetAddress ip = socket.getInetAddress();
				System.out.println("클라리언트의 ip:" + ip);
				dis = new DataInputStream(socket.getInputStream());
				dos = new DataOutputStream(socket.getOutputStream());
				Thread th = new Thread(this);
				th.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		new Ex14_Gugudan();
	}

	@Override
	public void run() {
		try {

			int dan = dis.readInt(); // 서버에서 온 숫자.
			System.out.println("클라이언트의 요청값:" + dan);
			StringBuilder sb = new StringBuilder();
			for (int i = 1; i <= 9; i++) {
				sb.append(dan + "x" + i + "=" + dan * i + "\r\n");

			}
			dos.writeUTF(sb.toString());
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

}
