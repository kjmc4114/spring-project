package ch19_input_ouput;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Ex03_Buffered {
public static void main(String[] args) {
	BufferedReader reader = new BufferedReader(new InputStreamReader ( System.in));
	System.out.println("입력하세요");
	try {
		String str = reader.readLine();
		System.out.println(str);
	} catch (Exception e) {
			e.printStackTrace();
	}
}
}
