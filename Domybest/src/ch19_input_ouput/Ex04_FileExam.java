package ch19_input_ouput;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;

public class Ex04_FileExam extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextArea ta;
	private JButton btnfile;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex04_FileExam frame = new Ex04_FileExam();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex04_FileExam() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 546, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblname = new JLabel("파일이름");
		lblname.setBounds(22, 21, 99, 29);
		contentPane.add(lblname);
		
		textField = new JTextField();
		textField.setBounds(132, 18, 186, 35);
		contentPane.add(textField);
		textField.setColumns(10);
		
		btnfile = new JButton("확인");
		btnfile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File file = new File (textField.getText()); //file = 파일의 정보를 가져오는 클래스.
				if (!file.exists()){ //파일이 존재하지 않으면.
					try {
						file.createNewFile();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				String str = "파일이름"+file.getName()+"\n"
						+"파일크기"+file.length()+"\n"
						+"상위폴더"+file.getParent();
				ta.setText(str);
			}
		});
		btnfile.setBounds(337, 17, 153, 37);
		contentPane.add(btnfile);
		
		ta = new JTextArea();
		ta.setBounds(22, 56, 468, 202);
		contentPane.add(ta);
	}
}
