package ch19_input_ouput;

import java.awt.EventQueue;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class GugudanClient extends JFrame {

	private JPanel contentPane;
	private JComboBox comboBox;
	private JTextArea taResult;
	private Socket socket;
	private DataInputStream dis;
	private DataOutputStream dos;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GugudanClient frame = new GugudanClient();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GugudanClient() {
		try {
			socket = new Socket("localhost",9999);
			dis = new DataInputStream(socket.getInputStream());
			dos = new DataOutputStream(socket.getOutputStream());
			
		}catch (Exception e){
			e.printStackTrace();
		}
		
		
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 404, 519);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDan = new JLabel("단을입력하세요");
		lblDan.setBounds(22, 21, 264, 29);
		contentPane.add(lblDan);
		
		taResult = new JTextArea();
		taResult.setBounds(22, 76, 334, 351);
		contentPane.add(taResult);
		
		comboBox = new JComboBox();
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED){
					int dan = Integer.parseInt(comboBox.getSelectedItem().toString());
					try{
						dos.writeInt(dan); //서버에 전송
						taResult.setText(dis.readUTF());
					}catch(Exception e2){
						e2.printStackTrace();
					}
				}
				
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9"}));
		comboBox.setBounds(259, 18, 97, 35);
		contentPane.add(comboBox);
	}
}
