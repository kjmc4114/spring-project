package ch19_input_ouput;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Ex13_ObjectStream {
	public static void main(String[] args) {
		//Stream = 데이터의 흐름.
		// 프로그램 --> 외부자원 ,출력스트림
		// 프로그램 <-- 외부자원 ,입력스트림
		
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		Ex12_MemberDTO m1 
		= new Ex12_MemberDTO("kim",30,"871213-111111","1234");
		Ex12_MemberDTO m2 
		= new Ex12_MemberDTO("lee",30,"871213-111111","1234");
		Ex12_MemberDTO m3 
		= new Ex12_MemberDTO("hong",30,"871213-111111","1234");
		 try {
			 fos = new FileOutputStream ( "d:\\object.dat");
			 oos = new ObjectOutputStream (fos);
			 oos.writeObject(m1);
			 oos.writeObject(m2);
			 oos.writeObject(m3);
			 System.out.println("객채를 파일로 저장하였습니다.");
		} catch (Exception e) {
			e.printStackTrace();
		
		}
		 FileInputStream fis = null;
		 ObjectInputStream ois = null;
		 try {
			 fis = new FileInputStream("d:\\object.dat");
			 ois = new ObjectInputStream(fis);
			 Ex12_MemberDTO dto1 = (Ex12_MemberDTO)ois.readObject();
			 Ex12_MemberDTO dto2 = (Ex12_MemberDTO)ois.readObject();
			 Ex12_MemberDTO dto3 = (Ex12_MemberDTO)ois.readObject();
			 System.out.println(dto1);
			 System.out.println(dto2);
			 System.out.println(dto3);
		 }catch(Exception e){
			 e.printStackTrace();
			 
		 }
		
		
		
	}

	
	
}
