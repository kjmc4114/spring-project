package ch19_input_ouput;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class Ex05_Directory extends JFrame {

	private JPanel contentPane;
	private JTextField tfDirectory;
	private JTextArea ta;
	private JLabel lblNewLabel;
	private JButton button1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex05_Directory frame = new Ex05_Directory();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex05_Directory() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		lblNewLabel = new JLabel("위치");
		panel.add(lblNewLabel);
		
		tfDirectory = new JTextField();
		panel.add(tfDirectory);
		tfDirectory.setColumns(10);
		
		button1 = new JButton("확인");
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String directory = tfDirectory.getText(); //경로가 들어옴.
				File file = new File(directory);
				String[] list = file.list();
				ta.setText("");
				for(int i=0; i<list.length; i++){
					File f = new File(directory,list[i]); //File 의 생성자가 2개인것.
					String str = f.isDirectory()?"디렉토리":"파일"; //디렉토리 면? 디렉토리 파일리스트면 파일
					ta.append(str+":"+list[i]+"\n");
				}
			}
		});
		panel.add(button1);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		ta = new JTextArea();
		scrollPane.setViewportView(ta);
	}

}
