package ch19_input_ouput;

import java.io.Serializable;

public class Ex12_MemberDTO implements Serializable{
	private String name;
	private int age;
	private transient Ex11_MemberInfoDTO memberInfo; 
	public Ex12_MemberDTO() {
	}
	public Ex12_MemberDTO(String name, int age, String jumin, String pwd) {
		super();
		this.name = name;
		this.age = age;
		this.memberInfo = memberInfo;
		memberInfo = new Ex11_MemberInfoDTO(jumin,pwd);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Ex11_MemberInfoDTO getMemberInfo() {
		return memberInfo;
	}
	public void setMemberInfo(Ex11_MemberInfoDTO memberInfo) {
		this.memberInfo = memberInfo;
	}
	@Override
	public String toString() {
		return "Ex12_MemberDTO [name=" + name + ", age=" + age + ", memberInfo=" + memberInfo + "]";
	}
	




}
