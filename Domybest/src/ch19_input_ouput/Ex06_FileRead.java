package ch19_input_ouput;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class Ex06_FileRead extends JFrame {

	private JPanel contentPane;
	private JTextField tf;
	private JTextArea ta;
	private JLabel lblNewLabel;
	private JButton button1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex06_FileRead frame = new Ex06_FileRead();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex06_FileRead() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		lblNewLabel = new JLabel("파일이름");
		panel.add(lblNewLabel);
		
		tf = new JTextField();
		panel.add(tf);
		tf.setColumns(10);
		
		button1 = new JButton("확인");
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			String file = tf.getText();
			String str = "";
			BufferedReader reader = null; //버퍼드 리더의 초기값.
//			InputStreamReder ir = null;
//			FileInputStream fi = null;
//			fi = new FileInputStream (file)
//			ir = new InputStreamReader(fi)
//			reader = new BufferedRedar(ir) 이방법을 줄임거임.
			try{	//한꺼번에 읽어오는 버퍼리더.	//한글파일을 읽을 수 있는 스트림 리더가 필요함 //읽어오기 위해서 inPutStream 이 필요함. 
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			ta.setText("");
//			while((str=reader.readLine())!=null) 리더에서 읽었을때 없지 않다면 . 어팬드를 하세요.
//			{ ta.append(str + \n);} 82~85줄을 줄인거.			
				while(true){
				str=reader.readLine();
				if (str == null ) break;
				ta.append(str + "\n");
			}
			}catch (Exception e2){
				e2.printStackTrace();
			} finally {
				try {
					if(reader!=null){reader.close();}
					
				} catch (IOException e1) {
				
					e1.printStackTrace();
				}
			}
			
			}
		});
		panel.add(button1);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		ta = new JTextArea();
		scrollPane.setViewportView(ta);
	}

}
