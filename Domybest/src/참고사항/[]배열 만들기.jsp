<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%-- <%
String name1="kim";
String name2="park";
String name3="choi";
String name4="hong";
String name5="lee";
int point1=100;
int point2=80;
int point3=70;
int point4=95;
int point5=90;
int average=(point1+point2+point3+point4+point5)/5;
%> --%>
<%

/* []배열 참조변수=다른 값을 가지고 있는 주소값 */ 

                 /*배열의 첨자들 [0      1      2      3      4] */
String[]/* 문자열배열 */ names={"kim","park","hong","choi","lee"}; 
int[] /* 정수배열 */points={100,80,70,60,93};
for(int i=0; i<names.length; /* names 배열의 길이 */ i++){
    //배열 참조변수[인덱스] 인덱스는 0부터 시작
	out.println(names[i]+" "+"<br>");
}

out.println("<br>");

//확장 for문 (자바 1.5 버전부터 사용 가능)
//for(개별값:집합)
//집합의 각각요소를 반복처리함
for(String n : names){
	out.println(n+" ");
}
out.println("<br>");

int total=0; /* 생성할 변수는 꼭 지정해주기 */
int average=0; /* 생성할 변수는 꼭 지정해주기 */
for(int i=0; i<points.length; i++){
	out.println(points[i]+" ");
	total=total+points[i];
}

average=total/points.length;

out.println("<br>");


%>
<br>
총점: <%=total%> 평균: <%=average%>



</body>
</html>