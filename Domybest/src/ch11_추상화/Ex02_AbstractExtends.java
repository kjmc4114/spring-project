package ch11_추상화;

public class Ex02_AbstractExtends extends Ex01_AbstractClass {

	// 추상화된 애를 구체화 시켜야함
	@Override
	void method1() {
		System.out.println("추상 매소드를 완성한 매소드");
	}
	public static void main(String[] args) {
		Ex02_AbstractExtends ex= new Ex02_AbstractExtends();
		ex.method1(); // 추상매소드를 불러와서 완성시킴.
		ex.method2(); // 완성매소드를 그냥 불러옴.
	}

}
