package ch11_추상화;

public class Ex04_Airplane implements Ex03_Flyer {

	@Override
	public void takeoff() {
	System.out.println("Airplane 이륙");

	}

	@Override
	public void fly() {
	System.out.println("Airplane 비행");

	}

	@Override
	public void land() {
	System.out.println("Airplane 착륙");
	}

}
