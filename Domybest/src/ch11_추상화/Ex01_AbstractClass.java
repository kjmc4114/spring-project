package ch11_추상화;

public abstract class Ex01_AbstractClass {
  abstract void method1(); //abstract method 헤더()만 있고 바디{}가 없음
  void method2(){ System.out.println("완성된 매소드");}; //완성된 메소드
}
