package ch11_추상화;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Ex11_DrawUse extends Applet{
	
	Ex07_Draw shape1; //참조변수
	Ex07_Draw shape2;
	
	public Ex11_DrawUse(){
		shape1 = new Ex09_DarwCircle(30,30,60,80);
		shape2 = new Ex10_DrawLine(10, 20, 150, 100);
		setBackground(Color.ORANGE);
	}
	@Override
	public void paint(Graphics g) {
		g.drawString("자바그래픽", 100, 60);
		shape1.paint(g);
		shape2.paint(g);
		
	}
}
