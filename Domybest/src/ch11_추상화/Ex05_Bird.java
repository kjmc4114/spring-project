package ch11_추상화;

public class Ex05_Bird implements Ex03_Flyer {

	@Override
	public void takeoff() {
		System.out.println("날아오라 주작이여");
	}

	@Override
	public void fly() {
		System.out.println("환상의 날개 나라오르라");
	}

	@Override
	public void land() {
		System.out.println("추락");

	}

}
