package ch04_조건문과반복문;

import java.util.Scanner;

public class Ex05_For {
	public static void main(String[] args) {
		if(args.length==0){
			System.out.println("아니 매개변수 지정안하고 뭐허냐!");
		System.exit(-1);
		}
		int num=Integer.parseInt(args[0]);
		for(int i=0; i<=num; i++){
		System.out.println(i);} //for문안에 i는 {}안에서만 유효하다.
	
//	System.out.println("종료하려면 0을 입력하세요");
//	Scanner input=new Scanner(System.in);
//	int n=input.nextInt();
//	for(;;){
//	System.out.println("입력하세요");
//	if(n==0) break;
//	System.out.println("입력한 값은 "+n+"입니다.");
//	}
	
 }
		/*for문의 형태 for(초기식; 조건식; 증감식){대입할식}*/
}
