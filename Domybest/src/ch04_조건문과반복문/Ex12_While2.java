package ch04_조건문과반복문;

public class Ex12_While2 {
	public static void main(String[] args) {
		int i=1;
		int num=10;
		int sum=0;
		while(i<=num){
			sum=sum+i;
			i++;
		}
		System.out.println("1~"+num+"까지의 합계:"+sum);
	}
}
