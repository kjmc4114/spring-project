package ch04_조건문과반복문;

import java.util.Scanner;

public class Ex01_if {
public static void main(String[] args) {
	Scanner scan=new Scanner(System.in);
	System.out.println("숫자입력하세요");
	int num=scan.nextInt();
	if(num % 2 == 0){ 
		System.out.println(num+"은  짝수입니다.");
	
	}else{
		System.out.println(num+"은 홀수입니다.");
	}
}
}
