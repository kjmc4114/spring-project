package ch04_조건문과반복문;

public class Ex17_Continue {
public static void main(String[] args) {
	for(int i=1; i<=10; i++){
		if(i%5 == 0) continue; // 5의 배수면 이하는 무시하고 가시오.
		System.out.println(i);
	}
	System.out.println("프로그램을 종료합니다.");
}
}
