package ch04_조건문과반복문;

public class Ex02_Ifelse {
	public static void main(String[] args) {
		int kor=85;
		int eng=74;
		int mat=95;
		int tot=kor+eng+mat;
		String grade="";
		double avg=tot/3.0;
		if(avg>=90){grade="수";}
		else if(avg>=80){grade="우";}
		else if(avg>=70){grade="미";}
		else if(avg>=60){grade="양";}
		else{grade="가";}
System.out.println("국어\t영어\t수학"
		+ "\t평균\t총점\t등급");		
System.out.println(kor+"\t"+eng+"\t"
		+mat+"\t"+String.format("%4.1f", avg)+"\t"+tot+"\t"+grade+"\t");		
		
	}

}
