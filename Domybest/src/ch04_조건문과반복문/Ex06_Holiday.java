package ch04_조건문과반복문;

import java.util.Scanner;

public class Ex06_Holiday {
	public static void main(String[] args) {
		int year;
		int days;
		Scanner input=new Scanner(System.in);
		System.out.println("몇년을 일하셨어요?");
		year=input.nextInt();
		input.close();
		if(year<=3){days=3;}
		else if(year<10){days=5;}
		else if(year<20){days=10;}
		else {days=20;}
		System.out.println("근속연수"+year+"년이고"+"\n"+
		"휴가일수는"+days+"일입니다.");
	}

}
