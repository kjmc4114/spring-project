package ch04_조건문과반복문;

public class Ex14_Dowhile2 {
public static void main(String[] args) {
	int i=1;
	while(true){
		System.out.println(i++);
		if(i>10) break;
	}
	System.out.println("프로그램 종료");
}
}
