package ch07_ClassandInstance;

public class Ex10_BookUse {
public static void main(String[] args) {
//	Ex09_Book b=new Ex09_Book();
//    b.input("아이유일대기", "아이유", "정명", 2017, 100, 1000000);
	
	Ex09_Book b=new Ex09_Book("아이유일대기", "아이유", "정명", 2017, 100, 1000000);
	b.print();
	Ex09_Book b2=new Ex09_Book(); //기본생성자를 호출하게 되는거임
//	new Ex09_Book : 객체를 생성
//	Ex09_Book() : 생성자를 호출
	b2.print();


}
}
