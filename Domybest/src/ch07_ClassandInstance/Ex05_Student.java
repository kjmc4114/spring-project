package ch07_ClassandInstance;

public class Ex05_Student {
	 private String name;
	 private String major;
	 private int year;
	 private String num;
	 private double point;
	 private int money;
	 private int save;
	
	 //계산할건 빼고 만들기 - void input 으로 변경하고 super는 빼기. =>입력받을 변수를 생성. 
	 public void input(String name, String major, int year, String num, double point, int money) {
		this.name = name;
		this.major = major;
		this.year = year;
		this.num = num;
		this.point = point;
		this.money = money;
		if(point >=4.3){save=money;}
		else if(point >=4.0){save=2500000;}
		else if(point >=3.5){save=1000000;}
	}
	 //@____ = 코드에 대한 주석
	@Override //기존 메서드를 재정의 한다. 원래는 object class(최상위 클래스)의 매서드였음.
	public String toString() {
		String str=
				"이름\t학과\t학년\t학번\t평점\t등록금\t장학금\n";
		str += name+"\t"+major+"\t"+year+"\t"+num+"\t"+point+"\t"+money+"\t"+save;
		
		return str; /*"[name=" + name + ", 전공=" + major + ", 학년=" + year + ", 학번=" + num + ", 평균="
				+ point + ", 등록금=" + money + ", 장학금=" + save + "]";*/
	/*	public void print(){
			System.out.println("이름\t전공\t학년\t학번\t평점평균\t등록금\t장학금");
			System.out.println(name+"\t"+major+"\t"+*/
	}
	 

}
