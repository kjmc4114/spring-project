package ch07_ClassandInstance;

public class Ex01_Person {

	private String name;
	private int age;
	private double height;

	public void setName(String name) {
		this.name = name;
	}

	String getName() {
		return name;
	}

	public void setAge(int age) {
		if(age<0 || age>150){
			System.out.println("말도 안되는...."+
		"\n"+"현재 인간의 최대수명은 150살을 넘지 못한다고 보고있으며..."
					);
		}else{
		this.age = age;}
	}

	public int getAge() {
		return age;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	
	public double getHeight() {
		return height;
	}
	
	public void print(){
		System.out.println("이름:"+getName());
//		System.out.println("이름:"+p1.name);
		System.out.println("나이:"+getAge());
		System.out.println("키:"+getHeight());
	}
}
