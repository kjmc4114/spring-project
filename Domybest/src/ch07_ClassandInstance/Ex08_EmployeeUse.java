package ch07_ClassandInstance;

import java.util.Scanner;

public class Ex08_EmployeeUse {
public static void main(String[] args) {
	Ex07_Employee p1 = new Ex07_Employee();
	Scanner input= new Scanner(System.in);
	System.out.println("이름을 입력하세요");
	String name=input.nextLine();
	System.out.println("부서를 입력하세요");
	String part=input.nextLine();
	System.out.println("직급을 입력하세요");
	String position=input.nextLine();
	System.out.println("기본급을 입력하세요");
	int basic=input.nextInt();
	
	p1.input(name, part, position, basic);
	p1.calc();
	p1.print();
}
}
