package ch07_ClassandInstance;

public class Ex02_PersonUse {
public static void main(String[] args) {
	//객체 (인스턴스) 생성, 인스턴스 화
	Ex01_Person p1=new Ex01_Person(); //p1 이라는 주소값을 저장해주는 것. 
	//클래스를 사용하기 위해서는 메모리에 올려야함
//	p1.name="아이유"; //객체 참조 변수.멤버
	p1.setAge(23);
	p1.setHeight(160);
	p1.setName("아이유");
//	System.out.println("이름:"+p1.getName());
////	System.out.println("이름:"+p1.name);
//	System.out.println("나이:"+p1.getAge());
//	System.out.println("키:"+p1.getHeight());
	p1.print();
}
}
