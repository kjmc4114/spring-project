package ch07_ClassandInstance;

public class Ex11_Member {
private String name;
private String userid;
private int money;
private String grade;


public Ex11_Member(){
	System.out.println("기본 생성자 호출....");
}

public Ex11_Member(String name, String userid, int money) {
	System.out.println("매개 변수가 있는 생성자");
	this.name = name;
	this.userid = userid;
	this.money = money;
}

public void input(String name, String userid, int money) {

	this.name = name;
	this.userid = userid;
	this.money = money;
}

public void setGrade(){
	if(money>=10000){
		grade="골드";
	}else {
		grade="실버";
	}
}
public void print(){System.out.println("이름\t아이디\t구매금액\t등급");
System.out.println(name+"\t"+userid+"\t"+money+"\t"+grade);}







}
