package ch07_ClassandInstance;

public class Ex12_MumberUse {
	public static void main(String[] args) {
		Ex11_Member m1=new Ex11_Member();
		m1.input("아이유", "IU", 50000);
		m1.setGrade();
		m1.print();
		
		Ex11_Member m2=new Ex11_Member("아이유", "IU", 1000000);
		m2.setGrade();
		m2.print();
	}

}
