package ch07_ClassandInstance;

public class Ex09_Book {
private String bookName;
private String author;
private String press;
private int year;
private int price;
private int amount;
private int money;
public Ex09_Book()//기본 생성자 

{
	this("꽃갈피","아이유","정명",2016,10,4000); //this() 다른 생성자를 호출함.
										// 생성자는 첫라인에 작성해야함
	System.out.println("기본 생성자 호출");
}

//public void input(String bookName,
//		String author, String press, 
//		int year, int price, int amount) {
//	
//	this.bookName = bookName;
//	this.author = author;
//	this.press = press;
//	this.year = year;
//	this.price = price;
//	this.amount = amount;
//	money = price*amount;
//}

public void print(){	
	System.out.println("도서명\t\t저자\t출판사\t출판연도\t단가\t수량\t금액");
	System.out.println(bookName+"\t"+author+"\t"+press+"\t"+year+"\t"+price+"\t"+amount+"\t"+money);
}



public Ex09_Book(String bookName, String author, String press, int year, int price, int amount) {
	System.out.println("매개변수가 있는 생성자 호출");
	this.bookName = bookName;
	this.author = author;
	this.press = press;
	this.year = year;
	this.price = price;
	this.amount = amount;
	money = price*amount;
}


}
