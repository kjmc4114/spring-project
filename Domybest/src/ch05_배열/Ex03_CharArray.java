package ch05_배열;

public class Ex03_CharArray {
public static void main(String[] args) {
//	ch=new char[4];
//	ch[0]='J';
//	ch[1]='A';
//	ch[2]='V';
//	ch[3]='A'; 을 간단하게 나타낸것.
	char[] ch={'J','A','V','A'};
	for( int i=0; i<ch.length; i++){
		System.out.println(i+"번째 문자:"+ ch[i]);
	}
}
}
