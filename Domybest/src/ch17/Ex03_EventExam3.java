package ch17;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Ex03_EventExam3 extends JFrame {
private JButton button1,button2,button3,button4,button5,button6;
public Ex03_EventExam3() {
setLayout(new FlowLayout());
button1 = new JButton("Red");
button2 = new JButton("Green");
button3 = new JButton("Blue");
button4 = new JButton("White");
button5 = new JButton("Yellow");
button6 = new JButton("Cyan");
this.add(button1); 
add(button2);  // 같은 표현
add(button3); 
add(button4);
add(button5);
add(button6);
 

button1.addActionListener(new EX06_MyColorAction(this, Color.red));
button2.addActionListener(new EX06_MyColorAction(this, Color.green));
button3.addActionListener(new EX06_MyColorAction(this, Color.blue));
button4.addActionListener(new EX06_MyColorAction(this, Color.white));
button5.addActionListener(new EX06_MyColorAction(this, Color.yellow));
button6.addActionListener(new EX06_MyColorAction(this, Color.cyan));


setSize(450, 300);
setVisible(true);
setDefaultCloseOperation(EXIT_ON_CLOSE);
//모든창을 닫음
setDefaultCloseOperation(DISPOSE_ON_CLOSE);
//현재창만 닫음 (여러개의 창을 띄울시 사용하는 기능)

}
public static void main(String[] args) {
	new Ex03_EventExam3();
}
}
