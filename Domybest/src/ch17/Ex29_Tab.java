package ch17;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.ImageIcon;

public class Ex29_Tab extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex29_Tab frame = new Ex29_Tab();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex29_Tab() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 680, 503);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(22, 21, 581, 307);
		contentPane.add(tabbedPane);
		
		JPanel tab1 = new JPanel();
		tab1.setToolTipText("탭1");
		tabbedPane.addTab("탭1", new ImageIcon(Ex29_Tab.class.getResource("/ch16/duke1.gif")), tab1, null);
		
		JButton btnNewButton_2 = new JButton("버튼1");
		btnNewButton_2.setIcon(new ImageIcon(Ex29_Tab.class.getResource("/ch16/duke8.gif")));
		tab1.add(btnNewButton_2);
		
		JPanel tab2 = new JPanel();
		tabbedPane.addTab("탭2", new ImageIcon(Ex29_Tab.class.getResource("/ch16/duke10.gif")), tab2, null);
		
		JButton btn2 = new JButton("버튼2");
		tab2.add(btn2);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("탭3", null, panel_2, null);
		
		JButton btnNewButton = new JButton("New button");
		panel_2.add(btnNewButton);
	}
}
