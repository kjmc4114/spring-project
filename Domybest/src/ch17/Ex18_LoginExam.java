package ch17;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JPasswordField;

public class Ex18_LoginExam extends JFrame {

	private JPanel contentPane;
	private JTextField textID;
	private JButton btnLogin;
	private JLabel lblresult;
	private Map<String, String> map = new HashMap<String, String>();
	private JPasswordField pwd;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex18_LoginExam frame = new Ex18_LoginExam();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex18_LoginExam() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel userid = new JLabel("아이디");
		userid.setBounds(46, 49, 57, 15);
		contentPane.add(userid);
		
		pwd = new JPasswordField();
		pwd.setBounds(130, 100, 133, 41);
		contentPane.add(pwd);

		textID = new JTextField();
		textID.setBounds(130, 46, 133, 33);
		contentPane.add(textID);
		textID.setColumns(10);

		map = new HashMap<>();
		map.put("kim", "1234");
		map.put("park", "2222");
		map.put("hong", "3333");

		btnLogin = new JButton("로그인");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String id = userid.getText();
				String pw = String.valueOf(pwd.getPassword());
				String strId=map.get(id);
				
				if (strId !=null&&strId.equals(pw)) {
					lblresult.setForeground(Color.blue); //글자색
					lblresult.setText(id + "님환영합니다.");
				} else {
					lblresult.setForeground(Color.red);
					lblresult.setText("아이디 또는 비밀번호가 일치하지 않습니다.");
				}
			}
		});
		btnLogin.setBounds(126, 151, 97, 23);
		contentPane.add(btnLogin);

		lblresult = new JLabel("");
		lblresult.setFont(new Font("돋움", Font.PLAIN, 12));
		lblresult.setBounds(46, 197, 356, 41);
		contentPane.add(lblresult);

		label = new JLabel("패스워드");
		label.setBounds(46, 113, 57, 15);
		contentPane.add(label);

	}
}
