package ch17;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Ex01_FrameExam {
	public static void main(String[] args) {
		Frame f = new Frame();
		f.setTitle("프레임테스트");
		f.setSize(200, 200);
		f.setVisible(true);
		// 이벤트 소스 리스너, 핸들러

		// window, mouse, key, = 윈도우 이벤트. 마우스 이벤트. 키 이벤트
		// 소스. add000Listenner
		f.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		// new winodwListener 대신 WindowAdapter 로대체 하여, 불필요한 코딩량을 줄임
	
		// @Override
		// public void windowOpened(WindowEvent e) {
		// }
		// @Override
		// public void windowIconified(WindowEvent e) {
		// }
		//
		// @Override
		// public void windowDeiconified(WindowEvent e) {
		// }
		//
		// @Override
		// public void windowDeactivated(WindowEvent e) {
		//
		// }
		//
		// @Override
		// public void windowClosing(WindowEvent e) {
		// System.exit(0);
		//
		// }
		//
		// @Override
		// public void windowClosed(WindowEvent e) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void windowActivated(WindowEvent e) {
		// // TODO Auto-generated method stub
		//
		// }
		// });
	}
}
