package ch17;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Ex02_PanerExam extends JFrame{
	public Ex02_PanerExam(){
		JPanel p = new JPanel();
		//패널에는 2개 이상의 컴포넌트를 배치할 수 있음
		//패널에 여러개의 컴포넌트를 배치한 후 
		//프레임에 패널을 붙이면 1개를 붙인 효과가 됨.
		JButton button1 = new JButton("버튼1");
		JButton button2 = new JButton("버튼2");
		JButton button3 = new JButton("버튼3");
		JButton button4 = new JButton("버튼4");
		JButton button5 = new JButton("버튼5");
		JButton button6 = new JButton("버튼6");
//		setLayout(new FlowLayout());

		//add("변수명", "위치")
		p.add(button1);
		p.add(button6);
		add(p, "North");
		add(p, BorderLayout.NORTH);
		//add(button1, "North");
		add(button2, "Center"); // 위치를 생략하면 Center로 배치
		add(button2); 
		add(button3, "South");
		add(button4, "West");
		add(button5, "East");
		//한개 영역에 한개의 컴포넌트만 배치됨.
		//add(button6, "North");
		
		setSize(300,300);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	public static void main(String[] args) {
		new Ex02_PanerExam();
	}
}
