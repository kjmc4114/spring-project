package ch17;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

class BallCanvas extends Canvas implements Runnable, ComponentListener {
	// ComponentListener 컴포넌트의 위치 변경, 사이즈 변경 등의 이벤트 처리
	private int x, y;
	private int moveX = 2, moveY = 2;
	private int red, green, blue;
	private Random random;
	private int width, height;

	public BallCanvas() {
		addComponentListener(this);
		random = new Random();
		Thread thread = new Thread(this);
		thread.start();

	}

	@Override
	public void paint(Graphics g) {
		g.setColor(new Color(red, green, blue));
		g.fillOval(x, y, 20, 20);
	}

	void setColor(){
		red=random.nextInt(256);
		green=random.nextInt(256);
		blue = random.nextInt(256);
	}
	@Override
	public void run() {
		while (true) {
			if (x > (width - 20) || x < 0) {
				setColor();
				moveX = -moveX;
			}
			if (y > (height - 20) || y < 0) {
				setColor();
				moveY = -moveY;
			}
			x = x + moveX;
			y = y + moveY;
			repaint();

			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}
	}

	@Override
	public void componentResized(ComponentEvent e) {
		width = getWidth();
		height = getHeight();
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub

	}
}

public class Ex15_CanvasExam extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex15_CanvasExam frame = new Ex15_CanvasExam();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex15_CanvasExam() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		BallCanvas canvas = new BallCanvas();
		canvas.setBackground(Color.YELLOW);
		contentPane.add(canvas);
	}

}
