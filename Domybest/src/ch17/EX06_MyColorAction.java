package ch17;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
// 버튼클릭, 메뉴아이템 클릭
public class EX06_MyColorAction implements ActionListener {
	private JFrame f;
	private Color c;
	private Container con;
	public EX06_MyColorAction(JFrame f, Color c){
		this.f = f;
		con = f.getContentPane();
		this.c = c;
	}
	//component : 화면구성요소 
	// container : 다른 컴포넌트를 담을 수 있는 요소 
	
	@Override //버튼을 클릭할때 호출
	public void actionPerformed(ActionEvent e) {
	con.setBackground(c);	//배경색상 변경
	}

}
