package ch17;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class EX12_CheckboxExam extends JFrame {

	private JPanel contentPane;
	private JTextArea ta;
	private JCheckBox ckJava;
	private JCheckBox ckC;
	private JCheckBox ckIOT;
	private JCheckBox ckDB;
	private JButton btnReset;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EX12_CheckboxExam frame = new EX12_CheckboxExam();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EX12_CheckboxExam() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 521, 326);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JButton btnend = new JButton("종료");
		btnend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		contentPane.add(btnend, BorderLayout.SOUTH);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);

		ckJava = new JCheckBox("Java");
		ckJava.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				// ta.setText("자바를 신청했습니다."); // 일회성 텍스트, 한번 나오면 끝!
				System.out.println(e.getStateChange());
				if (e.getStateChange() == ItemEvent.SELECTED) {
					ta.append("자바를 신청했습니다.\n");
				} else {
					// ta.append("자바를취소했습니다.\n");
					ta.setText("");
				}
			}
		});
		panel.add(ckJava);

		ckC = new JCheckBox("C");
		ckC.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {

				if (e.getStateChange() == ItemEvent.SELECTED) {
					ta.append("C를 신청했습니다.\n");
				} else {
					ta.append("C를취소했습니다.\n");
				}
			}
		});
		panel.add(ckC);

		ckIOT = new JCheckBox("IoT");
		ckIOT.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					ta.append("IOT를 신청했습니다.\n");
				} else {
					ta.append("IOT를취소했습니다.\n");
				}
			}
		});
		panel.add(ckIOT);

		ckDB = new JCheckBox("DB");
		ckDB.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					ta.append("DB를 신청했습니다.\n");
				} else {
					ta.append("DB를취소했습니다.\n");
				}
			}
		});
		panel.add(ckDB);

		btnReset = new JButton("clear");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ta.setText("");
			}
		});
		panel.add(btnReset);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);

		ta = new JTextArea();
		scrollPane.setViewportView(ta);
	}

}
