package ch17;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class Ex03_GridExam extends JFrame {
	//프레임은 기본이 border임. 위치 미지정시 센터고 한영역에 하나밖에 안됨.
	public Ex03_GridExam() {
		
		setLayout(new FlowLayout());
		//setLayout(new GridLayout(4, 5));
		for(int i = 1 ; i<=6; i++){
			JButton button = new JButton("button"+i);
			add(button);
		}	
		setSize(300,300);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	
	}
	
	public static void main(String[] args) {
		new Ex03_GridExam();
	}
}
