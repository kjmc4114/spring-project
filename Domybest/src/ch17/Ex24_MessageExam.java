package ch17;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ex24_MessageExam extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex24_MessageExam frame = new Ex24_MessageExam();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex24_MessageExam() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnMessage = new JButton("Message");
		btnMessage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(Ex24_MessageExam.this, "message");
			}									// 어디에 띄울거냐. null 이면 전체화면 기준
												//외부클래스로 맞춰야함.
		});
		contentPane.add(btnMessage);
		
		JButton btnConfirm = new JButton("Confirm");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			//	int result = JOptionPane.showConfirmDialog(Ex24_MessageExam.this, "종료할까요?");
			//	if(result == JOptionPane.YES_OPTION){
					//0예 1아니오 2취소
			//	JOptionPane.showMessageDialog(Ex24_MessageExam.this, "프로그램을 종료합니다.");
					
			int result = JOptionPane.showConfirmDialog
		(Ex24_MessageExam.this, "종료할까요?", "가겠느냐", JOptionPane.YES_NO_OPTION);	
				
				System.exit(0);
				}
	//		}
		});
		contentPane.add(btnConfirm);
		
		JButton btnInput = new JButton("Input");
		btnInput.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		String name = JOptionPane.showInputDialog(Ex24_MessageExam.this, "이름을 입력하세요");
					JOptionPane.showMessageDialog(Ex24_MessageExam.this, name+"님 환영합니다.");
			}
		});
		contentPane.add(btnInput);
		
		JButton btnOption = new JButton("Option");
		btnOption.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String[] str = {"추가","수정", "삭제"};
				JOptionPane.showOptionDialog(Ex24_MessageExam.this,
						"선택하세요", "작업선택", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, str, str[0]);
			}																			//에러 메세지, WARING_MESSAGE	
		});
		contentPane.add(btnOption);
	}

}
