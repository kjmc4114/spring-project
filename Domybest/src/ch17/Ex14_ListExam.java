package ch17;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Ex14_ListExam {

	private JFrame frame;
	private JTextField tf;

	private DefaultListModel model;
	private JList list;
	private JButton btnAdd;
	private JButton btnDelete;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex14_ListExam window = new Ex14_ListExam();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ex14_ListExam() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		
		tf = new JTextField();
		tf.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				model.addElement(tf.getText());
				//데이터 모델갱신
				list.setModel(model);
				//입력포커스 설정
				tf.setText("");
				tf.requestFocus();
			}
		});
		panel.add(tf);
		tf.setColumns(10);
		
		btnAdd = new JButton("추가");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//텍스트필드에서 텍스트를 가져옴.
				model.addElement(tf.getText());
				//데이터 모델갱신
				list.setModel(model);
				//입력포커스 설정
				tf.setText("");
				tf.requestFocus();
			}
		});
		panel.add(btnAdd);
		
		btnDelete = new JButton("삭제");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedIndex = list.getSelectedIndex();
				if(selectedIndex !=-1){
					System.out.println(selectedIndex);
					model.remove(selectedIndex);
									}
			}
		});
		panel.add(btnDelete);
		
		//데이터 모델을 생성
		//JList에 모델을 입력
		model = new DefaultListModel<>();
		model.addElement("서울");
		model.addElement("인천");
		model.addElement("부산");
		model.addElement("대전");
		model.addElement("춘천");
		list = new JList(model);
		frame.getContentPane().add(list, BorderLayout.CENTER);
		
	}

}
