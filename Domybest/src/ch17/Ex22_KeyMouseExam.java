package ch17;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JApplet;

public class Ex22_KeyMouseExam extends JApplet implements MouseListener,MouseMotionListener,KeyListener {
private int x,y;
private int width, height;
private Image img;
private boolean flag = false; // 불린 변수

public void init(){ 
	setSize(800,600);
	img = Toolkit.getDefaultToolkit().getImage(getClass().getResource("IU01.jpg"));
//	this.addMouseListener(this);
//	this.addMouseMotionListener(this);
	this.addKeyListener(this);
	this.setFocusable(true);
	requestFocus();
}
public void paint (Graphics g){
	super.paint(g);
	//drawImage (이미지,x,y,이미지관찰자)
	width = img.getWidth(null);
	height = img.getHeight(null);
	//if (flag == true){
	g.drawImage (img, x,y,this);
	//}
}
@Override
public void mouseClicked(MouseEvent e) {
	
	System.out.println(e);
	flag = true;
	x = e.getX();
	y = e.getY();
	repaint();
}
@Override
public void mousePressed(MouseEvent e) {
	// TODO Auto-generated method stub
	
}
@Override
public void mouseReleased(MouseEvent e) {
	// TODO Auto-generated method stub
	
}
@Override
public void mouseEntered(MouseEvent e) {
	// TODO Auto-generated method stub
	
}
@Override
public void mouseExited(MouseEvent e) {
	// TODO Auto-generated method stub
	
}
@Override
public void mouseDragged(MouseEvent e) {
	// TODO Auto-generated method stub
	System.out.println("mouse move x:"+x+"y:"+y);
	
}
@Override
public void mouseMoved(MouseEvent e) {
	x = e.getX();
	y = e.getY();
//	System.out.println("mouse move x:"+x+"y:"+y);
	repaint();
	
}
@Override
public void keyTyped(KeyEvent e) {
	// 누르고 떼었을때
	
}
@Override
public void keyPressed(KeyEvent e) {
	// 눌렀을때
	System.out.println(e.getKeyCode());
	System.out.println(e.getKeyChar());
	switch(e.getKeyCode()){
	case KeyEvent.VK_UP:
		y=Math.max(0, y-5); break;
	case KeyEvent.VK_DOWN:
		y=Math.min(800-height, y+5); break;
	case KeyEvent.VK_LEFT:
		x=Math.max(0, x-5); break;
	case KeyEvent.VK_RIGHT:
		x=Math.min(600-width, x+5); break;
	
	}
	repaint();
}
@Override
public void keyReleased(KeyEvent e) {
	// TODO Auto-generated method stub
	
}
}
