package ch17;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Ex25_MenuExam extends JFrame {

	private JPanel contentPane;
	private JMenu mnFile;
	private JMenuItem mntmNewFile;
	private JMenuItem mntmOpen;
	private JMenuItem mntmSave;
	private JMenuItem mntnExit;
	private JMenu mnColor;
	private JMenuItem mntnRed;
	private JMenuItem mntmGreen;
	private JMenuItem mntmBlue;
	private JMenu mnHelp;
	private JPopupMenu popupMenu;
	private JMenuItem miRed;
	private JMenuItem pmniGreen;
	private JMenuItem pmniBlue;
	private JMenu mnNewMenu;
	private JMenuItem mntmNewMenuItem;
	private JMenuItem mntmNewMenuItem_1;
	private JMenuItem mntmNewMenuItem_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex25_MenuExam frame = new Ex25_MenuExam();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex25_MenuExam() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnFile = new JMenu("파일");
		menuBar.add(mnFile);
		
		mntmNewFile = new JMenuItem("새파일");
		mntmNewFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(Ex25_MenuExam.this, "새파일이 필요한가?");
			
			}
		});
		mnFile.add(mntmNewFile);
		
		mntmOpen = new JMenuItem("열기");
		mntmOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(Ex25_MenuExam.this, "열기");
			}
		});
		mnFile.add(mntmOpen);
		
		mntmSave = new JMenuItem("저장");
		mnFile.add(mntmSave);
		
		mntnExit = new JMenuItem("종료");
		mntnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int Result = JOptionPane.showConfirmDialog(Ex25_MenuExam.this, "종료할까요?");
				if (Result == JOptionPane.YES_OPTION){
					JOptionPane.showMessageDialog(Ex25_MenuExam.this, "종료합니다");
					System.exit(0);
				}
			}
		});
		mnFile.add(mntnExit);
		
		mnColor = new JMenu("색상");
		menuBar.add(mnColor);
		
		mntnRed = new JMenuItem("빨강");
		mntnRed.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == 10){
				contentPane.setBackground(Color.red);}
			}
		});
		mntnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			contentPane.setBackground(Color.red);
			}
		});
		mnColor.add(mntnRed);
		
		mntmGreen = new JMenuItem("초록");
		mntmGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.green);
			}
		});
		mnColor.add(mntmGreen);
		
		mntmBlue = new JMenuItem("파랑");
		mntmBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.blue);
			}
		});
		mnColor.add(mntmBlue);
		
		mnHelp = new JMenu("도움말");
		menuBar.add(mnHelp);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		popupMenu = new JPopupMenu();
		addPopup(contentPane, popupMenu);
		
		miRed = new JMenuItem("Red");
		miRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.red);
			}
		});
		popupMenu.add(miRed);
		
		pmniGreen = new JMenuItem("Green");
		pmniGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.green);
			}
		});
		popupMenu.add(pmniGreen);
		
		pmniBlue = new JMenuItem("Blue");
		pmniBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				contentPane.setBackground(Color.blue);
			}
		});
		
		mnNewMenu = new JMenu("New menu");
		popupMenu.add(mnNewMenu);
		
		mntmNewMenuItem = new JMenuItem("New menu item");
		mnNewMenu.add(mntmNewMenuItem);
		
		mntmNewMenuItem_1 = new JMenuItem("New menu item");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		mntmNewMenuItem_2 = new JMenuItem("New menu item");
		mnNewMenu.add(mntmNewMenuItem_2);
		popupMenu.add(pmniBlue);
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
