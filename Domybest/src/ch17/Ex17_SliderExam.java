package ch17;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.metal.MetalSliderUI;

public class Ex17_SliderExam extends JFrame {

	private JPanel contentPane;
	private JSlider sliderR;
	private JSlider sliderG;
	private JSlider sliderB;
	private JPanel panel;
	private int red,green,blue;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex17_SliderExam frame = new Ex17_SliderExam();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex17_SliderExam() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 553, 451);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.setBounds(12, 278, 513, 125);
		contentPane.add(panel);
		
		sliderR = new JSlider();
		sliderR.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				red=sliderR.getValue();
				panel.setBackground(new Color(red,green,blue));
			}
		});
		sliderR.setMaximum(255);
		sliderR.setValue(0);
		sliderR.setBounds(12, 38, 410, 42);
		contentPane.add(sliderR);
		
		sliderG = new JSlider();
		sliderG.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				green = sliderG.getValue();
				panel.setBackground(new Color(red,green,blue));
			}
		});
		sliderG.setMaximum(255);
		sliderG.setValue(0);
		sliderG.setBounds(12, 115, 410, 36);
		contentPane.add(sliderG);
		
		sliderB = new JSlider();
		sliderB.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				blue = sliderB.getValue();
				panel.setBackground(new Color(red,green,blue));
			}
		});
		sliderG.setUI(new MetalSliderUI(){
			@Override
			protected void scrollDueToClickInTrack(int dir){
				green = sliderG.getValue();
				green = valueForXPosition(
						sliderG.getMousePosition().x);
				sliderG.setValue(green);
				panel.setBackground(new Color(red,green,blue));
			}
		});
		sliderR.setUI(new MetalSliderUI(){
			@Override
			protected void scrollDueToClickInTrack(int dir){
				red = sliderR.getValue();
				red = valueForXPosition(
						sliderR.getMousePosition().x);
				sliderR.setValue(red);
				panel.setBackground(new Color(red,green,blue));
			}
		});
		sliderB.setUI(new MetalSliderUI(){
			@Override
			protected void scrollDueToClickInTrack(int dir){
				blue = sliderB.getValue();
				blue = valueForXPosition(
						sliderB.getMousePosition().x);
				sliderB.setValue(blue);
				panel.setBackground(new Color(red,green,blue));
			}
		});
		
		 sliderR.setMajorTickSpacing(40); //큰 눈금
		 sliderR.setMinorTickSpacing(10); //작은 눈금
		 sliderR.setPaintTicks(true); //눈금 표시
		 sliderR.setPaintLabels(true); // 숫자 표시
		
		
		sliderB.setMaximum(255);
		sliderB.setValue(0);
		sliderB.setBounds(12, 186, 410, 46);
		contentPane.add(sliderB);
		
		JLabel lblR = new JLabel("RED");
		lblR.setBounds(12, 13, 57, 15);
		contentPane.add(lblR);
		
		JLabel lblG = new JLabel("Green");
		lblG.setBounds(12, 90, 57, 15);
		contentPane.add(lblG);
		
		JLabel lblB = new JLabel("Blue");
		lblB.setBounds(12, 161, 57, 15);
		contentPane.add(lblB);
	}
}
