package ch17;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

public class Ex16_ProgressExam extends JFrame  {

	private JPanel contentPane;
	private JProgressBar progress;
	private JButton btnStrat;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex16_ProgressExam frame = new Ex16_ProgressExam();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex16_ProgressExam() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		progress = new JProgressBar();
		progress.setStringPainted(true);
		contentPane.add(progress);
		
		btnStrat = new JButton("시작");
		btnStrat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Thread th = new Thread(new Runnable() {
					
					@Override
					public void run() {
						
				for(int i = 1 ; i<=100; i++){
					progress.setValue(i);
				try {
					Thread.sleep(100);
				} catch (InterruptedException e1) {
				e1.printStackTrace();
				}
			}
				
				JOptionPane.showMessageDialog(
						Ex16_ProgressExam.this, "완료되었습니다");
				// 어느 창에 띄울 것인지 , 텍스트
						
					}
				});
				th.start();
			}
		});
		contentPane.add(btnStrat);
	}

}
