package ch17;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;

public class Ex20_TreeExam extends JFrame {

	private JPanel contentPane;

	private Object[] obj = {"프로그램","시스템","디자인"};
	private Vector<Vector> node1 = new Vector<Vector>(){
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "Lesson";
		}
	};
	private Vector<String> node2 = new Vector<String>(){
		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return "JAVA";
		}
	};
	private Vector<String> node3 = new Vector<String>(){
		@Override
		public String toString() {
			return "XML";
		}
	};
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex20_TreeExam frame = new Ex20_TreeExam();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex20_TreeExam() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
//		JTree tree = new JTree();
//		tree.setModel(new DefaultTreeModel(
//			new DefaultMutableTreeNode("Console\t") {
//				{
//					DefaultMutableTreeNode node_1;
//					node_1 = new DefaultMutableTreeNode("nintendo");
//						node_1.add(new DefaultMutableTreeNode("switch"));
//						node_1.add(new DefaultMutableTreeNode("3ds"));
//						node_1.add(new DefaultMutableTreeNode("N64"));
//					add(node_1);
//					node_1 = new DefaultMutableTreeNode("PlayStation");
//						node_1.add(new DefaultMutableTreeNode("Ps4"));
//						node_1.add(new DefaultMutableTreeNode("Ps3"));
//						node_1.add(new DefaultMutableTreeNode("Ps2"));
//						node_1.add(new DefaultMutableTreeNode("Psvita"));
//					add(node_1);
//					node_1 = new DefaultMutableTreeNode("Xbox");
//						node_1.add(new DefaultMutableTreeNode("xbox360"));
//						node_1.add(new DefaultMutableTreeNode("xboxone"));
//						node_1.add(new DefaultMutableTreeNode("xboxone X"));
//					add(node_1);
//				}
//			}
//		));
		
		
		node1.add(node2);
		node1.add(node3);
		node2.add("C++");
		node2.add("JAVA");
		node3.add("XSLT");
		node3.add("DOM");
		obj[0] =node1;
		JTree tree = new JTree(obj);
		tree.setRootVisible(true);
		
		scrollPane.setRowHeaderView(tree);
	}

}
