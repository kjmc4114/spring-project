package ch17;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Font;

public class EX10_ClockExam extends JFrame implements Runnable{

	private JPanel contentPane;
	private JLabel lblTime;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EX10_ClockExam frame = new EX10_ClockExam();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EX10_ClockExam() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		lblTime = new JLabel("");
		lblTime.setFont(new Font("돋움", Font.PLAIN, 17));
		lblTime.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblTime, BorderLayout.CENTER);
		
		JButton btnClose = new JButton("종료");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			System.exit(0);
			}
		});
		contentPane.add(btnClose, BorderLayout.SOUTH);
		Thread th = new Thread(this);
		th.start();
	}

	@Override
	public void run() {
	
		for(;;){Calendar cal = Calendar.getInstance();
//		String str = cal.get(Calendar.YEAR)+"-"+(cal.get((Calendar.MONTH))+1)+"-"+cal.get(Calendar.DATE)+" "+
//				cal.get(Calendar.HOUR_OF_DAY)+":"+cal.get(Calendar.MINUTE)+":"+cal.get(Calendar.SECOND);
		String str = String.format("%d-%02d-%02d \n %02d:%02d:%02d", cal.get(Calendar.YEAR),cal.get(Calendar.MONTH)+1,cal.get(Calendar.DATE),cal.get(Calendar.HOUR)
				,cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));
		lblTime.setText(str);}
		//레이블에 텍스트를 입력
	
	}

}
