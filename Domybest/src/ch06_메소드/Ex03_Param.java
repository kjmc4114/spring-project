package ch06_메소드;

public class Ex03_Param {
	static int plus(int a, int b){
		return a+b;
	}
	static int minus(int a, int b){
		return a-b;
	}
	static int multi(int a, int b){
		return a*b;
	}
	static int divide(int a, int b){
		return a/b;
	}
	static int sum(int a, int b){
		int result =0;
		for(int i=a; a<=b; i++){
			result=result+i;
		}
		return result;
	}
	public static void main(String[] args){
		int n;
		n= plus(10, 20);
		System.out.println("plus"+n);
		n= minus(100, 20);  //우리가 만든건 step into (f5), 만들어진것들은 step over (f6)
		System.out.println("minus"+n);
		n= multi(10, 5);
		System.out.println("multi"+n);
		n= divide(10, 5);
		System.out.println("나누기"+n);
		n= sum(1,10);
		System.out.println("합하기"+n);
	}
}
