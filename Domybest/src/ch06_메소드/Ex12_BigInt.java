package ch06_메소드;
import java.math.BigInteger;
public class Ex12_BigInt {
static BigInteger factorial(int n){
	BigInteger fac=BigInteger.ONE; //초기값 설정
	for(int i=1; i<=n; i++){
		fac = fac.multiply(BigInteger.valueOf(i)); //객체로 전향
	} return fac;
}

public static void main(String[] args) {
	for(int i=1; i<=100; i++){
		System.out.println(i+"팩토리얼="+factorial(i));
		System.out.println();
		}
	}
}