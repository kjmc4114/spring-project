package ch06_메소드;

public class Ex05_Abs {

	static int abs(int num){
		return num>0 ? num : -num;
	}
	
	public static void main(String[] args) {
	int a=-5;
	System.out.println(a);
	System.out.println(abs(a));
	System.out.println(Math.abs(a));
}
}
