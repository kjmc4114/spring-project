package ch13_유용한클래스;

import java.util.Random;

public class Ex15_Rand {
public static void main(String[] args) {
	Random r = new Random();
	System.out.println(r.nextInt()); // 임의의 정수
	System.out.println(r.nextInt(100));// 00~99
	System.out.println(Math.random()); //0.0~1.0의 실수
	int num = (int)(Math.random()*100); // 정수로 만들고 싶을때
	System.out.println(num);
	String[] prize = {"1등", "2등" , "3등 ", "다음기회에"};
		System.out.println(prize[r.nextInt(4)]); // 0~3
	}
}
