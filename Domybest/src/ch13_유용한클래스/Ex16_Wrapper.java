package ch13_유용한클래스;

public class Ex16_Wrapper { //기본자료형을 변환하는 다양한 클래스
	public static void main(String[] args) {
//		byte Byte
//		short 
//		int Integer
//		long Long
//		float Float
//		double Double
//		char Character
//		boolean Boolean
//		
		
		Integer i1 = new Integer(10);
		Integer i3 = 10; //주소값 = 자료값 원래는 안됨. 허용이라는 것을 알아 둘것.
		Integer i2 = new Integer(20);
		int num1=i1.intValue();
		int num2= i2; 
		int sum = num1 + num2;
		System.out.println(sum);
		System.out.println(Integer.toBinaryString(sum));
		System.out.println(Integer.toOctalString(sum));
		System.out.println(Integer.toHexString(sum));
		System.out.println(Integer.MAX_VALUE);
		System.out.println(Integer.MIN_VALUE);
		System.out.println(Integer.parseInt("100"));
		System.out.println(Integer.toString(100));
		System.out.println(100+"");
	}

}
