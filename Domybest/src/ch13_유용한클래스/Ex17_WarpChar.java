package ch13_유용한클래스;

public class Ex17_WarpChar {
public static void main(String[] args) {
	char[] data = {'J','a','V','a','1','#'};
	for (int i = 0; i<data.length; i++){
//	대문자 변환	data[i] = Character.toUpperCase(data[i]);
//	소문자 변환	data[i] = Character.toLowerCase(data[i]);
		if (Character.isUpperCase(data[i])){System.out.println(data[i]+": 대문자");}
		else if (Character.isLowerCase(data[i])){System.out.println(data[i]+": 소문자");}
		else if (Character.isDigit(data[i])){System.out.println(data[i]+": 숫자");}
		else {System.out.println(data[i]+": 기타문자");}
	}
	System.out.println("문자변환 후 ");
	for (int i = 0; i<data.length; i++){
		
		data[i] = Character.toUpperCase(data[i]);
//		data[i] = Character.toLowerCase(data[i]);
		if (Character.isUpperCase(data[i])){System.out.println(data[i]+": 대문자");}
		else if (Character.isLowerCase(data[i])){System.out.println(data[i]+": 소문자");}
		else if (Character.isDigit(data[i])){System.out.println(data[i]+": 숫자");}
		else {System.out.println(data[i]+": 기타문자");}
	}




}
}
