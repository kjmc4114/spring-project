package ch13_유용한클래스;

public class Ex07_StrMethod {
	public static void main(String[] args) {
		String str1="java";
		str1 += " programming";
		System.out.println(str1.length());
	String str2 = str1.concat("programming");
	System.out.println(str2);
	System.out.println(str1);
	System.out.println(str1.charAt(2));
	System.out.println(str2.indexOf("program"));
	System.out.println(str2.indexOf("z")); //내용이없으면 -1
	System.out.println(str2.substring(0,4));
	System.out.println(str2.substring(5)); //5번부터 쭈욱~
	System.out.println(str2.replace("java","jsp"));
	System.out.println(str2);
	str2 = str2.replace("java","jsp"); //갱신이 필요함 불변성을 이해.
	System.out.println(str2);	
	}

}
