package ch13_유용한클래스;

import java.util.Random;

public class Ex01_수학처리_반올림_올림 {

	public static void main(String[] args) {
		System.out.println(Math.ceil(10.1)); //올림
		System.out.println(Math.round(10.1)); //반올림
		System.out.println(Math.floor(10.1)); // 버림
		System.out.println(Math.pow(10,3)); //10의 3승 제곱근
		System.out.println(Math.pow(2,3));
		for(int i=1; i<=10; i++){
			//0.0~1.0 사이의 난수. 아무것도 안넣었을때 
			System.out.println(Math.random());
		}
		Random rand=new Random();
		System.out.println("정수값"+ rand.nextInt(100));
		System.out.println("실수값"+ rand.nextDouble());
		for(int i=1; i<=10; i++){
			System.out.println((int)(Math.random()*100)); //소숫점이 나옴.을 생각할것.
		}
	}
}
