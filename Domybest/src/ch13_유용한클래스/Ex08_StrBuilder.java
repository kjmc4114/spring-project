package ch13_유용한클래스;
//불변과 가변성의 차이.
public class Ex08_StrBuilder {
public static void main(String[] args) {
	StringBuilder str1 = new StringBuilder(/*초기값을 넣는곳*/);
	str1.append("java");
	System.out.println(str1);
	str1.append(" programming");
	System.out.println(str1);
	
	str1.replace(0, 4, "jsp");
	System.out.println(str1);
	String str2=str1.substring(3);
	System.out.println(str1);
	System.out.println(str2);
	
	String str3 = "IU";
	str3 = str1.toString();//자료 형변환을 해줘야함 빌더를 스트링으로
	str1 = new StringBuilder(str3); //자료형변환 스트링을 빌더로
	
	System.out.println(str3);
	System.out.println(str1);
}
}
