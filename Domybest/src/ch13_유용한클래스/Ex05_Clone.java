package ch13_유용한클래스;

public class Ex05_Clone implements Cloneable {
int num = 10;
void pring(){
	System.out.println(num);
}

public static void main (String[] args){
	
	Ex05_Clone t1 = new Ex05_Clone();
	Ex05_Clone t2 = null;
	try {
		t2=(Ex05_Clone)t1.clone(); // clone 메소드 , (Ex05_Clone) = 형변환
	} catch (CloneNotSupportedException e) {

		e.printStackTrace(); //개발자를 위한 원인 분석 코드
	}
	System.out.println(t1);
	System.out.println(t2);
	t1.pring();
	t2.pring();
}
}
