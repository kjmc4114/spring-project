package ch13_유용한클래스;

import java.util.Calendar;
import java.util.Date;

public class Ex10_달력 {
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance(); 
		
		System.out.println(cal.get(Calendar.YEAR));
		System.out.println(cal.get(Calendar.MONTH)+1);
		System.out.println(cal.get(Calendar.DATE));
		System.out.println(cal.get(Calendar.HOUR));
		System.out.println(cal.get(Calendar.MINUTE));
		System.out.println(cal.get(Calendar.SECOND));
		System.out.println(cal.get(Calendar.AM_PM));
		System.out.println(cal.get(Calendar.DAY_OF_WEEK_IN_MONTH));
		System.out.println(cal.get(Calendar.DAY_OF_MONTH));
		System.out.println(cal.get(Calendar.DAY_OF_YEAR));
		
		String yoil="";
		
		
		switch( cal.get(Calendar.DAY_OF_WEEK) ){
		
		case 1: yoil ="일요일"; break;
		case 2: yoil ="월요일"; break;
		case 3: yoil ="화요일"; break;
		case 4: yoil ="수요일"; break;
		case 5: yoil ="목요일"; break;
		case 6: yoil ="금요일"; break;
		case 7: yoil ="토요일"; break;
		}
		System.out.println(yoil);
		if(cal.get(Calendar.AM_PM)==0){
			System.out.println("오전");
		}else{
			System.out.println("오후");
		}
	
 Date date = cal.getTime();
 System.out.println(date.getYear()+1900);
 System.out.println(date.getMonth());
	
	}
	

}
