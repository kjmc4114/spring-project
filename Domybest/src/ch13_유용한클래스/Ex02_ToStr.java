package ch13_유용한클래스;

public class Ex02_ToStr {
public static void main(String[] args) {
	
	Object obj = new Object();
	obj = new Integer(20);
	System.out.println( obj);
	obj = new Double(20.5);
	System.out.println(obj);
	obj = new String("hello");
	System.out.println(obj);

}

}
