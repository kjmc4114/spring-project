package ch13_유용한클래스;

import java.util.Arrays;

public class Ex14_ArrSort {
public static void main(String[] args) {
//	int a = 10;
//	int b = 20;
//	int temp;
//	temp=a;
//	a=b;
//	a=temp;
//	System.out.println(a);
//	System.out.println(b);

int[] num = {50,40,70,90,120,-20};
System.out.println("정렬하기 전");
for(int i=0; i<num.length; i++){
	System.out.println(num[i]+"\t");
}

int temp=0;
for(int i=0; i<num.length; i++){
	for ( int j = i+1; j<num.length; j++){
	if(num[i]>num[j]){
			temp = num[i];
			num[i]=num[j];
			num[j]=temp;  // 최종적으로 맨앞에 오게됨. 
			
		}
	}
}
Arrays.sort(num); // 오름차순 정렬 위에 식과 같음
System.out.println("\t 정렬한 후");
System.out.println("오름차순");
for(int i=0; i<num.length; i++){
	System.out.println(num[i] + "\t");
}
System.out.println("내림차순");
for(int i=num.length-1; i>=0; i--){
	System.out.println(num[i] + "\t");
}


}
}
