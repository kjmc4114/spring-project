package ch13_유용한클래스;

public class Ex03_ObjCast {
public static void main(String[] args) {
	int a = 10;
	Object obj = new Object();
	obj=20;
	//a=obj; // error why: 좌변에 더 큰 자료형 (부모) 우변에는 더 작은 자료형(자식)
	a=(int)obj; //더 큰 자료형이 올경우 형변환을 통해서 서로 맞춰 줘야함.
	System.out.println(a);
	Object[] obj2 = {100,200,5,true,"hello",'A'};
	for(Object o : obj2){System.out.println(o);}
}
}
