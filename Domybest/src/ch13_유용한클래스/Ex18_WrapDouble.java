package ch13_유용한클래스;

public class Ex18_WrapDouble {
public static void main(String[] args) {
	Double num1 = new Double("11.5");
	double num2 = 3.5; //auto boxing;;
	double num3 = num1/num2; // auto unboxing;; (참조형 변수의 주소값에 있는 자룟값을 꺼냄)

	System.out.println(num3);
	System.out.println(Double.MAX_VALUE);
	System.out.println(Double.MIN_VALUE);
	String str="11.5";
	System.out.println(str+200); //200을 "200"으로 자동변환됨.
	System.out.println(Double.parseDouble(str)+200);
}


}
