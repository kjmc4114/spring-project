package 교수님;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class Ex27_taxi extends JFrame {

	private JPanel contentPane;
	private JTextField tfKm;
	private int fee;
	private int bonus1,bonus2;
	private int total;
	private JLabel lblResult;
	private JCheckBox ck1;
	private JCheckBox ck2;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ex27_taxi frame = new Ex27_taxi();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ex27_taxi() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("운행거리(km)");
		lblNewLabel.setBounds(22, 42, 80, 15);
		contentPane.add(lblNewLabel);
		
		tfKm = new JTextField();
		tfKm.setBounds(106, 39, 116, 21);
		contentPane.add(tfKm);
		tfKm.setColumns(10);
		
		JButton btnEval = new JButton("계산");
		btnEval.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				eval();
			}
		});
		btnEval.setBounds(107, 105, 97, 23);
		contentPane.add(btnEval);
		
		JLabel label = new JLabel("요금 : ");
		label.setBounds(46, 130, 57, 15);
		contentPane.add(label);
		
		lblResult = new JLabel("");
		lblResult.setBounds(105, 130, 57, 15);
		contentPane.add(lblResult);
		
		ck1 = new JCheckBox("심야할증");
		ck1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				eval();
			}
		});
		ck1.setBounds(106, 66, 115, 23);
		contentPane.add(ck1);
		
		ck2 = new JCheckBox("시외요금");
		ck2.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				eval();
			}
		});
		ck2.setBounds(237, 66, 115, 23);
		contentPane.add(ck2);
	}
	void eval(){
		int m=Integer.parseInt( tfKm.getText() ) * 1000;
		if(m <= 2000){
			fee=3000;
		}else{
			int temp=m-2000;
			fee= 3000+(((int)Math.ceil(temp / 140.0))*150);
		}
		//심야요금
		bonus1=ck1.isSelected() ? (int)(fee * 0.2) : 0; //2가지 조건은 2 ? : 조건문 사용하기.
		bonus2=ck2.isSelected() ? (int)(fee * 0.2) : 0;		
		total = fee+bonus1+bonus2;
		lblResult.setText(total+"");
	}
}
