package Class;

import java.awt.EventQueue;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import ch17.Ex24_MessageExam;

import java.awt.Font;

public class Re_28 extends JFrame {

	private JPanel contentPane;
	private JTextField tfName;
	private JTextField tfKorean;
	private JTextField tfEng;
	private JTextField tfMath;
	private String message;
	private int Korean;
	private int Eng;
	private int Math;
	private int sum;
	private int avg;
	private Label lblResult;
	private String name;

	void calc() {

		name = tfName.getText();
		Korean = Integer.parseInt(tfKorean.getText());
		Eng = Integer.parseInt(tfEng.getText());
		Math = Integer.parseInt(tfMath.getText());
		sum = Korean + Eng + Math;
		avg = (Korean + Eng + Math) / 3;
		lblResult.setText("<html><body>"+name+"</body></html>");
	}

	void popup(String message) {
		JOptionPane.showMessageDialog(Re_28.this, message);
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Re_28 frame = new Re_28();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Re_28() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 452, 536);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("이름 ");
		label.setBounds(22, 66, 99, 29);
		contentPane.add(label);
		
		JLabel lblKorean = new JLabel("국어");
		lblKorean.setBounds(22, 125, 99, 29);
		contentPane.add(lblKorean);
		
		JLabel lblEng = new JLabel("영어");
		lblEng.setBounds(22, 175, 99, 29);
		contentPane.add(lblEng);
		
		JLabel lblmath = new JLabel("수학");
		lblmath.setBounds(22, 224, 99, 29);
		contentPane.add(lblmath);
		
		tfName = new JTextField();
		tfName.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tfName.getText()==""){JOptionPane.showMessageDialog(Re_28.this, "?");;}
			
			}
		});
		tfName.setBounds(117, 63, 186, 35);
		contentPane.add(tfName);
		tfName.setColumns(10);
		
		tfKorean = new JTextField();

		tfKorean.setBounds(117, 122, 186, 35);
		contentPane.add(tfKorean);
		tfKorean.setColumns(10);
		
		tfEng = new JTextField();
	
		tfEng.setBounds(117, 172, 186, 35);
		contentPane.add(tfEng);
		tfEng.setColumns(10);
		
		tfMath = new JTextField();
		tfMath.setBounds(117, 221, 186, 35);
		contentPane.add(tfMath);
		tfMath.setColumns(10);
		
		JButton btnCalc = new JButton("계산");
		btnCalc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(name==null){popup("이름을대시오");
				}else if (Korean==0){JOptionPane.showMessageDialog(Re_28.this, "국어점수대시오");
					}else if (Eng==0){JOptionPane.showMessageDialog(Re_28.this, "영어점수대시오");
					}else if (Math==0){JOptionPane.showMessageDialog(Re_28.this, "수학점수대시오");}
				
				
				calc();
				
				
			}
		});
		btnCalc.setBounds(59, 284, 153, 37);
		contentPane.add(btnCalc);
		
		JButton btnReset = new JButton("취소");
		btnReset.setBounds(234, 284, 153, 37);
		contentPane.add(btnReset);
		
		lblResult = new Label("");

		lblResult.setFont(new Font("Consolas", Font.PLAIN, 12));
		lblResult.setBounds(22, 347, 378, 90);
		contentPane.add(lblResult);
	}
}