package Class;

import java.util.Scanner;

public class Re13_Point_class {
private String name;
private String part;
private String grade;
private String num;
private int kor;
private int eng;
private int math;
private int tot;
private double avg;
public void infor(){
	Scanner infor=new Scanner(System.in);
	System.out.println("이름을 입력하세요");
	name=infor.nextLine();
	System.out.println("학과를 입력하세요");
	part=infor.nextLine();
	System.out.println("학년을 입력하세요");
	grade=infor.nextLine();
	System.out.println("학번을 입력하세요");
	num=infor.nextLine();
	System.out.println("국어점수를 입력하세요");
	kor=infor.nextInt();
	System.out.println("영어점수를 입력하세요");
	eng=infor.nextInt();
	System.out.println("수학점수를 입력하세요");
	math=infor.nextInt();
	

}


public void input(String name, String part, String grade, String num, int kor, int eng, int math) {
	this.name = name;
	this.part = part;
	this.grade = grade;
	this.num = num;
	this.kor = kor;
	this.eng = eng;
	this.math = math;
}
public int getTotal(){
	tot=kor+eng+math;
	return tot;
}

public double getAverage(){	
	double avg= (tot / 3.0);
	return avg;
	
	
}
@Override
public String toString() {
	String rlt 
	= "이름:"+name+"\n"+"학과:"+part+"\n"+"학년:"+grade+"\n"+
	"학번:"+num+"\n"+"국어:"+kor+"\n"+"영어:"+eng+"\n"+
			"수학:"+math+"\n"+"총점:"+getTotal()+"\n"+
	"평균:"+ getAverage();
	
	return rlt;
} 
}
