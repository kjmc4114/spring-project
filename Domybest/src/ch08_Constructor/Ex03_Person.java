package ch08_Constructor;

public class Ex03_Person {
private String name;
private int height;
private int weight;



public Ex03_Person(){
	this("김철수",150,100);
	
}
public Ex03_Person(String name){
	this(name,190);

}
public Ex03_Person(String name, int height){
	this(name,height,50);
}
public Ex03_Person(String name, int height, int weight){
	this.name=name;
	this.height=height;
	this.weight=weight;
}
public void showInfo(){
System.out.println("===신상 정보===");	
System.out.println("이  름:  "+name);	
System.out.println("신  장:  "+height);	
System.out.println("체  중:  "+weight);	
}
}
