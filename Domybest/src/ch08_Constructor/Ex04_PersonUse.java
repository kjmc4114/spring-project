package ch08_Constructor;

public class Ex04_PersonUse {
public static void main(String[] args) {
	Ex03_Person p1=new Ex03_Person();
	p1.showInfo();
	Ex03_Person p2=new Ex03_Person("김상범");
	p2.showInfo();
	Ex03_Person p3=new Ex03_Person("박찬호",169);
	p3.showInfo();
	Ex03_Person p4=new Ex03_Person("아이유",160,45);
	p4.showInfo();
	
}
}
