package ch08_Constructor;

public class Ex05_SingleTone {
	private static Ex05_SingleTone inst;
	public static Ex05_SingleTone getInstance() {
		if(inst==null){
			inst=new Ex05_SingleTone();
		}
		return inst;
	}
	private Ex05_SingleTone() {
	
	}

}
