package ch08_Constructor;

public class Ex02_CalculatorUse {
public static void main(String[] args) {
	Ex01_Calculator c1= new Ex01_Calculator();
	c1.setLeft(10);
	c1.setRight(20);
	System.out.println(c1.getLeft());
	System.out.println(c1.getRight());
	Ex01_Calculator c2= new Ex01_Calculator(10,20);
	c2.sum();
	c2.avg();
	Ex01_Calculator c3= new Ex01_Calculator(20,40);
	c3.sum();
	c3.avg();
	
}
}
