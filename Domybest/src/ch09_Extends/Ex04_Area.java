package ch09_Extends;

public class Ex04_Area {
		private int width;
		private int length;
public Ex04_Area(){} //기본 생성자는  꼭 만들것.


public Ex04_Area(int width, int length){
		this.width=width;
		this.length=length;
	}

	public int getArea(){
		return width*length;
	}
}
