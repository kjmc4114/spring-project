package ch09_Extends;

public class Ex09_BookSaleUse {

	public static void main(String[] args) {
		Ex08_BookSale b1=new Ex08_BookSale();
		b1.setBookName("아이유일대기");
		b1.setAuthor("아이유");
		b1.setPress("정명");
		b1.setYear(2017);
		b1.setPrice(30000);
		b1.setAmount(500);
		b1.setRank(1);
		b1.setMoney();
		b1.print();
	}
}
