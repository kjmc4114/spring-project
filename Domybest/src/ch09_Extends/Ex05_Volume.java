package ch09_Extends;

public class Ex05_Volume extends Ex04_Area {

//	public Ex05_Volume(int width, int length) {
//		super(width, length);
//		 
//	} 기본생성자가 있으면 이러한 부모생성자를 만들필요가 없음.
	
private int height;

public Ex05_Volume(){} //기본 생성자

public Ex05_Volume(int width, int length, int height){
	super(width, length); //생성자를 가져오는거지.
	this.height = height;
}
public int getVolume(){
	return getArea()*height; 
}

}
