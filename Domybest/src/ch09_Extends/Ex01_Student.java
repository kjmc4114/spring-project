package ch09_Extends;

public class Ex01_Student {
	protected String name;
	protected String num;
	protected String major;
	protected int year;
	public Ex01_Student(){
		
	}
	public Ex01_Student(String name, String num, String major, int year){
		this.name=name;
		this.num=num;
		this.major=major;
		this.year = year;
	}
	
	public void input(String name, String num, String major, int year){
		this.name=name;
		this.num=num;
		this.major=major;
		this.year = year;
	}
	public void print(){
		System.out.println("이름\t학번\t전공\t학년");
		System.out.println(name+"\t"+num+"\t"+major+"\t"+year);
		System.out.println(name);
	}
}
