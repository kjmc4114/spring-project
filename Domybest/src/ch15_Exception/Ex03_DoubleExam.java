package ch15_Exception;


import java.util.Scanner;

public class Ex03_DoubleExam {
	public static void main(String[] args) {
		int num = 50;
		Scanner scan = null;
		try {
			System.out.println("숫자를 입력하세요");
			scan = new Scanner(System.in);
			int value = scan.nextInt();
			System.out.println(num / value);
		} catch (ArithmeticException e) {
		System.out.println("0으로 나눌 수 없습니다.");
		} catch (NullPointerException e){
			System.out.println("입력기능이 없습니다");
		} catch (Exception e){ 
			System.out.println("숫자만 입력가능합니다.");
			
		}finally{
			if (scan !=null) scan.close();
		}
		System.out.println("프로그램 종료");
		
	
	
	}
}

