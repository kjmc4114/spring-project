package ch15_Exception;

public class Ex01_ExceptionExam {
	// Exception : 예외 ~~Exception = 예외처리 클래스
	public static void main(String[] args) {
		int a = 10, b = 0, c;
		try {
			c = a / b;
			System.out.println(c);
			//ArithmeticException 
		} catch (Exception e) {
		//자세한 Exception 을 적을 필요는 없음. 산술연산은  AirthmeticException 임.
			//System.out.println("0으로 나눌 수 없습니다.0")			
		e.printStackTrace();	 // 예외가 발생한 상태의 스택정보 출력
	
		}
		System.out.println("프로그램이 종료되었습니다.");
	}
}
