package ch16_스레드;

import java.applet.Applet;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

public class Ex10_Anim extends Applet implements Runnable {
	private Image[] img;
	private int idx; // 이미지 배열의 인덱스값

	@Override // 애플릿 초기화 매소드
	public void init() {
		img = new Image[10];
		for (int i = 0; i < img.length; i++) {
			img[i] = Toolkit.getDefaultToolkit().getImage(getClass().getResource("duke" + (i + 1) + ".gif"));
			// 이미지를 가져오는 매소드
			// getClasS()현재클래스의 경로 . getResource("리소스 이름) 현재 클래스가 속한 디렉토리의 리소스참조
		}

		Thread t = new Thread(this);
		// 새로운 작업단위가 할당되면서 run 매소드가 호출됨. 백그라운드 스레드 생성
		// 우변()에는 run 매소드가 실행되는 장소 입력할것.
		t.start();
	//애플릿 초기 사이즈 조정	
		setSize(500,500); 
	}

	@Override // 애플릿의 화면 출력작업
	public void paint(Graphics g) {
		// for(int i=0; i<img.length; i++){
		//
		// try {
		// Thread.sleep(1000);
		// } catch (InterruptedException e) {
		//
		// e.printStackTrace();}

		// g.drawImage(이미지, x좌표,y좌표, 이미지관찰자)
		// 이미지 관찰자 : 이미지를 변형하는 작업에 필요.

		g.drawImage(img[idx], 90, 73, this);

	}

	@Override
	public void run() {
		while (true) {
			idx++;
			if (idx >= 10) {
				idx = 0;
			}
			repaint(); //그래픽처리를 새로해주길 요청
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

}
