package ch16_스레드;

public class Ex03_RunnableExam implements Runnable {

	@Override
	public void run() {
		for(int i =1; i<=5; i++){
			System.out.println(
					Thread.currentThread().getName());
			try{
				Thread.sleep(1000);
			}catch (Exception e){
				e.printStackTrace();
			}
		}
		
	}
	public static void main(String[] args) {
		Ex03_RunnableExam e1 = new Ex03_RunnableExam();
		Thread t1=new Thread(e1,"thread1");
		Thread t2=new Thread(e1,"thread2");
		t1.start(); //run()이 호출됨 //새로운 작업단위를 추가할것.
		t2.start(); // 
	}

}
