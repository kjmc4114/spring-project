package ch16_스레드;

public class Ex12_ATm extends Thread {
	private long depositMoney = 100000;

	@Override
	public void run() {
		for (int i = 1; i <= 10; i++) {
			withDraw(10000);
		}
	}

	public synchronized void withDraw(int money) {
		// 동기화(순차적인 처리)

		String name = Thread.currentThread().getName();
		System.out.println(name);
		if (depositMoney > 0) {
			depositMoney = depositMoney - money;
			System.out.println(name + "잔액" + depositMoney);
		} else {
			System.out.println("잔액이없습니다.");
		}
	}
}
