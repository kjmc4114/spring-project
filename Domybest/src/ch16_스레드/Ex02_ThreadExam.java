package ch16_스레드;

// 멀티스레드 : 작업단위가 2개 이상임.
//Thread를 상속
// Runnable을 구현
public class Ex02_ThreadExam extends Thread {

	public Ex02_ThreadExam(String name) {
		super(name);
	}

	@Override // 반드시 해야하는 부분.
	public void run() {
		for (int i = 0; i <= 5; i++) {
			// 현재 실행중인 스레드의 이름
			System.out.println(Thread.currentThread().getName());
//			try {
//				Thread.sleep(1000); // 1초간 cpu를 멈춤(필수 예외처리)
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//				
//			}
		}
	}

	public static void main(String[] args) {
		Ex02_ThreadExam e1 = new Ex02_ThreadExam("thread1");
		Ex02_ThreadExam e2 = new Ex02_ThreadExam("thread2");
		Ex02_ThreadExam e3 = new Ex02_ThreadExam("thread3");
		e1.start(); // run 메소드가 실행됨
		e2.start();
		e3.start();
	}
}
