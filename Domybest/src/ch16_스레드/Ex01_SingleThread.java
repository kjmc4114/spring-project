package ch16_스레드;

public class Ex01_SingleThread {
void print(){
	// SingleThread = 작업단위가 1개 
	// 순차적으로 처리됨
	//현재진행중인 스레드의 이름 
	System.out.println(Thread.currentThread().getName());
	for(int i=1; i<=5; i++){
		System.out.println(i);
	}
	
}
public static void main(String[] args) {
	Ex01_SingleThread t = new Ex01_SingleThread();
	t.print();
	t.print();
}
}

