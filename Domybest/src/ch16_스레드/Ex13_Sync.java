package ch16_스레드;

public class Ex13_Sync {
	public static void main(String[] args) {
		Ex12_ATm atm = new Ex12_ATm();
		Thread mom = new Thread(atm, "mom");
		Thread son = new Thread(atm, "son");
		mom.start();
		son.start();
	}
}
