package ch16_스레드;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

//애플릿 = 웹브라우저 상에서 자바를 실행
public class Ex06_Ball extends Applet implements Runnable
,ComponentListener{
	private int x, y;
	private int mx = 2, my = 3;
	private int width=200, height=200;
	

	@Override // 애플릿 초기화
	public void init() {
		Thread t = new Thread(this);
		t.start();
		//이벤스 소스 이벤트 리스너 (이벤트 핸들러)
		this.addComponentListener(this); //화면감지 요청
	}

	@Override // 그래픽처리 클래스
	public void paint(Graphics g) {
		g.setColor(Color.RED);
		g.fillOval(x, y, 30, 30); //fill Oval (x,y,가로,세로)
//		for(int i=0; i<=200; i++){
//		try {
//			Thread.sleep(30);
//		} catch (InterruptedException e) {
//		
//			e.printStackTrace();
//		}
//		}
	}

	@Override //Runnable 이 인터페이스이므로
	public void run() {
		while(true){ //무한반복
			 // 좌표값 변경
			if(x>(width-30)||x<0){mx=-mx;}
			x=x+mx;
			if(y>(height-30)||y<0){my=-my;}
			y=y+my;
			repaint(); //화면을 새로그려달라.
			try{
				Thread.sleep(60);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void componentResized(ComponentEvent e) {
		//화면의 가로 세로 사이즈를 저장
		width = getWidth();
		height = getHeight();
		System.out.println("가로:"+width+",세로:"+height);
		
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		
		
	}

	@Override
	public void componentShown(ComponentEvent e) {
		
		
	}

	@Override
	public void componentHidden(ComponentEvent e) {
	
		
	}




}
