package ch16_스레드;

public class Ex08_ThreadJoin extends Thread {

	
	public static void main(String[] args) {
		System.out.println(Thread.currentThread().getName()+"스레드가 시작되었습니다.");
		Ex08_ThreadJoin e = new Ex08_ThreadJoin();
		e.setName("쓰레드1"); //쓰레드의 이름
		e.start();
		try {
			e.join();
		} catch (InterruptedException e1) {
			
			e1.printStackTrace();
		}
		System.out.println(Thread.currentThread().getName()+"스레드가 종료되었습니다.");
}

	@Override
	public void run(){
		System.out.println(Thread.currentThread().getName()+"스레드가 시작되었습니다");
try{
	Thread.sleep(1000);
}catch (InterruptedException e){
	e.printStackTrace();
}
System.out.println(Thread.currentThread().getName()+"스레드가 종료되었습니다.");
}

}
