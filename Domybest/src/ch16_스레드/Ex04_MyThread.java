package ch16_스레드;

public class Ex04_MyThread extends Thread {
	public Ex04_MyThread(String name) {
		super(name); // 스레드의 이름
	}

	@Override
	public void run() {
		for (int i = 1; i <= 5; i++) {
			System.out.println(Thread.currentThread().getName());
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();

			}
		}

	}

}
