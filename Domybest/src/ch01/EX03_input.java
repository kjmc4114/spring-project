package ch01;
//java.util 패키지에 들어있는 Scanner란 클래스를 참조
//자동 import : Ctrl+shift+o

import java.util.Scanner;

public class EX03_input {
	public static void main(String[] args){
		//Scanner = 키보드의 입력을 지원하는 클래스
		Scanner name=new Scanner(System.in);
		System.out.println("이름을 입력하세요:");
		String  a= name.next(); //스트링을 입력받음
		System.out.println("당신의 이름은"+a+"입니다");
		System.out.println("나이를 입력하세요:");
		int age=name.nextInt();
		System.out.println("당신의 나이는"+age+"입니다.");
	}
}
