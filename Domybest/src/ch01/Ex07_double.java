package ch01;

public class Ex07_double {
	public static void main(String[] args) {
		double num1=Math.PI;
		System.out.println(num1);
		float num2=3.14F;
		System.out.println(num2);
		System.out.println(Float.SIZE);
		System.out.println(Float.MAX_VALUE);
		
		double a=10.5;
		a=10.;
		System.out.println(Double.MAX_VALUE);
		System.out.println(Double.MIN_VALUE);
		System.out.println(Double.SIZE);
	}

}
