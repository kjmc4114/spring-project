package ch01;

public class EX04_char {
	public static void main(String[] args) {
		//문자형 변수 ch에 'A' 저장 (숫자 65가 저장됨.)
		char ch='\0'; // \n 엔터키 
		System.out.println(ch);
		//문자열 참조변수 str이 "AB"문자열을 가리킴
		System.out.println((int)ch);
		//자료형 형변환, cast 연산자
		
/*		for(int i=0; i<=255; i++)
		System.out.println(i+"==>"+(char)i);
		String str="AB";
		System.out.println(str);
	*/ 
		for(int a='ㄱ'; a<='ㄴ'; a++){
			System.out.println((char)a+"==>"+a);
		/*for(int b='家'; b<='後'; b++){
				System.out.println((char)b+"==>"+b);*/
		}
	
	}

}
