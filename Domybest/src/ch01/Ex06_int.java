package ch01;

public class Ex06_int {
	public static void main(String[] args) {
		int num1=0;
		System.out.println(num1);
		System.out.println(Integer.MAX_VALUE);
		//int의 최대값
		System.out.println(Integer.MIN_VALUE);
		//int의 최소값
		System.out.println(Integer.SIZE);
		//int의 사이즈 (비트수)
		
	    long num2=92233333333333333l;
	    System.out.println(num2);
	    System.out.println(Long.MAX_VALUE);
	    System.out.println(Long.MIN_VALUE);
	    System.out.println(Long.SIZE);
	    

	    System.out.println(Byte.MAX_VALUE);
	    System.out.println(Byte.MIN_VALUE);
	    System.out.println(Byte.SIZE);
	}

}
