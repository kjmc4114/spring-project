package ch10_다형성;


class Test{
	private int num;
	public void print(){
		System.out.println(num);
	}
}
	
public class Ex03_FinalMethod extends Test{
	int num=100;
@Override // annotation //final이 붙은 메소드는 오버라이딩이 안됨.
	public void print(){
		System.out.println(num);
	}


public static void main(String[] args) {
	Ex03_FinalMethod f= new Ex03_FinalMethod();
	f.print();
}
}
