package ch10_다형성;
// static 프로그램이 시작되면 클래스 영역에 자동으로 로딩
// nonstatic : 인스턴스가 생성될때 만들어짐
public class Ex04_StaticVar {
	static int a=10;
	int b = 20;
public static void main(String[] args) {
	System.out.println(a);
//	System.out.println(b);

	Ex04_StaticVar s1 = new Ex04_StaticVar();
	System.out.println(s1.b);
}
}
