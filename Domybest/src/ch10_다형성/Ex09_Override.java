package ch10_다형성;

class Test1{
	public void print(){
		System.out.println("test1 클라스의 메소드");
	}
}

class Test2 extends Test1{
	@Override
	public void print(){
		System.out.println("test2 클라스의 메소드"); 
	}
	

} 
public class Ex09_Override {

public static void main(String[] args) {
	Test2 t= new Test2();
	t.print();
}
}
