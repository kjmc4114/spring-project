package ch10_다형성;
class Triangle extends Figure{
	
	void draw(){System.out.println("삼각형");}
}

class Rectangle extends Figure{
	
	void draw(){System.out.println("사각형");}
	
}

class Circle extends Figure{
	
	void draw(){System.out.println("원");}
	
}

public class Figure {
	void draw(){System.out.println("도형을 그린다요");}

	public static void main(String[] args) {
	
	Figure f=new Figure();
	f.draw();
	f=new Triangle();
	f.draw();
	f=new Rectangle();
	f.draw();
	f=new Circle();
	f.draw();
	}
}
