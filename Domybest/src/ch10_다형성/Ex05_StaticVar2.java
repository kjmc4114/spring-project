package ch10_다형성;

public class Ex05_StaticVar2 {
static int total; //클래스 영역에 생성
private String model; //힙영역에 생성
public Ex05_StaticVar2(String model){
	this.model = model;
	total++;
}
public static void main(String[] args) {
	Ex05_StaticVar2 car1=new Ex05_StaticVar2("소나타");
	Ex05_StaticVar2 car2=new Ex05_StaticVar2("모닝");
	Ex05_StaticVar2 car3=new Ex05_StaticVar2("아반데");
	//System.out.println(total); 아래와 같은표현 (public일때 다른 메인 메소드도 불러올 수 있을까?)
	System.out.println(Ex05_StaticVar2.total);
	System.out.println(car1.model);
	System.out.println(car2.model);
	System.out.println(car3.model);
}
}
