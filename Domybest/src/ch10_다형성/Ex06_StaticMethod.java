package ch10_다형성;

public class Ex06_StaticMethod {
static int a=10;
int b = 20;
public static void printA(){
	System.out.println(a);
	Ex06_StaticMethod s1 = new Ex06_StaticMethod();
	System.out.println(s1.b);
}

public void printB(){
	System.out.println(a);
	System.out.println(b);
}

public static void main(String[] args) {
	Ex06_StaticMethod s1=new Ex06_StaticMethod();
	Ex06_StaticMethod.printA();
	s1.printB();
}
}
