package ch10_다형성;


//다형성 : 부모클래스의 참조변수를 자식클래스의 메소드호출 

//부모자료형 이 좌변, 우변에 자식자료형 자식자료형을 호출한다 (오버라이딩 됐을때)
	class AA{
		int a = 10;
		public void print(){
			System.out.println(a);
		}
	}
	class BB extends AA{
		int a= 20;
		public void print(){
			System.out.println(a);
			}
		}
public class Ex10_Poly {	
public static void main(String[] args) {
	BB b = new BB(); //참조변수 b 
	b.print();
	AA aa= new AA(); //참조 변수 aa
	aa.print();
	AA cc= new BB(); //참조 변수 cc
	cc.print();
}	
}

