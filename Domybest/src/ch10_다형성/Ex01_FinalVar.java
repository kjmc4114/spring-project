package ch10_다형성;

public class Ex01_FinalVar {
//static final int MAX_NUM; // final은 초깃값이 필요합니다.
	
public static void main(String[] args) {
	final int MAX_NUM=100; //final 변수는 대문자로 적습니다.
	System.out.println(MAX_NUM);
//	MAX_NUM=200; // final 변수는 변경할 수 없습니다. 
	
}
}
