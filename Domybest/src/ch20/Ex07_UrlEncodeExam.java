package ch20;

import java.net.URLDecoder;
import java.net.URLEncoder;

public class Ex07_UrlEncodeExam {
public static void main(String[] args) {
	try{
	String str="세상에는 좋은 사람이 어디있겠어.";
	System.out.println(URLEncoder.encode(str,"utf-8"));
	System.out.println(URLDecoder.decode("%EC%84%B8%EC%83%81%EC%97%90%EB%8A%94+%EC%A2%8B%EC%9D%80+%EC%82%AC%EB%9E%8C%EC%9D%B4+%EC%96%B4%EB%94%94%EC%9E%88%EA%B2%A0%EC%96%B4.","utf-8"));
	} catch (Exception e){
		e.printStackTrace();
	}
}
}
