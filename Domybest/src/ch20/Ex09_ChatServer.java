package ch20;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Ex09_ChatServer {
											//네트워크 관련 작업은 예외처리가 필수임.
	public static void main(String[] args) throws Exception{
		//serversocket : 서비스용 소켓
		
		ServerSocket ss = new ServerSocket (9100); // 포트번호 = 0~65535
		System.out.println("서버가 시작되었습니다.");
		while (true){
			Socket socket = ss.accept(); //클라이언트가 접속할때까지 대기 상태
			PrintWriter out = new PrintWriter( //접속하면 소캣이 연결 (논리적인 회선연결)
					socket.getOutputStream(),true);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 MM월 dd일 HH시 mm분 ss초");
			String str = sdf.format(new Date());
			out.println(str); //클라이언트에게 데이터 전송
			socket.close(); //소켓종료 (연결종료)
		}
	}

}
