package ch20;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Ex01_InetAddressExam {
	public static void main(String[] args) {
		try {
			InetAddress address = InetAddress.getByName("google.com");
			System.out.println(address);
			InetAddress[] address2 = InetAddress.getAllByName("naver.com");
			for(InetAddress i : address2){System.out.println(i);}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
