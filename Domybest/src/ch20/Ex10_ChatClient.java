package ch20;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;

public class Ex10_ChatClient {
	public static void main(String[] args) throws Exception {
		Socket s = new Socket("192.168.0.149", 9100); // 로컬은 로컬.
		BufferedReader input = new BufferedReader ( new InputStreamReader(s.getInputStream()));
		String res = input.readLine();
		System.out.println(res);
		s.close(); 	// 소켓 닫기
		System.exit(0);; //프로그램 종료
	}
}
	
