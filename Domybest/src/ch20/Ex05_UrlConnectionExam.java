package ch20;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Ex05_UrlConnectionExam {
	public static void main(String[] args) {
		URL url = null;

		try {
			url = new URL("http://google.com");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			if (conn != null) {
				conn.setConnectTimeout(3000);
				StringBuilder sb = new StringBuilder();
				// Http 상태코드 http state code 200 == HttpURLConnection.HTTP_OK
				if (conn.getResponseCode() == 200) {
					// 버퍼드 리더 (리더 객채(스트림또는파일객체)))
					BufferedReader br = new BufferedReader              //포맷형식.
							(new InputStreamReader(conn.getInputStream(), "ms949"));
					while (true) {
						String line = br.readLine();
						if (line == null)
							break;
						sb.append(line + "\r\n");
					}
					br.close();
				}
				conn.disconnect();
				System.out.println(sb);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
