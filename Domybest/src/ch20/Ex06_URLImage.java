package ch20;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public class Ex06_URLImage {
	public static void main(String[] args) throws Exception {
		String website = "http://data.ygosu.com/upload_files/board_stars/90840/596d994f52298.jpg";
		System.out.println("다운로드를 시작합니다.");
		URL url = new URL(website);
		byte[] buffer = new byte[2048];
		//파일로 저장 <=프로그램 <=저장
		try (InputStream in = url.openStream(); 
				OutputStream out = new FileOutputStream("d:\\test2.jpg")) {
			// 읽은 바이트수 = 스트림.read(버퍼)
			//출력스트림.write(버퍼,시작인덱스,길이)
			int length = 0;
			while ((length = in.read(buffer)) != -1) {
				System.out.println(length+"바이트읽음");
				out.write(buffer, 0, length);
			}
			System.out.println("다운로드가 완료되었습니다.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}