package ch14_CollectionGeneric;

public class Ex01_Type1 {
Object value;

public Object getValue() {
	return value;
}

public void setValue(Object value) {
	this.value = value;
}
public static void main(String[] args) {
	Ex01_Type1 t = new Ex01_Type1(); 
	t.setValue(100);
	t.setValue(100.5);
	t.setValue("hello");
	System.out.println(t.getValue());
}
}
