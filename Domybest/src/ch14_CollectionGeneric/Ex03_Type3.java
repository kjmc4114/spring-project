package ch14_CollectionGeneric;

public class Ex03_Type3<T> {
//<>안은 아무 의미 없음 런타임. 즉 인스턴스 생성때 자료형을 정해줌을 의미

T t;
public void setT(T t){
	this.t=t;
}
public T getT(){
	return t;

}

public static void main(String[] args) {
	Ex03_Type3<String> t = new Ex03_Type3<String>();
	t.setT("100");
	System.out.println(t.getT());
	Ex03_Type3<Integer> t2 = new Ex03_Type3<Integer>();
	t2.setT(100);
	System.out.println(t2.getT());
	Ex03_Type3 t3 = new Ex03_Type3();
	t3.setT(100.5); // Object 처럼 작동함.
	System.out.println(t3.getT());
}
}
