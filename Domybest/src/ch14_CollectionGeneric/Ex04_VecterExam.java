package ch14_CollectionGeneric;

import java.util.Vector;
public class Ex04_VecterExam {
public static void main(String[] args) {
	Vector<Object> v = new Vector<Object>();
	v.add("first");
	v.add(2);
	v.add(3.5);
	v.add(true);
	v.add(100);
	System.out.println("초기데이터");
	for(int i=0; i<v.size(); i++){System.out.print(v.get(i)+" ");}
	System.out.println();
	v.remove(0);
	for(int i=0; i<v.size(); i++){System.out.print(v.get(i)+" ");}
	System.out.println();
	v.add(0, "hello");
	for(int i=0; i<v.size(); i++){System.out.print(v.get(i)+" ");}
	
	
}

}
