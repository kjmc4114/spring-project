package ch14_CollectionGeneric;
import java.util.ArrayList;
import java.util.List;

import ch14_CollectionGeneric.Ex10_Book;

public class Ex11_ListBook {
	public static void main(String[] args) {
		List<Ex10_Book> list = new ArrayList<>();
		Ex10_Book b1 = new Ex10_Book();
		b1.input();
		Ex10_Book b2 = new Ex10_Book();
		b2.input();
		Ex10_Book b3 = new Ex10_Book();
		b3.input();
		list.add(b1);
		list.add(b2);
		list.add(b3);
		System.out.println("도서명\t\t출판사\t단가\t수량\t금액");
		for (int i = 0; i < list.size(); i++) {
			Ex10_Book b = list.get(i);
			System.out.println(b.getBookName()+"\t"+b.getPress()+"\t"+b.getPrice()+"\t"+b.getAmount()+"\t"+b.getMoney());
		}

	}

}
