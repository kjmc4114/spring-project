package ch14_CollectionGeneric;

import java.util.ArrayList;
import java.util.List; //awt가 아니고 util 임.

public class Ex05_ArrayList {
public static void main(String[] args) {
	List<Object> list=new ArrayList<Object>(); // 좌변에 ArrayList를 써도 됨.
	list.add("하나");
	list.add(2);
	list.add(5.5);
	list.add(false);
	list.add(3,4); //(인덱스,추가할 값)
	list.add("IU");
	list.add("이쁨");
	list.remove(5);
	
	for(int i=0; i<list.size(); i++){
		System.out.print(list.get(i)+" ");
	}
	
}
}
