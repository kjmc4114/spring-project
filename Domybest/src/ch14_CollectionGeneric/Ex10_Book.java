package ch14_CollectionGeneric;

import java.util.Scanner;

public class Ex10_Book {
	private String BookName;
	private String press;
	private int price;
	private int amount;

	public Ex10_Book() {
		
	}
	
	public Ex10_Book(String bookName, String press, int price, int money) {
		BookName = bookName;
		this.press = press;
		this.price = price;
		this.money = money;
	}

	public void input() {
		Scanner scan = new Scanner(System.in);
		System.out.println("도서명:");
		BookName = scan.nextLine();

		System.out.println("출판사:");
		press = scan.nextLine();

		System.out.println("단가:");
		price = scan.nextInt();

		System.out.println("수량:");
		amount = scan.nextInt();

		money = price * amount;
	}

	public String getBookName() {
		return BookName;
	}

	public void setBookName(String bookName) {
		BookName = bookName;
	}

	public String getPress() {
		return press;
	}

	public void setPress(String press) {
		this.press = press;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	private int money;


}
