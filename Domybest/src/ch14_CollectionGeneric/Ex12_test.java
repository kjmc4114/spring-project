package ch14_CollectionGeneric;

import java.util.ArrayList;
import java.util.List;

public class Ex12_test {
public static void main(String[] args) {
	List<String> nations = new ArrayList<>();
	nations.add("한국");
	nations.add("중국");
	nations.add("미국");
	
	List<String> city = new ArrayList<>();
	nations.add("서울");
	nations.add("베이징");
	nations.add("워싱턴");
	
	for(int i = 0; i < nations.size(); i++){
		if(nations.get(i).equals("한국")){
		 System.out.println(nations.get(i)+"의수도는"+city.get(i));
		}
	}
	
}
}
