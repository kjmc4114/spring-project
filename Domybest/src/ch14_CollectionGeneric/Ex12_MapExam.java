package ch14_CollectionGeneric;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

//list = 순서대로 저장 Map : 순서가 없음
//Map <key 자료형, value  자료형>
public class Ex12_MapExam {

	public static void main(String[] args) {
		Map<String, String> map = new HashMap<>();
		System.out.println(map.get("한국"));
		map.put("가나", "초콜렛");
		map.put("한국", "서울");
		map.put("일본", "동경");
		map.put("중국", "북경");
		map.put("미국", "워싱턴");
		map.put("태국", "방콕");
		map.put("영국", "런던");
		map.put("나비", "고양이");
		map.put("와우", "하스스톤");

		
	System.out.println(map);
	String nation="태국";
	System.out.println(nation+"의수도는"+map.get(nation));

	
	
	
	Iterator<String> iterator = map.keySet().iterator();
	while(iterator.hasNext()){ //다음요소가 있을때
		String key=(String)iterator.next();
		System.out.print("Key="+key);
		System.out.println(",value="+map.get(key));
	}
	
	}


}
