package ch14_CollectionGeneric;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Ex07_ListMember {
public static void main(String[] args) {
	List<Ex06_Member> list;
	
	list =  new ArrayList<>();
	Optional<String> exist = Optional.of("123");
	Optional<String> empty = Optional.empty();
	String value = null;
	Optional <String> optional = Optional.ofNullable(value);
	Ex06_Member m1 = new Ex06_Member();
	m1.setName("김철수");
	m1.setUserid("kim");
	m1.setPasswd("1234");
	m1.setTel("02-999-8888");
	m1.setEmail("kim@naver.com");
	// List 에 member 인스턴스 추가
	list.add(m1);
	
	Ex06_Member m2 = new Ex06_Member("hong", "1234",
			"jun", "1234-1523", "hong@naver.com");
	list.add(m2);
	list.add(new Ex06_Member("choi", "12345", "sun", "12394-12334", "sun@daum.net"));
	//참조변수(m2)를 생략 
	System.out.println("이름\t아이디\t비번\t전화");
	for(int i=0; i<list.size(); i++){
		Ex06_Member m = list.get(i);
		System.out.println(m.getName()+"\t"+m.getUserid()+"\t"+m.getPasswd()+"\t"+m.getTel());
	}
}
}
