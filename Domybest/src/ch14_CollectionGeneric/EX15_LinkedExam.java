package ch14_CollectionGeneric;
import java.util.LinkedList;
// Queue(큐) 구조
// First In First Out (FIFO) 먼저 입력된 자료가 먼저 출력됨
public class EX15_LinkedExam {
public static void main(String[] args) {
String[] fruits = { "사과", "배", "포도", "메론", "망고" };
LinkedList<String> list = new LinkedList<>();
for (String str : fruits) {
list.offer(str); // 큐 구조에 자료 입력
}
String str = "";
while ((str = (String) list.poll()) != null) 
//대입을 먼저시키고 비교를 하기
{ // poll() 자료 꺼내기
System.out.println(str + "가 삭제되었습니다.");
}
}
}