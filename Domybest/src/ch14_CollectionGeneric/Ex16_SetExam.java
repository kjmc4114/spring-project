package ch14_CollectionGeneric;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Ex16_SetExam {
public static void main(String[] args) {
	//Set<Object> hs = new HashSet<Object>(); 정렬이 필요없는 Set
	Set<Object> hs = new TreeSet<Object>(); //sort가 필요한 Set 
	hs.add("korea");
	hs.add("japan");
	hs.add("america");
	hs.add("britain");
	hs.add("korea");
	hs.add("korea");
	System.out.println(hs);
	for(Object str : hs){
		System.out.println(str);
	}
System.out.println("===============");	
	Iterator<Object> it = hs.iterator();
	while(it.hasNext()){
		System.out.println(it.next());
	}
}
}
