package ch14_CollectionGeneric;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ex13_MapList {

	
public static void print(Map map)	{
	
	List<Ex08_Student> list2 = 
			(ArrayList<Ex08_Student>)map.get("list");
	for(Ex08_Student s : list2 ){
		System.out.println(s.getNum()+"\t"+s.getName()+"\t"+s.getMajor()+"\t"+s.getYear()+"\t"+s.getProfessor());
	}
}
	
public static void main(String[] args) {
	Map<String,Object> map = new HashMap<String, Object>();
	List<Ex08_Student> list = new ArrayList<>();
	list.add(new Ex08_Student("082029","아이유","오우",1,"이열"));
	map.put("list", list);
	print(map);
	
}
}
