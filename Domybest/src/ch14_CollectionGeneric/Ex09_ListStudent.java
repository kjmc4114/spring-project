package ch14_CollectionGeneric;

import java.util.ArrayList;
import java.util.List;

public class Ex09_ListStudent {
	
	public static void main(String[] args) {
		List<Ex08_Student> list = new ArrayList();
		list.add(new Ex08_Student("3208209", "김정명", "웹", 1, "교수님"));
		list.add(new Ex08_Student("3208209", "김정명", "웹", 1, "교수님"));
		list.add(new Ex08_Student("3208209", "김정명", "웹", 1, "교수님"));

		
		System.out.println("학번\t이름\t학과\t학년\t지도교수");
//		for(int i=0; i<list.size(); i++){
//			Ex08_Student s=list.get(i);
//			System.out.println(s.getNum()+"\t"+s.getName()+"\t"+s.getMajor()+"\t"+s.getYear()+"\t"+s.getProfessor());
//		
		for(Ex08_Student s : list ){
			System.out.println(s.getNum()+"\t"+s.getName()+"\t"+s.getMajor()+"\t"+s.getYear()+"\t"+s.getProfessor());
		}
		
		
		}
	}


