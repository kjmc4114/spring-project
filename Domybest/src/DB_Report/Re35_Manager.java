package DB_Report;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import ch21.oracle.ScoreList;
import ch21.oracle.ScoreSave;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Re35_Manager extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private Re35_DAO dao;
	private Vector cols;
	private DefaultTableModel model;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Re35_Manager frame = new Re35_Manager();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void refreshTable() {
		model = new DefaultTableModel(dao.listAccount(), cols);
		table.setModel(model);
	}

	public Re35_Manager() {
		dao = new Re35_DAO();
		cols = new Vector();
		cols.add("은행이름");
		cols.add("은행계좌");
		cols.add("이름");
		cols.add("잔액");
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);

		JButton btnNewButton = new JButton("추가");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Re35_insert insert = new Re35_insert (Re35_Manager.this); //그냥 this 라고 할시 ActionListener를 가리킴.
				insert.setVisible(true);
				insert.setLocation(200,200); //부모창의 왼쪽위 모서리 기준으로 좌푯값이 설정됨.
			}
		});
		btnNewButton.setFont(new Font("Gulim", Font.PLAIN, 24));
		panel.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("편집");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int idx = table.getSelectedRow();
				if (idx == -1){
					JOptionPane.showMessageDialog(Re35_Manager.this, "편집할 레코드를 선택하세요");
					return;
				}
				String bankname = String.valueOf(table.getValueAt(idx, 0));
				String accountno = String.valueOf(table.getValueAt(idx, 1)); 
				String name = String.valueOf(table.getValueAt(idx, 2)); 
				int balance = Integer.valueOf(table.getValueAt(idx, 3)+"");
				Re35_DTO dto = new Re35_DTO(bankname,accountno,name,balance);
				Re35_Edit edit = new Re35_Edit(Re35_Manager.this,dto);
				edit.setVisible(true);
				edit.setLocation(200,200);
			}
		});
		btnNewButton_1.setFont(new Font("Gulim", Font.PLAIN, 24));
		panel.add(btnNewButton_1);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		table.setFont(new Font("Gulim", Font.PLAIN, 12));
		scrollPane.setViewportView(table);
		DefaultTableModel model = new DefaultTableModel(dao.listAccount(), cols);
		table.setModel(model);
	}

}
