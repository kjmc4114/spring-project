package DB_Report;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Re32 {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost/java";
		String id = "java";
		String password = "java1234";
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		//try with문 try 후에 with 안에 있는 애들이 자동으로 소멸됨. 자바 1.7 버전의 경우에 사용.
		try/*(Connection=DriverManager.getConnection(url, id, password);)*/ {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("JDBC를 로딩했습니다.");
			conn = DriverManager.getConnection(url, id, password);
			System.out.println("mySQL 접속 성공");
			String sql = "select * from confidentiality";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int person_id = rs.getInt("person_id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				String num = rs.getString("num");
				String email = rs.getString("email");
				System.out.println(person_id+"\t"+name+"\t"+address+"\t"+num+"\t"+email);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{ //finally 를 쓰지않더라도 문제는 생기지않음. 하지만 사용자들이 많아 질경우에 리소스를 너무 많이 먹음.
			try {
				if(rs!=null)rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(stmt!=null){
					stmt.close();}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if(conn!=null){conn.close();}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}
}
