package DB_Report;

import java.sql.*;
import java.io.*;
import java.util.*;

public class Re33_DAO {
	public Connection dbConn() {
		Connection conn = null;
		try {
			FileInputStream fis = new FileInputStream("d:\\db.prop");
			Properties prop = new Properties();
			prop.load(fis);
			String url = prop.getProperty("url");
			String id = prop.getProperty("id");
			String password = prop.getProperty("password");
			conn = DriverManager.getConnection(url, id, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public List<Re33_DTO> CallRecordTable() { // 테이블을 불러오는 메소드
		List<Re33_DTO> items = new ArrayList<>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = dbConn();
			System.out.println("DB접속성공");
			String sql = "select*from bank";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				int user_num = rs.getInt("user_num");
				String bankname = rs.getString("bankname");
				String account = rs.getString("account");
				String name = rs.getString("name");
				int balance = rs.getInt("balance");
				Re33_DTO dto = new Re33_DTO(user_num, bankname, account, name, balance);
				items.add(dto);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items;

	}
	public int DeletebyAccount(String account){
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn =dbConn();
			String sql = "delete from bank where account =? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, account);
			//영향을 받은 행 을 나타나는 것.
			result =pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt!=null){pstmt.close();}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
			if(conn != null){conn.close();}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
		
	}

	public int DeletebyUsernum(int user_num){
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn =dbConn();
			String sql = "delete from bank where user_num =? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, user_num);
			result =pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstmt!=null){pstmt.close();}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
			if(conn != null){conn.close();}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public void InsertRecordBank(Re33_DTO dto) { // 입력메소드
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = dbConn();
			String sql = "insert into bank" 
			+ "(bankname, account, name, balance) " 
			+ "values (?,?,?,?)"; 
			// 은행이름,계좌,고객이름,잔액
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, dto.getBankname());
			pstmt.setString(2, dto.getAccount());
			pstmt.setString(3, dto.getName());
			pstmt.setInt(4, dto.getBalance());
			pstmt.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}

		}

	}
	
	
}
