package DB_Report;

public class Re33_DTO {
private int user_num;
private String bankname;
private String account;
private String name;
private int balance;
public Re33_DTO() {
	// TODO Auto-generated constructor stub
}


public Re33_DTO(int user_num, String bankname, String account, String name, int balance) {
	this.user_num = user_num;
	this.bankname = bankname;
	this.account = account;
	this.name = name;
	this.balance = balance;
}


public Re33_DTO( String bankname, String account, String name, int balance) {
//	this.user_num = user_num;
	this.bankname = bankname;
	this.account = account;
	this.name = name;
	this.balance = balance;
}
public int getUser_num() {
	return user_num;
}
public void setUser_num(int user_num) {
	this.user_num = user_num;
}
public String getBankname() {
	return bankname;
}
public void setBankname(String bankname) {
	this.bankname = bankname;
}
public String getAccount() {
	return account;
}
public void setAccount(String account) {
	this.account = account;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getBalance() {
	return balance;
}
public void setBalance(int balance) {
	this.balance = balance;
}
@Override
public String toString() {
	return "Re33_DTO [user_num=" + user_num + ", bankname=" + bankname + ", account=" + account + ", name=" + name
			+ ", balance=" + balance + "]";
}

}
