package DB_Report.RE32;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import ch21.oracle.DB;

public class StudentDAO {
	public Vector listStudent(int deptno1) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.HrConn();
			String sql = "select s.STUDNO ,s.name , s.ID, s.birthday, s.TEL, d.dname , p.name pname from student s, department d , professor p where s.deptno1 = d.deptno and s.profno = p.profno(+) and s.DEPTNO1 = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, deptno1);
			rs = pstmt.executeQuery();
			while (rs.next()){
				Vector row = new Vector();
				row.add(rs.getInt("STUDNO"));
				row.add(rs.getString("NAME"));
				row.add(rs.getString("ID"));
				row.add(rs.getString("BIRTHDAY"));
				row.add(rs.getString("TEL"));
				row.add(rs.getString("DNAME"));
				row.add(rs.getString("PNAME"));
				items.add(row);
			}
					
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs!=null)rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(pstmt!=null)pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(conn!=null)conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;

	}

}
