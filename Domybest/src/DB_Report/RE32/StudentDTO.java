package DB_Report.RE32;

import java.sql.Date;

public class StudentDTO {
private int studno;
private String name;
private String id;
private String birthday;
private String tel;
private int deptno1;
private String dname;
private int profno;
private String pfname;
public StudentDTO() {
}
public int getStudno() {
	return studno;
}
public void setStudno(int studno) {
	this.studno = studno;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getBirthday() {
	return birthday;
}
public void setBirthday(String birthday) {
	this.birthday = birthday;
}
public String getTel() {
	return tel;
}
public void setTel(String tel) {
	this.tel = tel;
}
public int getDeptno1() {
	return deptno1;
}
public void setDeptno1(int deptno1) {
	this.deptno1 = deptno1;
}
public String getDname() {
	return dname;
}
public void setDname(String dname) {
	this.dname = dname;
}
public int getProfno() {
	return profno;
}
public void setProfno(int profno) {
	this.profno = profno;
}
public String getPfname() {
	return pfname;
}
public void setPfname(String pfname) {
	this.pfname = pfname;
}



public StudentDTO(int studno, String name, String id, String birthday, String tel, int deptno1, String dname, int profno,
		String pfname) {
	this.studno = studno;
	this.name = name;
	this.id = id;
	this.birthday = birthday;
	this.tel = tel;
	this.deptno1 = deptno1;
	this.dname = dname;
	this.profno = profno;
	this.pfname = pfname;
}
@Override
public String toString() {
	return "StudentDTO [studno=" + studno + ", name=" + name + ", id=" + id + ", birthday=" + birthday + ", tel=" + tel
			+ ", deptno1=" + deptno1 + ", dname=" + dname + ", profno=" + profno + ", pfname=" + pfname + "]";
}


}
