package DB_Report.RE32;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import ch21.oracle.DB;


public class DepartmentDAO {
	
	public ArrayList<DepartmentDTO> listDept() {
		ArrayList<DepartmentDTO> items = new ArrayList<>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.HrConn();
			StringBuilder sb = new StringBuilder();
			sb.append(" select DEPTNO, DNAME from department"); 
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()){
				int deptno = rs.getInt("DEPTNO");
				String dname = rs.getString("DNAME");
				DepartmentDTO dto = new DepartmentDTO();
				dto.setDeptno(deptno);
				dto.setDname(dname);
				items.add(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs!=null)rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(pstmt!=null)pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(conn!=null)conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	return items;
	}
}
