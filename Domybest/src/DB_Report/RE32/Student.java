package DB_Report.RE32;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class Student extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private StudentDTO dto;
	private Vector data, col;
	private StudentDAO dao;
	private DepartmentDAO deptdao;
	private JComboBox cobDept;
	private ArrayList<DepartmentDTO> listDept;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Student frame = new Student();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	
	
	public Student() {
		dao = new StudentDAO();
		dto = new StudentDTO();
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		col = new Vector();
		col.add("학번");
		col.add("이름");
		col.add("아이디");
		col.add("생일");
		col.add("전화번호");
		col.add("학과");
		col.add("담당교수");
		
		
		
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		cobDept = new JComboBox();
		cobDept.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				
				String dname = cobDept.getSelectedItem().toString(); // getsel.. 메소드의 반환타입이 오브젝트이기 떄문에 String으로 바뀜				
				//emplist = empDao.listEmp(dname);
				
				int idx = cobDept.getSelectedIndex(); //콤보박스에서 선택한 아이템의 인덱스값.
				int deptno1 = listDept.get(idx).getDeptno(); //해당되는 리스트에서, 부서코드만 가져옴.
				System.out.println("deptno1 :"+deptno1);
				System.out.println(dao);
				DefaultTableModel model = 
					new DefaultTableModel(dao.listStudent(listDept.get(idx).getDeptno()), col);
				table.setModel(model);
			}
		});
		deptdao = new DepartmentDAO();
		listDept = deptdao.listDept();
		for(DepartmentDTO dto : listDept){
			cobDept.addItem(dto.getDname());
		}
		contentPane.add(cobDept, BorderLayout.NORTH);
	}

}
