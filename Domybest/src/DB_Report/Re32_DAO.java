package DB_Report;

import java.io.FileInputStream;
import java.sql.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class Re32_DAO {
	public Connection dbConn(){
		Connection conn = null;
		try {
FileInputStream fis = new FileInputStream ("d:\\db.prop");
Properties prop = new Properties();
prop.load(fis);
String url = prop.getProperty("url");
String id = prop.getProperty("id");
String password = prop.getProperty("password");
conn = DriverManager.getConnection(url, id, password);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	
}
	public List<Re32_DTO> listAdress(){
		List<Re32_DTO> items = new ArrayList<>();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn= dbConn();
			System.out.println("DB에 접속되었습니다.");
			String sql = "select * from confidentiality ";
			pstmt = conn.prepareStatement(sql); //테이블의 요소를 가져옴.
			rs = pstmt.executeQuery(); // 각각의 레코드를 저장 (쿼리를 생성)
			while (rs.next()){
				int person_id = rs.getInt("person_id");
				String name = rs.getString("name");
				String address = rs.getString("address");
				String num = rs.getString("num");
				String email = rs.getString("email");
				Re32_DTO dto = new Re32_DTO(person_id,name,address,num,email);
				items.add(dto);
				
			}
		
		
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(rs!=null)rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(pstmt!=null)pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if(conn!=null)conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;

	
	}
	
}

	
	
	

