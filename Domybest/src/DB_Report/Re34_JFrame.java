package DB_Report;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Re34_JFrame extends JFrame {
	private Re34_DAO dao;
	private JPanel contentPane;
	private JTextField tffind;
	private JTable table;
	private Vector items, col;
	private DefaultTableModel model;
	private Re34_DTO dto;
	private JLabel lbl;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JLabel label;
	private JTextField tfid;
	private JTextField tfName;
	private JTextField tfWeight;
	private JTextField tfHeight;
	private JButton btnInsert;
	private JButton btnDelet;
	private JButton btnChange;
	private JScrollPane scrollPane;
	private JTextField tfDate;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Re34_JFrame frame = new Re34_JFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void delete() {
		String user_id = tfid.getText();
		int result = 0;
		int response = JOptionPane.showConfirmDialog(Re34_JFrame.this, "삭제하시겠습니까?");
		if (response == JOptionPane.YES_OPTION) {
			result = dao.deleteMember(user_id);
		}
		if (result == 1) {
			JOptionPane.showMessageDialog(Re34_JFrame.this, "삭제했습니다.");
			show(dao.listBmi());
			clear();
		}
	}

	public void change() {
		Date signdate = Date.valueOf(tfDate.getText());
		String user_id = tfid.getText();
		String name = tfName.getText();
		double weight = Double.parseDouble(tfWeight.getText());
		double height = Double.parseDouble(tfHeight.getText());
		dto = new Re34_DTO(signdate, user_id, name, height, weight);
		int result = dao.updateMember(dto);
		if (result == 1) {
			JOptionPane.showMessageDialog(Re34_JFrame.this, "수정성공!");
			show(dao.listBmi());
			clear();

		}
	}

	public void sign() { // 추가 메소드
		Date signdate = Date.valueOf(tfDate.getText());
		String user_id = tfid.getText();
		String name = tfName.getText();
		double weight = Double.parseDouble(tfWeight.getText());
		double height = Double.parseDouble(tfHeight.getText());
		dto = new Re34_DTO(signdate, user_id, name, height, weight);

	}

	public void clear() { // 클리어 메소드
		tfDate.setText("");
		tfid.setText("");
		tfName.setText("");
		tfHeight.setText("");
		tfWeight.setText("");
		tfid.setEditable(true);

	}

	public void show(Vector items) { // 테이블에 추가 (백터이름 기재)

		model = new DefaultTableModel(items, col) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table = new JTable(model);
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int index = table.getSelectedRow();
				tfid.setEditable(false);
				tfDate.setText(Date.valueOf(table.getValueAt(index, 0) + "") + "");
				tfid.setText((String) table.getValueAt(index, 2));
				tfName.setText((String) table.getValueAt(index, 3));
				tfWeight.setText(table.getValueAt(index, 5) + "");
				tfHeight.setText(table.getValueAt(index, 4) + "");
			}
		});
		table.setFont(new Font("굴림", Font.PLAIN, 12));
		scrollPane.setViewportView(table);
	}

	public Re34_JFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 707, 535);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 313, 681, 151);
		contentPane.add(scrollPane);

		dao = new Re34_DAO();
		col = new Vector();
		col.add("등록날짜");
		col.add("회원번호");
		col.add("유저아이디");
		col.add("이름");
		col.add("키");
		col.add("체중");
		col.add("bmi");
		col.add("결과");

		show(dao.listBmi());

		tffind = new JTextField();
		tffind.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {
				String user_id = tffind.getText();
				dao.FindMember(user_id);
				show(dao.FindMember(user_id));

			}
		});
		tffind.setBounds(124, 257, 219, 35);
		contentPane.add(tffind);
		tffind.setColumns(10);

		JLabel lblNewLabel = new JLabel("아이디");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setFont(new Font("Gulim", Font.PLAIN, 24));
		lblNewLabel.setBounds(0, 73, 99, 29);
		contentPane.add(lblNewLabel);

		lbl = new JLabel("이름");
		lbl.setFont(new Font("Gulim", Font.PLAIN, 24));
		lbl.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl.setBounds(0, 113, 99, 29);
		contentPane.add(lbl);

		lblNewLabel_2 = new JLabel("체중");
		lblNewLabel_2.setFont(new Font("Gulim", Font.PLAIN, 24));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setBounds(0, 158, 99, 29);
		contentPane.add(lblNewLabel_2);

		lblNewLabel_3 = new JLabel("신장");
		lblNewLabel_3.setFont(new Font("Gulim", Font.PLAIN, 24));
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_3.setBounds(0, 211, 99, 29);
		contentPane.add(lblNewLabel_3);

		label = new JLabel("검색");
		label.setFont(new Font("Gulim", Font.PLAIN, 24));
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setBounds(0, 257, 99, 29);
		contentPane.add(label);

		tfid = new JTextField();
		tfid.setBounds(124, 67, 219, 35);
		contentPane.add(tfid);
		tfid.setColumns(10);

		tfName = new JTextField();
		tfName.setBounds(124, 110, 219, 35);
		contentPane.add(tfName);
		tfName.setColumns(10);

		tfWeight = new JTextField();
		tfWeight.setBounds(124, 155, 219, 35);
		contentPane.add(tfWeight);
		tfWeight.setColumns(10);

		tfHeight = new JTextField();
		tfHeight.setBounds(124, 208, 219, 35);
		contentPane.add(tfHeight);
		tfHeight.setColumns(10);

		btnInsert = new JButton("추가");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				sign();
				int result = dao.InsertMember(dto);
				if (result == 1) {
					JOptionPane.showMessageDialog(Re34_JFrame.this, "추가되었습니다");
					show(dao.listBmi());
					clear();
				}
			}
		});
		btnInsert.setBounds(474, 69, 99, 37);
		contentPane.add(btnInsert);

		btnDelet = new JButton("삭제");
		btnDelet.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delete();

			}
		});
		btnDelet.setBounds(474, 119, 99, 37);
		contentPane.add(btnDelet);

		btnChange = new JButton("수정");
		btnChange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				change();
			}
		});
		btnChange.setBounds(474, 169, 99, 37);
		contentPane.add(btnChange);

		JLabel label_1 = new JLabel("등록날짜");
		label_1.setHorizontalAlignment(SwingConstants.RIGHT);
		label_1.setBounds(0, 21, 99, 29);
		contentPane.add(label_1);

		tfDate = new JTextField();
		tfDate.setBounds(124, 18, 219, 37);
		contentPane.add(tfDate);
		tfDate.setColumns(10);

		JLabel lblyymmdd = new JLabel("20YY-MM-DD 로입력");
		lblyymmdd.setBounds(365, 21, 228, 29);
		contentPane.add(lblyymmdd);
	}
}
