package DB_Report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class Re34_DAO {

	public int InsertMember(Re34_DTO dto) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.dbConn();
			String sql = "insert into Health(signdate, user_id, name, height, weight, bmi) values (?,?,?,?,?,round(weight/(((height/100)*(height/100))),2))";
			pstmt = conn.prepareStatement(sql);
			pstmt.setDate(1, dto.getSigndate());
			pstmt.setString(2, dto.getUser_id());
			pstmt.setString(3, dto.getName());
			pstmt.setDouble(4, dto.getHeight());
			pstmt.setDouble(5, dto.getWeight());
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;

	}

	public Vector FindMember(String user_id) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.dbConn();
			String sql = "select * from health where user_id like ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, "%" + user_id + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				String result = "";
				if (rs.getDouble("bmi") <= 18.5) {
					result = "저체중";
				} else if (rs.getDouble("bmi") <= 22.9) {
					result = "정상";
				} else if (rs.getDouble("bmi") <= 24.9) {
					result = "과체중";
				} else {
					result = "비만";
				}
				row.add(rs.getDate("signdate"));
				row.add(rs.getInt("user_no"));
				row.add(rs.getString("user_id"));
				row.add(rs.getString("name"));
				row.add(rs.getDouble("height"));
				row.add(rs.getDouble("weight"));
				row.add(rs.getDouble("bmi"));
				row.add(result);
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return items;

	}

	public Vector listBmi() {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Re34_DTO dto = new Re34_DTO();
		try {

			conn = DB.dbConn();
			String sql = "select signdate, user_no , user_id, name, height, weight, bmi from Health";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				String result = "";
				if (rs.getDouble("bmi") <= 18.5) {
					result = "저체중";
				} else if (rs.getDouble("bmi") <= 22.9) {
					result = "정상";
				} else if (rs.getDouble("bmi") <= 24.9) {
					result = "과체중";
				} else {
					result = "비만";
				}
				row.add(rs.getDate("signdate"));
				row.add(rs.getInt("user_no"));
				row.add(rs.getString("user_id"));
				row.add(rs.getString("name"));
				row.add(rs.getDouble("height"));
				row.add(rs.getDouble("weight"));
				row.add(rs.getDouble("bmi"));
				row.add(result);
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				if (pstmt != null) {
					pstmt.close();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}

		}

		return items;
	}

	public int deleteMember(String user_id) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.dbConn();
			String sql = "delete from health where user_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, user_id);
			result = pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	public int updateMember(Re34_DTO dto) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.dbConn();
			String sql = "update health set signdate=?, name=?, height=?, weight=? where user_id=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setDate(1 ,dto.getSigndate() );
			pstmt.setString(2, dto.getName());
			pstmt.setDouble(3, dto.getHeight());
			pstmt.setDouble(4, dto.getWeight());
			pstmt.setString(5, dto.getUser_id());
			result = pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
}
