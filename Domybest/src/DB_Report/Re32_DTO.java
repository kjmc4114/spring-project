package DB_Report;

public class Re32_DTO {
private int Person_id;
private String name;
private String address;
private String num;
private String email;
public Re32_DTO() { //기본생성자
}

public Re32_DTO(int person_id, String name, String address, String num, String email) {
	Person_id = person_id;
	this.name = name;
	this.address = address;
	this.num = num;
	this.email = email;
}

public int getPersion_id() {
	return Person_id;
}
public void setPersion_id(int persion_id) {
	Person_id = persion_id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getNum() {
	return num;
}
public void setNum(String num) {
	this.num = num;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}

@Override
public String toString() {
	return "Re32_DAO [Persion_id=" + Person_id + ", name=" + name + ", address=" + address + ", num=" + num
			+ ", email=" + email + "]";
}

}
