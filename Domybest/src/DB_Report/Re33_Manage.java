package DB_Report;

import java.util.List;
import java.util.Scanner;

public class Re33_Manage {
	void DeletebyAccount() {
		Scanner input = new Scanner(System.in);
		System.out.println("삭제할 계좌번호를 입력하세요 ex ***-***-***");
		String account = input.nextLine();
		Re33_DAO dao = new Re33_DAO();
		int result = dao.DeletebyAccount(account);
		// if(result == 1){
		// System.out.println("삭제되었습니다");
		// }else {
		// System.out.println("계좌번호를 확인하세요");
		// }
		String a = (result == 1) ? "삭제되었습니다." : "계좌번호를 확인하세요";
		System.out.println(a);

	}

	void insert() {
		Scanner scan = new Scanner(System.in);
		System.out.println("은행이름");
		String bankname = scan.nextLine(); //next Line 으로 할것이야.
		System.out.println("은행계좌");
		String account = scan.nextLine();
		System.out.println("고객명");
		String name = scan.nextLine();
		System.out.println("잔액");
		int balance = scan.nextInt();
		Re33_DAO dao = new Re33_DAO();
		Re33_DTO dto = new Re33_DTO(bankname, account, name, balance);
		dao.InsertRecordBank(dto);
		System.out.println("추가되었습니다.");

	}

	void list() {
		Re33_DAO dao = new Re33_DAO();
		List<Re33_DTO> items = dao.CallRecordTable();
		System.out.println("고객번호\t은행\t계좌번호\t\t예금주\t\t잔액");
		System.out.println("===================================================");
		for (Re33_DTO dto : items) {

			System.out.print(dto.getUser_num()+"\t");
			System.out.print(dto.getBankname()+"\t");
			System.out.print(dto.getAccount()+"\t");
			System.out.print(dto.getName()+"\t"+"\t");
			System.out.println(String.format("%,d", dto.getBalance()));
		}

	}
	public enum CMD {
		LIST , ADD , DELETE , EXIT
	} 

	public static void main(String[] args){ 
		Re33_Manage manage = new Re33_Manage();
		
		Scanner input = new Scanner(System.in);
		
		for (;;) {
			System.out.println("작업을 선택하세요 (list = 목록 add = 추가  delete = 삭제  exit = 종료)");
			CMD cmd = CMD.valueOf(input.nextLine().toUpperCase());
			
			switch (cmd) {
			case EXIT:
				input.close();
				System.out.println("프로그램종료");
				System.exit(0);
				break;
			case LIST:
				manage.list();
				break;
			case ADD:
				manage.insert();
				break;

			case DELETE:
				manage.DeletebyAccount();
				break;
			}
		}
	}
}
