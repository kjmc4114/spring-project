package DB_Report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import ch21.oracle.ScoreDTO;

public class Re35_DAO {
	public int deleteAccount(String accountno){
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.OracleConn();
			StringBuilder sb = new StringBuilder();
			sb.append("delete from account ");
			sb.append(" where accountno =?");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, accountno);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
		
	}
	public int updateAccount(Re35_DTO dto){
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.OracleConn();
			StringBuilder sb = new StringBuilder();
			sb.append("update account set bankname =?, name =? ,balance =? ");
			sb.append(" where accountno =?");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, dto.getBankname());
			pstmt.setString(4, dto.getAccountno());
			pstmt.setString(2, dto.getName());
			pstmt.setInt(3, dto.getBalance());
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
		
	}
	
	public int insertAccount(Re35_DTO dto) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.OracleConn();
			StringBuilder sb = new StringBuilder();
			sb.append("insert into account ");
			sb.append("values (?,?,?,?)");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, dto.getBankname());
			pstmt.setString(2, dto.getAccountno());
			pstmt.setString(3, dto.getName());
			pstmt.setInt(4, dto.getBalance());
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;

	}

	public Vector listAccount() {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.OracleConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select * from account");
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();

			while (rs.next()) {
				Vector row = new Vector();
				String bankname = rs.getString("bankname");
				String accountno = rs.getString("accountno");
				String name = rs.getString("name");
				int balance = rs.getInt("balance");
				row.add(bankname);
				row.add(accountno);
				row.add(name);
				row.add(balance);
				items.add(row);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items;
	}
}
