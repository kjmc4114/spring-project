package DB_Report;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Re35_Edit extends JFrame {

	private JPanel contentPane;
	private JTextField tfEditBankname;
	private JTextField tfEditAccount;
	private JTextField tfEditName;
	private JTextField tfEditBalance;
	private Re35_Manager manager; // 부모창의 명 parent 
	private Re35_DTO dto;

	
	
	public Re35_Edit(Re35_Manager manager, Re35_DTO dto) {
		this.manager = manager;
		this.dto =dto;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 406, 420);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("은행이름");
		lblNewLabel.setBounds(22, 21, 99, 29);
		contentPane.add(lblNewLabel);
		
		tfEditBankname = new JTextField();
		tfEditBankname.setBounds(162, 18, 186, 35);
		contentPane.add(tfEditBankname);
		tfEditBankname.setColumns(10);
		
		JLabel label = new JLabel("계좌");
		label.setBounds(22, 66, 99, 29);
		contentPane.add(label);
		
		tfEditAccount = new JTextField();
		tfEditAccount.setColumns(10);
		tfEditAccount.setBounds(162, 63, 186, 35);
		contentPane.add(tfEditAccount);
		
		JLabel label_1 = new JLabel("이름");
		label_1.setBounds(22, 110, 99, 29);
		contentPane.add(label_1);
		
		tfEditName = new JTextField();
		tfEditName.setColumns(10);
		tfEditName.setBounds(162, 107, 186, 35);
		contentPane.add(tfEditName);
		
		JLabel label_2 = new JLabel("잔액");
		label_2.setBounds(22, 153, 99, 29);
		contentPane.add(label_2);
		
		tfEditBalance = new JTextField();
		tfEditBalance.setColumns(10);
		tfEditBalance.setBounds(162, 150, 186, 35);
		contentPane.add(tfEditBalance);
		
		tfEditBankname.setText(dto.getBankname());
		tfEditAccount.setText(dto.getAccountno());
		tfEditName.setText(dto.getName());
		tfEditBalance.setText(dto.getName());
		tfEditBalance.setText(String.valueOf(dto.getBalance()));
		
		
		JButton btnNewButton = new JButton("수정");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Re35_DAO dao = new Re35_DAO();
				String bankname = tfEditBankname.getText();
				String account = tfEditAccount.getText();
				String name = tfEditName.getText();
				int balance = Integer.valueOf(tfEditBalance.getText());
				Re35_DTO dto = new Re35_DTO(bankname,account,name,balance);
				int result = dao.updateAccount(dto);
				if (result == 1){
				JOptionPane.showMessageDialog(Re35_Edit.this,"수정되었습니다.");
				manager.refreshTable();
				dispose();}
			}
		});
		btnNewButton.setBounds(22, 221, 153, 37);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("삭제");
		btnNewButton_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Re35_DAO dao = new Re35_DAO();
					String accountno = tfEditAccount.getText();
					int result = dao.deleteAccount(accountno);
					
					JOptionPane.showConfirmDialog(Re35_Edit.this, "삭제하시겠습니까?");
					// 확인메세지 작성할것.
				
				}
		});
		btnNewButton_1.setBounds(197, 221, 153, 37);
		contentPane.add(btnNewButton_1);
	}

}
