package DB_Report;

public class Re35_DTO {
	private String bankname;
	private String accountno;
	private String name;
	private int balance;
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public String getAccountno() {
		return accountno;
	}
	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "Re35_DTO [bankname=" + bankname + ", accountno=" + accountno + ", name=" + name + ", balance=" + balance
				+ "]";
	}
	 public Re35_DTO() {
		// TODO Auto-generated constructor stub
	}
	public Re35_DTO(String bankname, String accountno, String name, int balance) {
		this.bankname = bankname;
		this.accountno = accountno;
		this.name = name;
		this.balance = balance;
	}
	 

}
