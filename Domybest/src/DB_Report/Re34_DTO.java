package DB_Report;

import java.sql.*;

public class Re34_DTO {
private Date signdate;
private int user_no;
private String user_id;
private String name;
private double weight;
private double height;
private double bmi;
private String bmiresult;
public Re34_DTO() {
}


public Re34_DTO(Date signdate, int user_no, String user_id, String name, double weight, double height, double bmi,
		String bmiresult) {
	this.signdate = signdate;
	this.user_no = user_no;
	this.user_id = user_id;
	this.name = name;
	this.weight = weight;
	this.height = height;
	this.bmi = bmi;
	this.bmiresult = bmiresult;
}




public Re34_DTO(Date signdate, String user_id, String name, double height, double weight) {
	this.signdate = signdate;
	this.user_id = user_id;
	this.name = name;
	this.weight = weight;
	this.height = height;
}


public Date getSigndate() {
	return signdate;
}
public void setSigndate(Date signdate) {
	this.signdate = signdate;
}
public int getUser_no() {
	return user_no;
}
public void setUser_no(int user_no) {
	this.user_no = user_no;
}
public String getUser_id() {
	return user_id;
}
public void setUser_id(String user_id) {
	this.user_id = user_id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public double getWeight() {
	return weight;
}
public void setWeight(double weight) {
	this.weight = weight;
}
public double getHeight() {
	return height;
}
public void setHeight(double height) {
	this.height = height;
}
public double getBmi() {
	return bmi;
}
public void setBmi(double bmi) {
	this.bmi = bmi;
}
public String getBmiresult() {
	return bmiresult;
}
public void setBmiresult(String bmiresult) {
	this.bmiresult = bmiresult;
}
@Override
public String toString() {
	return "Re34_DTO [signdate=" + signdate + ", user_no=" + user_no + ", user_id=" + user_id + ", name=" + name
			+ ", weight=" + weight + ", height=" + height + ", bmi=" + bmi + ", bmiresult=" + bmiresult + "]";
}

}
