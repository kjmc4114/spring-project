package DB_Report;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ch21.oracle.ScoreDAO;
import ch21.oracle.ScoreDTO;
import ch21.oracle.ScoreSave;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Re35_insert extends JFrame {

	private JPanel contentPane;
	private JTextField tfBankname;
	private JTextField tfAccountno;
	private JTextField tfName;
	private JTextField tfBalance;
	private Re35_Manager manager;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Re35_insert frame = new Re35_insert();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Re35_insert(Re35_Manager manager){
		this();
		this.manager = manager;
		
		
	}
	public Re35_insert() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 399, 493);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("은행이름");
		lblNewLabel.setBounds(22, 53, 99, 29);
		contentPane.add(lblNewLabel);
		
		tfBankname = new JTextField();
		tfBankname.setBounds(143, 50, 186, 35);
		contentPane.add(tfBankname);
		tfBankname.setColumns(10);
		
		tfAccountno = new JTextField();
		tfAccountno.setBounds(143, 106, 186, 35);
		contentPane.add(tfAccountno);
		tfAccountno.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("계좌번호");
		lblNewLabel_1.setBounds(22, 109, 99, 29);
		contentPane.add(lblNewLabel_1);
		
		JLabel label = new JLabel("이름");
		label.setBounds(22, 165, 99, 29);
		contentPane.add(label);
		
		tfName = new JTextField();
		tfName.setColumns(10);
		tfName.setBounds(143, 162, 186, 35);
		contentPane.add(tfName);
		
		JLabel label_1 = new JLabel("잔액");
		label_1.setBounds(22, 218, 99, 29);
		contentPane.add(label_1);
		
		tfBalance = new JTextField();
		tfBalance.setColumns(10);
		tfBalance.setBounds(143, 215, 186, 35);
		contentPane.add(tfBalance);
		
		JButton btnSave = new JButton("저장");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String accountno = tfAccountno.getText();
				String bankname = tfBankname.getText();
				String name = tfName.getText();
				int balance = Integer.parseInt(tfBalance.getText());
				
				Re35_DTO dto = new Re35_DTO(bankname, accountno ,name , balance);
				Re35_DAO dao = new Re35_DAO();
				int result = dao.insertAccount(dto);
			
				
				
				if (result == 1) {
					JOptionPane.showMessageDialog(Re35_insert.this, "저장되었습니다.");
					//frm = new ScoreList 으로 했을시 아예 새로운 프레임을 띄우는 것과 같기 떄문에 매개변수를 이용하여 띄워야함. 
					manager.refreshTable();
					dispose();// 현재 창을 닫음.
				}
			}
		});
		btnSave.setBounds(87, 293, 153, 37);
		contentPane.add(btnSave);
	}

}
