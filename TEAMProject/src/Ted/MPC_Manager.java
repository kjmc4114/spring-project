package Ted;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Calendar;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.JLabel;
import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MPC_Manager extends JFrame implements Runnable {

	private JPanel contentPane;
	private JLabel lblTime1;
	private JLabel lblTime2;
	private JTextArea taSystemlog;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MPC_Manager frame = new MPC_Manager();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MPC_Manager() {
		setTitle("PC방 관리자");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1368, 1143);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("주문내역");
		menuBar.add(mnNewMenu);

		JMenu mnNewMenu_1 = new JMenu("푸른이");
		menuBar.add(mnNewMenu_1);

		JMenu mnNewMenu_2 = new JMenu("염메뉴");
		menuBar.add(mnNewMenu_2);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 1342, 1032);
		contentPane.add(tabbedPane);
																								
																								Thread th = new Thread(this);
																								th.start();
																										
																												JPanel panel = new JPanel();
																												panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
																												panel.setBackground(new Color(56, 63, 72));
																												tabbedPane.addTab("좌석정보", null, panel, null);
																												panel.setLayout(null);
																												
																														JLabel lblTeam = new JLabel("Team1 PC 방");
																														lblTeam.setHorizontalAlignment(SwingConstants.CENTER);
																														lblTeam.setForeground(new Color(255, 255, 255));
																														lblTeam.setBounds(4, 9, 238, 49);
																														panel.add(lblTeam);
																														
																																JPanel panel_info = new JPanel();
																																panel_info.setBounds(3, 59, 247, 189);
																																panel.add(panel_info);
																																panel_info.setLayout(null);
																																
																																		JLabel label = new JLabel("현재좌석/총좌석");
																																		label.setFont(new Font("굴림", Font.PLAIN, 32));
																																		label.setHorizontalAlignment(SwingConstants.CENTER);
																																		label.setBounds(-1, 91, 248, 56);
																																		panel_info.add(label);
																																		
																																		lblTime1 = new JLabel("년 월 일");
																																		lblTime1.setHorizontalAlignment(SwingConstants.CENTER);
																																		lblTime1.setFont(new Font("굴림", Font.PLAIN, 30));
																																		lblTime1.setBounds(0, 0, 247, 34);
																																		panel_info.add(lblTime1);
																																		
																																		lblTime2 = new JLabel("시 분 초");
																																		lblTime2.setHorizontalAlignment(SwingConstants.CENTER);
																																		lblTime2.setFont(new Font("굴림", Font.PLAIN, 30));
																																		lblTime2.setBounds(0, 36, 247, 34);
																																		panel_info.add(lblTime2);
																																		
																																				JPanel panel_3 = new JPanel();
																																				panel_3.setBounds(404, 61, 266, 212);
																																				panel.add(panel_3);
																																				panel_3.setBackground(new Color(0, 128, 128));
																																				panel_3.setLayout(null);
																																				
																																				JButton btnNewButton_3 = new JButton("1");
																																				btnNewButton_3.setBounds(0, 175, 57, 37);
																																				panel_3.add(btnNewButton_3);
																																				
																																				JLabel label_1 = new JLabel("1");
																																				label_1.setHorizontalAlignment(SwingConstants.CENTER);
																																				label_1.setBounds(0, 2, 49, 29);
																																				panel_3.add(label_1);
																																				
																																				JButton button_3 = new JButton("찻잔");
																																				button_3.setFont(new Font("굴림", Font.PLAIN, 10));
																																				button_3.setBounds(0, 94, 45, 37);
																																				panel_3.add(button_3);
																																				
																																				JLabel lblNewLabel = new JLabel("이름");
																																				lblNewLabel.setFont(new Font("Gulim", Font.PLAIN, 24));
																																				lblNewLabel.setBounds(166, 2, 99, 29);
																																				panel_3.add(lblNewLabel);
																																				
																																				JButton button = new JButton("1");
																																				button.setBounds(72, 175, 57, 37);
																																				panel_3.add(button);
																																				
																																				JButton button_1 = new JButton("1");
																																				button_1.setBounds(141, 175, 57, 37);
																																				panel_3.add(button_1);
																																				
																																				JButton button_2 = new JButton("1");
																																				button_2.setBounds(209, 175, 57, 37);
																																				panel_3.add(button_2);
																																				
																																				JLabel lblNewLabel_1 = new JLabel("등급");
																																				lblNewLabel_1.setFont(new Font("Gulim", Font.PLAIN, 24));
																																				lblNewLabel_1.setBounds(100, 3, 58, 29);
																																				panel_3.add(lblNewLabel_1);
																																				
																																				JLabel label_8 = new JLabel("남은시간");
																																				label_8.setBounds(166, 52, 99, 29);
																																				panel_3.add(label_8);
																																				
																																				JPanel panel_4 = new JPanel();
																																				panel_4.setLayout(null);
																																				panel_4.setBackground(new Color(0, 128, 128));
																																				panel_4.setBounds(726, 59, 266, 212);
																																				panel.add(panel_4);
																																				
																																				JButton button_4 = new JButton("1");
																																				button_4.setBounds(0, 175, 57, 37);
																																				panel_4.add(button_4);
																																				
																																				JLabel label_2 = new JLabel("1");
																																				label_2.setHorizontalAlignment(SwingConstants.CENTER);
																																				label_2.setBounds(0, 2, 49, 29);
																																				panel_4.add(label_2);
																																				
																																				JButton button_5 = new JButton("찻잔");
																																				button_5.setFont(new Font("굴림", Font.PLAIN, 10));
																																				button_5.setBounds(0, 94, 45, 37);
																																				panel_4.add(button_5);
																																				
																																				JLabel label_3 = new JLabel("이름");
																																				label_3.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_3.setBounds(166, 2, 99, 29);
																																				panel_4.add(label_3);
																																				
																																				JButton button_6 = new JButton("1");
																																				button_6.setBounds(72, 175, 57, 37);
																																				panel_4.add(button_6);
																																				
																																				JButton button_7 = new JButton("1");
																																				button_7.setBounds(141, 175, 57, 37);
																																				panel_4.add(button_7);
																																				
																																				JButton button_8 = new JButton("1");
																																				button_8.setBounds(209, 175, 57, 37);
																																				panel_4.add(button_8);
																																				
																																				JLabel label_4 = new JLabel("등급");
																																				label_4.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_4.setBounds(100, 3, 58, 29);
																																				panel_4.add(label_4);
																																				
																																				JPanel panel_5 = new JPanel();
																																				panel_5.setLayout(null);
																																				panel_5.setBackground(new Color(0, 128, 128));
																																				panel_5.setBounds(1048, 59, 266, 212);
																																				panel.add(panel_5);
																																				
																																				JButton button_9 = new JButton("1");
																																				button_9.setBounds(0, 175, 57, 37);
																																				panel_5.add(button_9);
																																				
																																				JLabel label_5 = new JLabel("1");
																																				label_5.setHorizontalAlignment(SwingConstants.CENTER);
																																				label_5.setBounds(0, 2, 49, 29);
																																				panel_5.add(label_5);
																																				
																																				JButton button_10 = new JButton("찻잔");
																																				button_10.setFont(new Font("굴림", Font.PLAIN, 10));
																																				button_10.setBounds(0, 94, 45, 37);
																																				panel_5.add(button_10);
																																				
																																				JLabel label_6 = new JLabel("이름");
																																				label_6.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_6.setBounds(166, 2, 99, 29);
																																				panel_5.add(label_6);
																																				
																																				JButton button_11 = new JButton("1");
																																				button_11.setBounds(72, 175, 57, 37);
																																				panel_5.add(button_11);
																																				
																																				JButton button_12 = new JButton("1");
																																				button_12.setBounds(141, 175, 57, 37);
																																				panel_5.add(button_12);
																																				
																																				JButton button_13 = new JButton("1");
																																				button_13.setBounds(209, 175, 57, 37);
																																				panel_5.add(button_13);
																																				
																																				JLabel label_7 = new JLabel("등급");
																																				label_7.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_7.setBounds(100, 3, 58, 29);
																																				panel_5.add(label_7);
																																				
																																				JPanel panel_7 = new JPanel();
																																				panel_7.setLayout(null);
																																				panel_7.setBackground(new Color(0, 128, 128));
																																				panel_7.setBounds(404, 324, 266, 212);
																																				panel.add(panel_7);
																																				
																																				JButton button_19 = new JButton("1");
																																				button_19.setBounds(0, 175, 57, 37);
																																				panel_7.add(button_19);
																																				
																																				JLabel label_11 = new JLabel("1");
																																				label_11.setHorizontalAlignment(SwingConstants.CENTER);
																																				label_11.setBounds(0, 2, 49, 29);
																																				panel_7.add(label_11);
																																				
																																				JButton button_20 = new JButton("찻잔");
																																				button_20.setFont(new Font("굴림", Font.PLAIN, 10));
																																				button_20.setBounds(0, 94, 45, 37);
																																				panel_7.add(button_20);
																																				
																																				JLabel label_12 = new JLabel("이름");
																																				label_12.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_12.setBounds(166, 2, 99, 29);
																																				panel_7.add(label_12);
																																				
																																				JButton button_21 = new JButton("1");
																																				button_21.setBounds(72, 175, 57, 37);
																																				panel_7.add(button_21);
																																				
																																				JButton button_22 = new JButton("1");
																																				button_22.setBounds(141, 175, 57, 37);
																																				panel_7.add(button_22);
																																				
																																				JButton button_23 = new JButton("1");
																																				button_23.setBounds(209, 175, 57, 37);
																																				panel_7.add(button_23);
																																				
																																				JLabel label_13 = new JLabel("등급");
																																				label_13.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_13.setBounds(100, 3, 58, 29);
																																				panel_7.add(label_13);
																																				
																																				JPanel panel_9 = new JPanel();
																																				panel_9.setLayout(null);
																																				panel_9.setBackground(new Color(0, 128, 128));
																																				panel_9.setBounds(1048, 322, 266, 212);
																																				panel.add(panel_9);
																																				
																																				JButton button_29 = new JButton("1");
																																				button_29.setBounds(0, 175, 57, 37);
																																				panel_9.add(button_29);
																																				
																																				JLabel label_17 = new JLabel("1");
																																				label_17.setHorizontalAlignment(SwingConstants.CENTER);
																																				label_17.setBounds(0, 2, 49, 29);
																																				panel_9.add(label_17);
																																				
																																				JButton button_30 = new JButton("찻잔");
																																				button_30.setFont(new Font("굴림", Font.PLAIN, 10));
																																				button_30.setBounds(0, 94, 45, 37);
																																				panel_9.add(button_30);
																																				
																																				JLabel label_18 = new JLabel("이름");
																																				label_18.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_18.setBounds(166, 2, 99, 29);
																																				panel_9.add(label_18);
																																				
																																				JButton button_31 = new JButton("1");
																																				button_31.setBounds(72, 175, 57, 37);
																																				panel_9.add(button_31);
																																				
																																				JButton button_32 = new JButton("1");
																																				button_32.setBounds(141, 175, 57, 37);
																																				panel_9.add(button_32);
																																				
																																				JButton button_33 = new JButton("1");
																																				button_33.setBounds(209, 175, 57, 37);
																																				panel_9.add(button_33);
																																				
																																				JLabel label_19 = new JLabel("등급");
																																				label_19.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_19.setBounds(100, 3, 58, 29);
																																				panel_9.add(label_19);
																																				
																																				JPanel panel_11 = new JPanel();
																																				panel_11.setLayout(null);
																																				panel_11.setBackground(new Color(0, 128, 128));
																																				panel_11.setBounds(404, 577, 266, 212);
																																				panel.add(panel_11);
																																				
																																				JButton button_39 = new JButton("1");
																																				button_39.setBounds(0, 175, 57, 37);
																																				panel_11.add(button_39);
																																				
																																				JLabel label_23 = new JLabel("1");
																																				label_23.setHorizontalAlignment(SwingConstants.CENTER);
																																				label_23.setBounds(0, 2, 49, 29);
																																				panel_11.add(label_23);
																																				
																																				JButton button_40 = new JButton("찻잔");
																																				button_40.setFont(new Font("굴림", Font.PLAIN, 10));
																																				button_40.setBounds(0, 94, 45, 37);
																																				panel_11.add(button_40);
																																				
																																				JLabel label_24 = new JLabel("이름");
																																				label_24.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_24.setBounds(166, 2, 99, 29);
																																				panel_11.add(label_24);
																																				
																																				JButton button_41 = new JButton("1");
																																				button_41.setBounds(72, 175, 57, 37);
																																				panel_11.add(button_41);
																																				
																																				JButton button_42 = new JButton("1");
																																				button_42.setBounds(141, 175, 57, 37);
																																				panel_11.add(button_42);
																																				
																																				JButton button_43 = new JButton("1");
																																				button_43.setBounds(209, 175, 57, 37);
																																				panel_11.add(button_43);
																																				
																																				JLabel label_25 = new JLabel("등급");
																																				label_25.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_25.setBounds(100, 3, 58, 29);
																																				panel_11.add(label_25);
																																				
																																				JPanel panel_12 = new JPanel();
																																				panel_12.setLayout(null);
																																				panel_12.setBackground(new Color(0, 128, 128));
																																				panel_12.setBounds(726, 575, 266, 212);
																																				panel.add(panel_12);
																																				
																																				JButton button_44 = new JButton("1");
																																				button_44.setBounds(0, 175, 57, 37);
																																				panel_12.add(button_44);
																																				
																																				JLabel label_26 = new JLabel("1");
																																				label_26.setHorizontalAlignment(SwingConstants.CENTER);
																																				label_26.setBounds(0, 2, 49, 29);
																																				panel_12.add(label_26);
																																				
																																				JButton button_45 = new JButton("찻잔");
																																				button_45.setFont(new Font("굴림", Font.PLAIN, 10));
																																				button_45.setBounds(0, 94, 45, 37);
																																				panel_12.add(button_45);
																																				
																																				JLabel label_27 = new JLabel("이름");
																																				label_27.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_27.setBounds(166, 2, 99, 29);
																																				panel_12.add(label_27);
																																				
																																				JButton button_46 = new JButton("1");
																																				button_46.setBounds(72, 175, 57, 37);
																																				panel_12.add(button_46);
																																				
																																				JButton button_47 = new JButton("1");
																																				button_47.setBounds(141, 175, 57, 37);
																																				panel_12.add(button_47);
																																				
																																				JButton button_48 = new JButton("1");
																																				button_48.setBounds(209, 175, 57, 37);
																																				panel_12.add(button_48);
																																				
																																				JLabel label_28 = new JLabel("등급");
																																				label_28.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_28.setBounds(100, 3, 58, 29);
																																				panel_12.add(label_28);
																																				
																																				JPanel panel_13 = new JPanel();
																																				panel_13.setLayout(null);
																																				panel_13.setBackground(new Color(0, 128, 128));
																																				panel_13.setBounds(1048, 575, 266, 212);
																																				panel.add(panel_13);
																																				
																																				JButton button_49 = new JButton("1");
																																				button_49.setBounds(0, 175, 57, 37);
																																				panel_13.add(button_49);
																																				
																																				JLabel label_29 = new JLabel("1");
																																				label_29.setHorizontalAlignment(SwingConstants.CENTER);
																																				label_29.setBounds(0, 2, 49, 29);
																																				panel_13.add(label_29);
																																				
																																				JButton button_50 = new JButton("찻잔");
																																				button_50.setFont(new Font("굴림", Font.PLAIN, 10));
																																				button_50.setBounds(0, 94, 45, 37);
																																				panel_13.add(button_50);
																																				
																																				JLabel label_30 = new JLabel("이름");
																																				label_30.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_30.setBounds(166, 2, 99, 29);
																																				panel_13.add(label_30);
																																				
																																				JButton button_51 = new JButton("1");
																																				button_51.setBounds(72, 175, 57, 37);
																																				panel_13.add(button_51);
																																				
																																				JButton button_52 = new JButton("1");
																																				button_52.setBounds(141, 175, 57, 37);
																																				panel_13.add(button_52);
																																				
																																				JButton button_53 = new JButton("1");
																																				button_53.setBounds(209, 175, 57, 37);
																																				panel_13.add(button_53);
																																				
																																				JLabel label_31 = new JLabel("등급");
																																				label_31.setFont(new Font("굴림", Font.PLAIN, 24));
																																				label_31.setBounds(100, 3, 58, 29);
																																				panel_13.add(label_31);
																																				
																																				JScrollPane scrollPane_1 = new JScrollPane();
																																				scrollPane_1.setBounds(407, 835, 930, 154);
																																				panel.add(scrollPane_1);
																																				
																																				taSystemlog = new JTextArea();
																																				taSystemlog.setText("시스템로그");
																																				scrollPane_1.setViewportView(taSystemlog);
																																				
																																				JPanel panel_2 = new JPanel();
																																				panel_2.setBounds(4, 269, 247, 612);
																																				panel.add(panel_2);
																																				panel_2.setLayout(null);
																																				
																																				JLabel lblNewLabel_2 = new JLabel("pc no");
																																				lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
																																				lblNewLabel_2.setBounds(22, 39, 187, 42);
																																				panel_2.add(lblNewLabel_2);
																																				
																																				JLabel lblNewLabel_3 = new JLabel("uesr name");
																																				lblNewLabel_3.setBounds(22, 116, 187, 34);
																																				panel_2.add(lblNewLabel_3);
																																				
																																				JLabel lblNewLabel_4 = new JLabel("user grade");
																																				lblNewLabel_4.setBounds(22, 171, 187, 34);
																																				panel_2.add(lblNewLabel_4);
																																				
																																				JLabel lblNewLabel_5 = new JLabel("pay_state");
																																				lblNewLabel_5.setBounds(22, 233, 187, 34);
																																				panel_2.add(lblNewLabel_5);
																								
																										JPanel panel_1 = new JPanel();
																										tabbedPane.addTab("대균이형", null, panel_1, null);
																											
	}

	@Override
	public void run() {
		for(;;){Calendar cal = Calendar.getInstance();
		String str = String.format("%d-%02d-%02d", cal.get(Calendar.YEAR),cal.get(Calendar.MONTH)+1,cal.get(Calendar.DATE));
		String str2 = String.format("%02d:%02d:%02d", cal.get(Calendar.HOUR),cal.get(Calendar.MINUTE),cal.get(Calendar.SECOND));
		lblTime1.setText(str);
		lblTime2.setText(str2);
				}
	}
}
